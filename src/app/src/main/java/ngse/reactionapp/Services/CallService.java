package ngse.reactionapp.Services;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.ResultReceiver;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.NGSE.RequestLibrary.Query;
import com.NGSE.RequestLibrary.RequestInterface;
import com.NGSE.RequestLibrary.RequestParameters;
import com.android.internal.telephony.ITelephony;
import com.google.gson.JsonObject;

import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import ngse.Utility.OnSwipeTouchListener;
import ngse.reactionapp.Activities.MainActivity;
import ngse.reactionapp.Data.Call;
import ngse.reactionapp.Data.PreferencesData;
import ngse.reactionapp.R;
import ngse.reactionapp.Services.NetworkServices.AlertCustomDialog;
import ngse.reactionapp.Services.NetworkServices.PickerActivity;
import ngse.reactionapp.Services.NetworkServices.rest.FileUploadService;
import ngse.reactionapp.Services.NetworkServices.rest.RateService;
import ngse.reactionapp.Services.NetworkServices.rest.ServiceGenerator;
import ngse.reactionapp.Services.NetworkServices.rest.UserService;
import ngse.reactionapp.Utility.Constants;
import ngse.reactionapp.Utility.CustomRequestHandler;
import ngse.reactionapp.Utility.Utility;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;


public class CallService extends Service {
    public static final String ACTION_PICK_PHOTO = "ACTION_PICK_PHOTO";
    public static final String ACTION_PICK_VIDEO = "ACTION_PICK_VIDEO";
    public static final String ACTION_PICK_FILE = "ACTION_PICK_FILE";
    private ArrayList<FileModel> filesList = new ArrayList<>();


    private static final String TAG = CallService.class.getName();
    private View widgetView;
    private View widgetViewSmall;
    private View widgetViewRate;

    private int from; //из какой базы взято, константа с сервера

    public BroadcastReceiver callReceiver = new BroadcastReceiver() {
        String phoneNumber = null;
        boolean isIncoming = false;

        public void onReceive(final Context context, Intent intent) {
            if (intent.getAction().equals("android.intent.action.NEW_OUTGOING_CALL")) {
                //получаем исходящий номер
                phoneNumber = intent.getExtras().getString("android.intent.extra.PHONE_NUMBER");
                Log.e("TAG", "numbers= " + phoneNumber + "    :   " + PreferencesData.getLastCallNumber(getApplicationContext()));
//                if (phoneNumber.equals(PreferencesData.getLastCallNumber(getApplicationContext()))) {
                PreferencesData.setLastCallTime(getApplicationContext(), System.currentTimeMillis());
//                } else {
//                    resetPref();
//                }
                Log.e("TAG", "phoneNumber" + phoneNumber);

                isIncoming = true;
            } else if (intent.getAction().equals("android.intent.action.PHONE_STATE")) {
                String phone_state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
                if (phone_state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
                    Log.e("TAG", "EXTRA_STATE_RINGING");
                    phoneNumber = null;
                    resetPref();
                } else if (phone_state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK) && isIncoming) {
                    Log.e("TAG", "EXTRA_STATE_OFFHOOK");


                    new AsyncNetwork().execute(phoneNumber, null);

                } else if (phone_state.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
                    Log.e("TAG", "EXTRA_STATE_IDLE");

                    hideBigWidget();
                    hideSmallWidget();
                    showRateWidget();
                    //hideRateWidget();
                    //stopForeground(true);

                    if (PreferencesData.getLastCallTime(getApplicationContext()) == 0) {
                        Log.e("TAG", "getLatCallTime == 0");
                        return;
                    }
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            getCallDetails(context);
                        }
                    }, 1000);

                    isIncoming = false;
                }
            }
        }
    };


    public static void start(final Context context) {
        Intent intent = new Intent(context, CallService.class);
        context.startService(intent);

    }

    public static void stop(final Context context) {
        Intent intent = new Intent(context, CallService.class);
        context.stopService(intent);
    }

    private void resetPref() {
        Log.e("TAG", "resetPref");
        PreferencesData.setLastCallTime(getApplicationContext(), 0);
        PreferencesData.setLastCallNumber(getApplicationContext(), "");
    }

    private Call getCallDetails(Context context) {
        Call callLogsItem = new Call();
        Log.e("TAG", "getCallDetails");
        //List<CallLogsItem> callLogsItems=new ArrayList<>();


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {
            Log.e(TAG, "PERMISSION 'READ_CALL_LOG' DENIED");
        }

        Cursor managedCursor = getApplicationContext().getContentResolver().query(CallLog.Calls.CONTENT_URI, null,
                null, null, null);
        //CallLog.Calls.DATE + ">=" + (PreferencesData.getLastCallTime(getApplicationContext()) - 3000), null, null);//CallLog.Calls.TYPE + "=" + CallLog.Calls.OUTGOING_TYPE
        int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
        int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
        int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
        int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);

        while (managedCursor.moveToLast()) {
            String phNumber = managedCursor.getString(number);
            String callType = managedCursor.getString(type);
            long callDate = managedCursor.getLong(date);
            Date callDayTime = new Date(callDate);
            long callDuration = managedCursor.getLong(duration);
            String dir = null;
            int dircode = Integer.parseInt(callType);
            switch (dircode) {
                case CallLog.Calls.OUTGOING_TYPE:
                    dir = "OUTGOING";
                    break;

                case CallLog.Calls.INCOMING_TYPE:
                    dir = "INCOMING";
                    break;

                case CallLog.Calls.MISSED_TYPE:
                    dir = "MISSED";
                    break;
            }
            Log.e("TAG", "phNumbers" + phNumber + "    :   " + PreferencesData.getLastCallNumber(getApplicationContext()));
            if (!phNumber.equals(PreferencesData.getLastCallNumber(getApplicationContext())))
                break;
            else
                Log.e("TAG", "\nPhone Number:--- " + phNumber + " \nCall Type:--- "
                        + dir + " \nCall Date:--- " + new Date(callDate)// callDayTime
                        + " \nCall duration in sec :--- " + callDuration);
            Log.e("TAG", "\n----------------------------------" + new Date(PreferencesData.getLastCallTime(getApplicationContext())) + "\n");
            postCall(context, callDuration);
            break;

        }

        Log.e("TAG", "end of getCallDetails");
        managedCursor.close();
        return callLogsItem;

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(callReceiver);
        hideBigWidget();
        hideSmallWidget();
        hideRateWidget();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(final Intent intent, final int flags, final int startId) {
        int result = super.onStartCommand(intent, flags, startId);
        Log.e("TAG", "CallService onStartCommand");
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.intent.action.PHONE_STATE");
        filter.addAction("android.intent.action.NEW_OUTGOING_CALL");
        try {
            unregisterReceiver(callReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }

        registerReceiver(callReceiver, filter);
//        return START_STICKY;
        return result;
    }

    void postCall(Context context, long callDuration) {
        Log.e("TAG", "postCall");
        final String url = Constants.ROOT_CALL;
        List<NameValuePair> paramsList = new ArrayList<>();
//        paramsList.addAll(Arrays.asList(
//                new BasicNameValuePair("client_id", PreferencesData.getLoginID(context)),
//                new BasicNameValuePair("user_id", Long.toString(PreferencesData.getLastCallManagerID(context))),
//                new BasicNameValuePair("duration", Long.toString(callDuration)),
//                new BasicNameValuePair("status", "3")));/* 1: Missed; 2: Incoming; 3: Outgoing; 0: None; */
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getDefault());
        Log.e("TAG", "SENDED TIME: " + sdf.format(new Date()));

        paramsList.addAll(Arrays.asList(
                new BasicNameValuePair("id", PreferencesData.getLoginID(context)),
//                new BasicNameValuePair("user_id", Long.toString(PreferencesData.getLastCallManagerID(context))),
                new BasicNameValuePair("company_name", PreferencesData.getLastCallCompany(context)),
                new BasicNameValuePair("contact_name", PreferencesData.getLastCallName(context)),
                new BasicNameValuePair("phone", PreferencesData.getLastCallNumber(context)),
                new BasicNameValuePair("from", String.valueOf(from)),
                new BasicNameValuePair("type_call", "3"),
                new BasicNameValuePair("duration", Long.toString(callDuration)),
                new BasicNameValuePair("status", "3"),/* 1: Missed; 2: Incoming; 3: Outgoing; 0: None; */
                new BasicNameValuePair("created_at", sdf.format(new Date()))));/* 1: Missed; 2: Incoming; 3: Outgoing; 0: None; */
        resetPref();
        Log.e(TAG, "url= " + url);
        RequestParameters requestParameters = new RequestParameters(Query.TYPE_REQUEST.POST, -1, paramsList, url);
        requestParameters.setHeader(Arrays.asList(new BasicHeader("X-Username", PreferencesData.getLoginName(context)),
                new BasicHeader("X-Password", PreferencesData.getLoginPassword(context))));

        Query.getInstance().setQuery(context, new CustomRequestHandler(context, true) {
            @Override
            public void processingRequest(RequestInterface requestInterface, Object response, int code, int type, Header[] headers) {
                MainActivity.newInstance(CallService.this);
                super.processingRequest(requestInterface, response, code, type, headers);
                if (!errorInfo.isResult()) {
                    requestInterface.error(errorInfo);
                } else {
                    Log.e("TAG", "request json= " + data);

                    try {
                        initRateWidget(
                                data.getString("id"),
                                data.getString("company_name")
                        );
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            }
        }, new RequestInterface() {
            @Override
            public void finish(Object obj) {
                Log.e("TAG", "данные о звонке успешно сохранены");
            }

            @Override
            public void error(Object obj) {
            }
        }, requestParameters);

    }

    private void initBigWidget(String number, String name, String company) {
        if (widgetView == null) {
            Log.e("TAG", "init widget");

            PreferencesData.setLastCallNumber(getApplicationContext(), number);
            PreferencesData.setLastCallName(getApplicationContext(), name);
            PreferencesData.setLastCallCompany(getApplicationContext(), company);
            sendBroadcast(new Intent("xyz"));

            widgetView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.activity_call, null);

            widgetView.findViewById(R.id.activity_call_end).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    disconnectPhoneItelephony(getApplicationContext());
                }
            });
            TextView activity_call_number = (TextView) widgetView.findViewById(R.id.activity_call_number);
            activity_call_number.setText(getApplicationContext().getString(R.string.activity_call_number, number));

//            ((TextView) widgetView.findViewById(R.id.activity_call_name)).setText(getContactName(number, getApplicationContext()));
            ((TextView) widgetView.findViewById(R.id.activity_call_name)).setText(name);
            ((TextView) widgetView.findViewById(R.id.activity_call_company)).setText(company);

            showBigWidget();

            //from here
            widgetView.findViewById(R.id.activity_call_pick_photo).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(CallService.this, PickerActivity.class);
                    intent.putExtra(PickerReceiver.KEY_RECEIVER, new PickerReceiver());
                    intent.setAction(ACTION_PICK_PHOTO);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    widgetView.setVisibility(View.GONE);
                }
            });
            widgetView.findViewById(R.id.activity_call_pick_photo).setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return false;
                }
            });
            widgetView.findViewById(R.id.activity_call_pick_video).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(CallService.this, PickerActivity.class);
                    intent.putExtra(PickerReceiver.KEY_RECEIVER, new PickerReceiver());
                    intent.setAction(ACTION_PICK_VIDEO);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    widgetView.setVisibility(View.GONE);
                }
            });
            widgetView.findViewById(R.id.activity_call_pick_file).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(CallService.this, PickerActivity.class);
                    intent.putExtra(PickerReceiver.KEY_RECEIVER, new PickerReceiver());
                    intent.setAction(ACTION_PICK_FILE);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    widgetView.setVisibility(View.GONE);
                }
            });

            widgetView.findViewById(R.id.tv_send_files).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!filesList.isEmpty()) {
                        new AsyncUpload().execute();
                    }
                }
            });

            widgetView.findViewById(R.id.activity_call_arrow_down).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    initSmallWidget();
                    widgetView.setVisibility(View.GONE);
                }
            });

        }
    }

    private void initRateWidget(final String callId, String company) {
        if (widgetViewRate == null) {
            Log.e("TAG", "init Rate widget");

            widgetViewRate = LayoutInflater.from(getApplicationContext()).inflate(R.layout.widget_rate_call, null);

            showRateWidget();
            //listeners

            ((TextView) widgetViewRate.findViewById(R.id.rate_call_company)).setText(company);

            widgetViewRate.findViewById(R.id.btn_close_rate).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    hideRateWidget();
                    stopForeground(true);
                }
            });

            widgetViewRate.findViewById(R.id.iv_rate_dislike).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new AsyncRate().execute(callId, "1");
                }
            });
            widgetViewRate.findViewById(R.id.iv_rate_non).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new AsyncRate().execute(callId, "2");
                }
            });
            widgetViewRate.findViewById(R.id.iv_rate_like).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new AsyncRate().execute(callId, "3");
                }
            });



            widgetViewRate.setOnTouchListener(new OnSwipeTouchListener(CallService.this) {
                //                public void onSwipeTop() {
//                    Toast.makeText(MyActivity.this, "top", Toast.LENGTH_SHORT).show();
//                }
                public void onSwipeRight() {
//                    Toast toast = Toast.makeText(CallService.this, "right", Toast.LENGTH_SHORT);
//                    toast.setGravity(Gravity.TOP, 0, 0);
//                    toast.show();
                    hideRateWidget();
                    stopForeground(true);
                }
                public void onSwipeLeft() {
//                    Toast toast = Toast.makeText(CallService.this, "left", Toast.LENGTH_SHORT);
//                    toast.setGravity(Gravity.TOP, 0, 0);
//                    toast.show();
                    hideRateWidget();
                    stopForeground(true);
                }
//                public void onSwipeBottom() {
//                    Toast.makeText(MyActivity.this, "bottom", Toast.LENGTH_SHORT).show();
//                }

            });
        }
    }

    private void initSmallWidget() {
        if (widgetViewSmall == null) {
            Log.e("TAG", "init SMALL widget");

            widgetViewSmall = LayoutInflater.from(getApplicationContext()).inflate(R.layout.widget_small_call, null);

            showSmallWidget();

            widgetViewSmall.findViewById(R.id.iv_show_big_widget).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    widgetView.setVisibility(View.VISIBLE);
                    hideSmallWidget();
                }
            });
        }
    }


    private void showBigWidget() {
        Log.e("TAG", "showBigWidget widget");
        if (widgetView == null)
            return;

        final WindowManager.LayoutParams param = new WindowManager.LayoutParams();
        param.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
        param.format = PixelFormat.RGBA_8888;
        param.type = WindowManager.LayoutParams.TYPE_SYSTEM_ALERT;
        param.gravity = Gravity.TOP | Gravity.LEFT;

        Point screenSize = Utility.getDisplaySize(getApplicationContext());
        param.height = screenSize.y / 2;

        final WindowManager windowManager = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);


        windowManager.addView(widgetView, param);
        widgetView.setOnTouchListener(new View.OnTouchListener() {
            private int initialX;
            private int initialY;
            private float initialTouchX;
            private float initialTouchY;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        initialX = param.x;
                        initialY = param.y;
                        initialTouchX = event.getRawX();
                        initialTouchY = event.getRawY();
                        return true;
                    case MotionEvent.ACTION_UP:
                        return true;
                    case MotionEvent.ACTION_MOVE:
                        param.x = initialX + (int) (event.getRawX() - initialTouchX);
                        param.y = initialY + (int) (event.getRawY() - initialTouchY);
                        windowManager.updateViewLayout(widgetView, param);

                        return true;
                }
                return false;
            }
        });
    }

    private void showRateWidget() {
        Log.e("TAG", "showRateWidget widget");
        if (widgetViewRate == null)
            return;

        final WindowManager.LayoutParams param = new WindowManager.LayoutParams();
        param.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
        param.format = PixelFormat.RGBA_8888;
        param.type = WindowManager.LayoutParams.TYPE_SYSTEM_ALERT;
        param.gravity = Gravity.BOTTOM;

        Point screenSize = Utility.getDisplaySize(getApplicationContext());
        param.height = screenSize.y / 2;

        final WindowManager windowManager = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);

        try {
            windowManager.addView(widgetViewRate, param);
        } catch (Exception e) {
            e.printStackTrace();
        }

//        widgetViewRate.setOnTouchListener(new View.OnTouchListener() {
//            private int initialX;
//            private int initialY;
//            private float initialTouchX;
//            private float initialTouchY;
//
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                switch (event.getAction()) {
//                    case MotionEvent.ACTION_DOWN:
//                        initialX = param.x;
//                        initialY = param.y;
//                        initialTouchX = event.getRawX();
//                        initialTouchY = event.getRawY();
//                        return true;
//                    case MotionEvent.ACTION_UP:
//                        return true;
//                    case MotionEvent.ACTION_MOVE:
//                        param.x = initialX + (int) (event.getRawX() - initialTouchX);
//                        param.y = initialY + (int) (event.getRawY() - initialTouchY);
//                        windowManager.updateViewLayout(widgetViewRate, param);
//
//                        return true;
//                }
//                return false;
//            }
//        });

    }


    private void showSmallWidget() {
        Log.e("TAG", "showSmallWidget widget");
        if (widgetViewSmall == null)
            return;

        final WindowManager.LayoutParams param = new WindowManager.LayoutParams();
        param.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
        param.format = PixelFormat.RGBA_8888;
        param.type = WindowManager.LayoutParams.TYPE_SYSTEM_ALERT;
        param.gravity = Gravity.BOTTOM | Gravity.RIGHT;
        param.width = 200;
        param.height = 200;

//        Point screenSize = Utility.getDisplaySize(getApplicationContext());
//        param.height = screenSize.y / 2;

        final WindowManager windowManager = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);

        windowManager.addView(widgetViewSmall, param);

        widgetViewSmall.setOnTouchListener(new View.OnTouchListener() {
            private int initialX;
            private int initialY;
            private float initialTouchX;
            private float initialTouchY;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        initialX = param.x;
                        initialY = param.y;
                        initialTouchX = event.getRawX();
                        initialTouchY = event.getRawY();
                        return true;
                    case MotionEvent.ACTION_UP:
                        return true;
                    case MotionEvent.ACTION_MOVE:
                        param.x = initialX + (int) (event.getRawX() - initialTouchX);
                        param.y = initialY + (int) (event.getRawY() - initialTouchY);
                        windowManager.updateViewLayout(widgetView, param);

                        return true;
                }
                return false;
            }
        });
    }

    private void hideBigWidget() {
        Log.e("TAG", "hideBigWidget widget");

        final WindowManager windowManager = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);

        try {
            ((TextView) widgetView.findViewById(R.id.activity_call_number)).setText("");
            ((TextView) widgetView.findViewById(R.id.activity_call_name)).setText("");
            ((TextView) widgetView.findViewById(R.id.activity_call_company)).setText("");

            windowManager.removeView(widgetView);
        } catch (Exception e) {
            e.printStackTrace();
        }
        widgetView = null;
    }

    private void hideSmallWidget() {
        Log.e("TAG", "hideSmallWidget widget");

        final WindowManager windowManager = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);

        try {
            windowManager.removeView(widgetViewSmall);
        } catch (Exception e) {
            e.printStackTrace();
        }
        widgetViewSmall = null;
    }

    private void hideRateWidget() {
        Log.e("TAG", "hideRateWidget widget");

        final WindowManager windowManager = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);

        try {
            windowManager.removeView(widgetViewRate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        widgetViewRate = null;
    }


    @SuppressWarnings({"rawtypes", "unchecked"})
    private void disconnectPhoneItelephony(Context context) {
        ITelephony telephonyService;
        TelephonyManager telephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        try {
            Class c = Class.forName(telephony.getClass().getName());
            Method m = c.getDeclaredMethod("getITelephony");
            m.setAccessible(true);
            telephonyService = (ITelephony) m.invoke(telephony);
            telephonyService.endCall();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void createFileView(String path, ContentType contentType) {
        //проверка если путь существует
        int viewId = filesList.size(); //next element id
        String ivTag = "image_view" + viewId;
        String rlTag = "relative_layout" + viewId;

        RelativeLayout relativeLayout = new RelativeLayout(getApplicationContext());
        RelativeLayout.LayoutParams relativeParams = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        relativeLayout.setPadding(5, 5, 5, 5);
        relativeLayout.setTag(rlTag);

        ImageView imageView = new ImageView(getApplicationContext());
        imageView.setLayoutParams(new RelativeLayout.LayoutParams(70, 70));
        imageView.setTag(ivTag);

        Bitmap thumbImage = null;
        switch (contentType) {
            case IMAGE_TYPE:
                thumbImage = ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(path), 64, 64);
                imageView.setImageBitmap(thumbImage);
                break;
            case VIDEO_TYPE:
                thumbImage = ThumbnailUtils.createVideoThumbnail(path, MediaStore.Images.Thumbnails.MINI_KIND);
                imageView.setImageBitmap(thumbImage);
                break;
            case FILE_TYPE:
                imageView.setImageResource(R.drawable.ic_file);
                break;
        }

        relativeLayout.addView(imageView);

        ((LinearLayout) widgetView.findViewById(R.id.ll_files_to_send)).addView(relativeLayout, relativeParams);

        filesList.add(new FileModel(path, ivTag, rlTag, contentType));
    }

    @SuppressLint("ParcelCreator")
    public class PickerReceiver extends ResultReceiver {
        private static final int RESULT_OK = 1;
        public static final int RESULT_PHOTO = 101;
        public static final int RESULT_VIDEO = 102;
        public static final int RESULT_FILE = 103;
        public static final int RESULT_DEFAULT = 104;
        public static final String KEY_RECEIVER = "KEY_RECEIVER";
        public static final String KEY_PHOTO = "KEY_PHOTO";
        public static final String KEY_VIDEO = "KEY_VIDEO";
        public static final String KEY_FILE = "KEY_FILE";


        public PickerReceiver() {
            super(null);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            if (widgetView != null) {
                widgetView.setVisibility(View.VISIBLE);
            }

            String path;
            //String newPath;
            switch (resultCode) {
                case RESULT_PHOTO:
                    path = resultData.getString(KEY_PHOTO);
                    Log.d("camera_test", "path to photo : " + path);
                    //newPath = getFileNameByUri(getApplicationContext(), Uri.parse(path));
                    //Log.d("camera_test", "newPath to file : " + newPath);
                    createFileView(path, ContentType.IMAGE_TYPE);
                    break;
                case RESULT_VIDEO:
                    path = resultData.getString(KEY_VIDEO);
                    Log.d("camera_test", "path to video : " + path);
                    createFileView(path, ContentType.VIDEO_TYPE);
                    break;
                case RESULT_FILE:
                    path = resultData.getString(KEY_FILE);
                    //newPath = getFileNameByUri(getApplicationContext(), Uri.parse(path));
                    Log.d("camera_test", "path to file : " + path);
                    //Log.d("camera_test", "newPath to file : " + newPath);
                    createFileView(path, ContentType.FILE_TYPE);
                    break;
                case RESULT_DEFAULT:

                    break;
                default:

                    break;
            }
        }
    }

    public static String getFileNameByUri(Context context, Uri uri)
    {
        String fileName="unknown";//default fileName
        Uri filePathUri = uri;
        if (uri.getScheme().toString().compareTo("content")==0)
        {
            Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
            if (cursor.moveToFirst())
            {
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);//Instead of "MediaStore.Images.Media.DATA" can be used "_data"
                filePathUri = Uri.parse(cursor.getString(column_index));
                fileName = filePathUri.getLastPathSegment().toString();
            }
        }
        else if (uri.getScheme().compareTo("file")==0)
        {
            fileName = filePathUri.getLastPathSegment().toString();
        }
        else
        {
            fileName = fileName+"_"+filePathUri.getLastPathSegment();
        }
        return fileName;
    }

    class AsyncUpload extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            widgetView.findViewById(R.id.tv_send_files).setVisibility(View.GONE);
            widgetView.findViewById(R.id.pb_loading_files).setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
//            FileUploadService service = ServiceGenerator.createService(FileUploadService.class);
            FileUploadService service = ServiceGenerator.createServiceAuth(
                    FileUploadService.class,
                    PreferencesData.getLoginName(getApplicationContext()),
                    PreferencesData.getLoginPassword(getApplicationContext())
            );
            HashMap<String, RequestBody> images = new HashMap<>();
            HashMap<String, RequestBody> videos = new HashMap<>();
            HashMap<String, RequestBody> files = new HashMap<>();
            RequestBody requestBody = null;
            File file = null;
            int countImages = 0;
            int countVideos = 0;
            int countFiles = 0;
            for (final FileModel fileModel : filesList) {
                file = new File(fileModel.getPath());
                requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                switch (fileModel.getContentType()) {
                    case IMAGE_TYPE:
                        images.put("images[" + countImages + "]\"; filename=\"" + file.getName(), requestBody);
                        countImages++;
                        break;
                    case VIDEO_TYPE:
                        videos.put("videos[" + countVideos + "]\"; filename=\"" + file.getName(), requestBody);
                        countVideos++;
                        break;
                    case FILE_TYPE:
                        files.put("files[" + countFiles + "]\"; filename=\"" + file.getName(), requestBody);
                        countFiles++;
                        break;
//                    default:
//                        files.put("file\"; filename=\"" + file.getName(), requestBody);
//                        break;
                }
            }
            String titleString = "Files from Reaction app";
            RequestBody title = RequestBody.create(MediaType.parse("multipart/form-data"), titleString);

            String descriptionString = "Reaction app user files";
            RequestBody description = RequestBody.create(MediaType.parse("multipart/form-data"), descriptionString);

            retrofit2.Call<FileUploadService.FileResponse> call = service.upload(images, videos, files, title, description);

            try {
                FileUploadService.FileResponse fileResponse = call.execute().body();
                Log.v("Upload", "file response code = " + fileResponse.code);
                //publishProgress("Загружено");
                publishProgress(fileResponse.code);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            Log.v("Upload", "value from onProgressUpdate " + values[0]);

            AlertCustomDialog stationsDialog = new AlertCustomDialog(getApplicationContext(), "Use this code to access : " + values[0]);
            stationsDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
            stationsDialog.show();

            if (widgetView != null) {
                widgetView.findViewById(R.id.pb_loading_files).setVisibility(View.GONE);
                widgetView.findViewById(R.id.tv_send_files).setVisibility(View.VISIBLE);
            }

            if (!values[0].equals("Upload error")) {
                if (widgetView != null) {
                    ((LinearLayout) widgetView.findViewById(R.id.ll_files_to_send)).removeAllViews();
                }
                filesList.clear();
            }
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            Log.v("Upload", "on post execute");
        }
    }

    class AsyncNetwork extends AsyncTask<String, String, Boolean> {
        String username = "";
        String company = "";
        String phone = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            initSmallWidget();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            phone = params[0];
            UserService userService =
                    ServiceGenerator.createServiceAuth(
                            UserService.class,
                            PreferencesData.getLoginName(getApplicationContext()),
                            PreferencesData.getLoginPassword(getApplicationContext())
                    );
            try {
                retrofit2.Call<List<UserService.CompanyJO>> call =
                        userService.listUser("{\"phone\":\"" + phone.replace("+", "") + "\"}");

                Log.d("retrofit", "make query  =  " + "{\"phone\":\"" + phone.replace("+", "") + "\"}");
                List<UserService.CompanyJO> companyJOList = call.execute().body();
                UserService.CompanyJO companyJO = companyJOList.get(0);
                Log.d("retrofit", "userName: " + companyJO.contact_name + "   companyJO: " + companyJO.company_name);
                username = companyJO.contact_name;
                this.company = companyJO.company_name;
                from = companyJO.from;
            } catch (IOException e) {
                Log.e("retrofit", "ioe doInBackground: " + e.getMessage());
                return false;
            } catch (Exception e) {
                Log.e("retrofit", "e doInBackground: " + e.getMessage());
                return false;
            }
            return true;

        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if (aBoolean) {
                hideSmallWidget();
                initBigWidget(phone, username, company);
//                PreferencesData.setLastCallManagerID(getApplicationContext(), Long.parseLong(id));

                Notification notification;
                Intent notificationIntent = new Intent();
                PendingIntent pendingIntent = PendingIntent.getActivity(CallService.this, 0, notificationIntent, 0);

                Notification.Builder builder = new Notification.Builder(CallService.this);

                builder.setAutoCancel(false);
                builder.setTicker("");
                builder.setContentTitle(getText(R.string.app_name));
                builder.setContentText("Calling " + (phone == null ? "" : phone));
                builder.setSmallIcon(R.drawable.ic_launcher_shape);
                builder.setLargeIcon(BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.mipmap.ic_launcher));
                builder.setContentIntent(pendingIntent);
                builder.setOngoing(true);
//                    builder.setSubText(getContactName(phoneNumber, getApplicationContext()));   //API level 16
                builder.setSubText(username);   //API level 16
                //builder.setNumber(1);
                builder.build();

//                notification = builder.getNotification();
                notification = builder.build();
                startForeground(1010, notification);
            } else {
                hideSmallWidget();
            }
        }
    }


    class AsyncRate extends AsyncTask<String, String, Boolean> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            Log.e(TAG, "rate id : " + params[0]);
            String callId = params[0];
            String rate = params[1];
            RateService rateService =
                    ServiceGenerator.createServiceAuth(
                            RateService.class,
                            PreferencesData.getLoginName(getApplicationContext()),
                            PreferencesData.getLoginPassword(getApplicationContext())
                    );

            retrofit2.Call<ResponseBody> call = rateService.postRating(
                    Integer.valueOf(callId),
                    new RateService.RateRequest(rate)
            );
            try {
                ResponseBody responseBody = call.execute().body();
                Log.e(TAG, "post Rate response : " + responseBody.string());
                return true;
            } catch (Exception e) {
                Log.e(TAG, "e doInBackground: " + e.getMessage());
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            hideRateWidget();
            CallService.this.stopForeground(true);
        }
    }

    public enum ContentType {
        IMAGE_TYPE, VIDEO_TYPE, FILE_TYPE
    }

    class FileModel {
        private String path;
        private String ivTag;
        private String rlTag;
        private ContentType contentType;

        public FileModel(String path, String ivTag, String rlTag, ContentType contentType) {
            this.path = path;
            this.ivTag = ivTag;
            this.rlTag = rlTag;
            this.contentType = contentType;
        }

        public String getPath() {
            return path;
        }

        public String getIvTag() {
            return ivTag;
        }

        public String getRlTag() {
            return rlTag;
        }

        public ContentType getContentType() {
            return contentType;
        }
    }

    private String getContactName(String number, Context context) {
        String contactNumber = "";
        String contactName = null;

        // // define the columns I want the query to return
        String[] projection = new String[]{
                ContactsContract.PhoneLookup.DISPLAY_NAME,
                ContactsContract.PhoneLookup.NUMBER,
                ContactsContract.PhoneLookup.HAS_PHONE_NUMBER};

        // encode the phone number and build the filter URI
        Uri contactUri = Uri.withAppendedPath(
                ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                Uri.encode(number));

        // query time
        Cursor cursor = context.getContentResolver().query(contactUri,
                projection, null, null, null);
        // querying all contacts = Cursor cursor =
        // context.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI,
        // projection, null, null, null);

        if (cursor.moveToFirst()) {
            contactName = cursor.getString(cursor
                    .getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
        }
        cursor.close();
        return contactName == null ? "..." : contactName;

    }

}

