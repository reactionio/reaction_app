package ngse.reactionapp.Services;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.RemoteViews;

import com.NGSE.RequestLibrary.Query;
import com.NGSE.RequestLibrary.RequestInterface;
import com.NGSE.RequestLibrary.RequestParameters;

import org.apache.http.Header;
import org.apache.http.message.BasicHeader;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import ngse.reactionapp.Activities.MainActivity;
import ngse.reactionapp.Data.Notification;
import ngse.reactionapp.Data.PreferencesData;
import ngse.reactionapp.R;
import ngse.reactionapp.Utility.Constants;
import ngse.reactionapp.Utility.CustomRequestHandler;
import ngse.reactionapp.Utility.JsonParser;


public class CheckService extends Service {

    public static final int NOTIFICATION = 123;
    static final String TAG = CheckService.class.getName();
    Context context;
    private NotificationManager notificationManager;
    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //DebugUtility.logE(context, TAG, "onStartCommand");
        Log.e("TAG", "CheckService onStartCommand");
        checkSend();

        return START_STICKY;
    }

    public void checkSend() {
        int timeBetweenChecks = 1000 * 60;
        if (PreferencesData.getLoginID(context) != null && PreferencesData.getEnableNotifications(context)) {
            loadData();
        }
        else
            return;
        Handler h = new Handler();
        h.postDelayed(new Runnable() {
            public void run() {
                checkSend();
            }
        }, timeBetweenChecks);
    }

    private void loadData() {

        RequestParameters requestParameters = new RequestParameters(Query.TYPE_REQUEST.GET, -1, Constants.ROOT_NOTIFY);
        requestParameters.setHeader(Arrays.asList(new BasicHeader("X-Username", PreferencesData.getLoginName(this)),
                new BasicHeader("X-Password", PreferencesData.getLoginPassword(this))));
        Query.getInstance().setQuery(this, new CustomRequestHandler(this, false) {
            @Override
            public void processingRequest(RequestInterface requestInterface, Object response, int code, int type, Header[] headers) {
                super.processingRequest(requestInterface, response, code, type, headers);
                Log.e("TAG","json= "+ data);
                if (!errorInfo.isResult()) {
                    requestInterface.error(errorInfo);
                } else {
                    List<Notification> notifications = JsonParser.parseNotifications(data);
                    requestInterface.finish(notifications);
                }

            }
        }, new RequestInterface() {
            @Override
            public void finish(Object obj) {
                List<Notification> notifications = (ArrayList<Notification>) obj;
                for (Notification notification:notifications)
                {
                    addNotification(notification);
                }

            }

            @Override
            public void error(Object obj) {
            }
        }, requestParameters);


    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }
    private void addNotification(Notification notificationItem) {

        RemoteViews views = new RemoteViews(getPackageName(), R.layout.notification);
        // views.setImageViewBitmap(R.id.icon, icon);
        views.setImageViewResource(R.id.notification_icon, R.mipmap.ic_launcher);

        views.setTextViewText(R.id.notification_body, notificationItem.getNotification());
        views.setTextViewText(R.id.notification_date, notificationItem.getCreated());

        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        android.app.Notification notification = new android.app.Notification();
        notification.contentView = views;
        notification.flags |= android.app.Notification.FLAG_AUTO_CANCEL;//FLAG_ONGOING_EVENT;
        notification.icon = R.mipmap.ic_launcher;
        notification.sound=soundUri;
        //notification.sound = Uri.parse("android.resource://com.example.serviceproject/" + R.raw.kalimba);

        Intent notificationIntent = new Intent(getApplicationContext(), MainActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent intent = PendingIntent.getActivity(getApplicationContext(), 0,
                notificationIntent, 0);
        notification.contentIntent = intent;

        /*status.contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(MainActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK), 0);*/
        Random random=new Random();
        notificationManager.notify(random.nextInt(), notification);
        //startForeground(random.nextInt(), notification);
    }

}
