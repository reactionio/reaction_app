package ngse.reactionapp.Services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import ngse.reactionapp.Data.PreferencesData;

public class ServiceStarter extends BroadcastReceiver
{

	@Override
	public void onReceive(Context context, Intent intent)
	{
		Intent service1= new Intent(context, CallService.class);
		context.startService(service1);

//		context.startService(service1);
		if (PreferencesData.getEnableNotifications(context)) {
			Intent service = new Intent(context, CheckService.class);
			context.startService(service);


		}
	}

}
