package ngse.reactionapp.Services.NetworkServices;

import android.annotation.TargetApi;
import android.content.ClipData;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.ResultReceiver;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.nononsenseapps.filepicker.FilePickerActivity;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import ngse.reactionapp.R;
import ngse.reactionapp.Services.CallService;
import ngse.reactionapp.Utility.FileUtils;

public class PickerActivity extends AppCompatActivity {
    private static final String TAG = "camera_test";
    private static final int RESULT_OK = 1;
    private static final int REQUEST_PHOTO = 7001;
    private static final int REQUEST_VIDEO = 7002;
    private static final int REQUEST_FILE = 7003;
    private static final int REQUEST_FILE_KITKAT = 7004;

    private String photoPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        switch (getIntent().getAction()) {
            case CallService.ACTION_PICK_PHOTO:
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                // Ensure that there's a camera activity to handle the intent
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    // Create the File where the photo should go
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                    } catch (IOException ex) {
                        Log.e(TAG, "Error occurred while creating the File");
                        // Error occurred while creating the File
                    }
                    // Continue only if the File was successfully created
                    if (photoFile != null) {
                        Log.d(TAG, "make intent");
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                Uri.fromFile(photoFile));
                        startActivityForResult(takePictureIntent, REQUEST_PHOTO);
                    }
                    Log.d(TAG, "onCreate: Camera starts");
                }
                break;
            case CallService.ACTION_PICK_VIDEO:
                Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
                    takeVideoIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 30000);
                    takeVideoIntent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
                    startActivityForResult(takeVideoIntent, REQUEST_VIDEO);
                }
                break;
            case CallService.ACTION_PICK_FILE:

                if (Build.VERSION.SDK_INT < 19){
                    Intent intent = new Intent();
                    intent.setType("*/*");
                    //intent.setType("text/plain");
                    //intent.setType("application/pdf");
                    //intent.putExtra("CONTENT_TYPE", "image/*;text/*;application/pdf");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    intent.addCategory(Intent.CATEGORY_OPENABLE);
                    startActivityForResult(Intent.createChooser(intent, "Select a File to Upload"),REQUEST_FILE);
                } else {
                    Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                    intent.addCategory(Intent.CATEGORY_OPENABLE);
                    intent.setType("*/*");
                    startActivityForResult(intent, REQUEST_FILE_KITKAT);
                }

                /*// ACTION_OPEN_DOCUMENT is the intent to choose a file via the system's file browser.
                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);

                // Filter to only show results that can be "opened", such as a file (as opposed to a list
                // of contacts or timezones)
                intent.addCategory(Intent.CATEGORY_OPENABLE);

                // Filter to show only images, using the image MIME data type.
                // If one wanted to search for ogg vorbis files, the type would be "audio/ogg".
                // To search for all documents available via installed storage providers, it would be
                // "**///*".
                //intent.setType("image*//*");



                //Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                //intent.setType("**/*//*");
                //intent.addCategory(Intent.CATEGORY_OPENABLE);
                /*try {
                    startActivityForResult(intent, REQUEST_FILE);
                    //startActivityForResult(
                    //        Intent.createChooser(intent, "Select a File to Upload"),
                     //       REQUEST_FILE);
                } catch (android.content.ActivityNotFoundException ex) {

                    Intent i = new Intent(this, FilePickerActivity.class);
                    i.putExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, false);
                    i.putExtra(FilePickerActivity.EXTRA_ALLOW_CREATE_DIR, false);
                    i.putExtra(FilePickerActivity.EXTRA_MODE, FilePickerActivity.MODE_FILE);
                    i.putExtra(FilePickerActivity.EXTRA_START_PATH, Environment.getExternalStorageDirectory().getPath());
                    startActivityForResult(i, REQUEST_FILE);
                }*/

                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "resultCode = " + resultCode);
//        if (resultCode == 0) {
//            return;
//        }
        ResultReceiver receiver;
        Bundle resultData;
        try {
            switch (requestCode) {
                case REQUEST_PHOTO:
                    Log.d(TAG, "on activity result REQUEST PHOTO");
                    Log.d(TAG, "photo path = " + photoPath);
                    receiver = getIntent().getParcelableExtra(CallService.PickerReceiver.KEY_RECEIVER);
                    resultData = new Bundle();
                    resultData.putString(CallService.PickerReceiver.KEY_PHOTO, photoPath);
                    receiver.send(CallService.PickerReceiver.RESULT_PHOTO, resultData);
                    galleryAddPic();
                    finish();
                    break;
                case REQUEST_VIDEO:
                    Uri videoUri = data.getData();
                    String videoPath = getRealPathFromURI(videoUri);
                    Log.d(TAG, "on activity result REQUEST VIDEO");
                    receiver = getIntent().getParcelableExtra(CallService.PickerReceiver.KEY_RECEIVER);
                    resultData = new Bundle();
                    resultData.putString(CallService.PickerReceiver.KEY_VIDEO, videoPath);
                    receiver.send(CallService.PickerReceiver.RESULT_VIDEO, resultData);
                    //Toast.makeText(this, videoPath, Toast.LENGTH_LONG).show();
                    finish();
                    break;
                case REQUEST_FILE:
                    if (data.getBooleanExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, false)) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            ClipData clip = data.getClipData();

                            if (clip != null) {
                                for (int i = 0; i < clip.getItemCount(); i++) {
                                    Uri uri = clip.getItemAt(i).getUri();
                                    //File file = FileUtils.getFileForUri(uri);
                                    //Uri fileUri = Uri.fromFile(file);
                                    //String transUri = getRealPathFromURI(getApplicationContext(), uri);
                                    //Log.d(TAG, "transUri: " + fileUri.getPath());
                                    // Do something with the URI
                                    Log.d(TAG, "on activity result REQUEST FILE MULTIPLE !!!");
                                    receiver = getIntent().getParcelableExtra(CallService.PickerReceiver.KEY_RECEIVER);
                                    resultData = new Bundle();
                                    //resultData.putString(CallService.PickerReceiver.KEY_PHOTO, photoPath);
                                    //receiver.send(CallService.PickerReceiver.RESULT_PHOTO, resultData);
                                    resultData.putString(CallService.PickerReceiver.KEY_FILE, String.valueOf(uri).replace("file://", "").replace("%20", " "));
                                    receiver.send(CallService.PickerReceiver.RESULT_FILE, resultData);
                                }
                            }
                        }
                    } else {
                        Uri uri = data.getData();
                        //File file = FileUtils.getFileForUri(uri);
                        //Uri fileUri = Uri.fromFile(file);
                        Log.d(TAG, "on activity result REQUEST FILE");
                        String transUri = getPath(getApplicationContext(), uri);
                        Log.d(TAG, "transUri: " + transUri);
                        receiver = getIntent().getParcelableExtra(CallService.PickerReceiver.KEY_RECEIVER);
                        resultData = new Bundle();
                        //resultData.putString(CallService.PickerReceiver.KEY_PHOTO, photoPath);
                        //receiver.send(CallService.PickerReceiver.RESULT_PHOTO, resultData);
                        resultData.putString(CallService.PickerReceiver.KEY_FILE, transUri.replace("file://", "").replace("%20", " "));
                        receiver.send(CallService.PickerReceiver.RESULT_FILE, resultData);
                    }
                    finish();
                    break;

                case REQUEST_FILE_KITKAT:
                    Uri uri = data.getData();
                    final int takeFlags = data.getFlags()
                            & (Intent.FLAG_GRANT_READ_URI_PERMISSION
                            | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                    // Check for the freshest data.
                    getContentResolver().takePersistableUriPermission(uri, takeFlags);
                    //File file = FileUtils.getFileForUri(uri);
                    //Uri fileUri = Uri.fromFile(file);
                    Log.d(TAG, "on activity result REQUEST FILE KITKAT AND HIRE");
                    String transUri = getPath(getApplicationContext(), uri);
                    Log.d(TAG, "transUri: " + transUri);
                    receiver = getIntent().getParcelableExtra(CallService.PickerReceiver.KEY_RECEIVER);
                    resultData = new Bundle();
                    resultData.putString(CallService.PickerReceiver.KEY_FILE, transUri.replace("file://", "").replace("%20", " "));
                    receiver.send(CallService.PickerReceiver.RESULT_FILE, resultData);
                    finish();
                    break;


            }
        } catch (Exception e) {
            Log.e(TAG, "onActivityResult: ERROR" + e.getMessage());
            receiver = getIntent().getParcelableExtra(CallService.PickerReceiver.KEY_RECEIVER);
            resultData = new Bundle();
            receiver.send(CallService.PickerReceiver.RESULT_DEFAULT, resultData);
            finish();
        }
    }

    /**
     * Get a file path from a Uri. This will get the the path for Storage Access
     * Framework Documents, as well as the _data field for the MediaStore and
     * other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri The Uri to query.
     * @author paulburke
     */
    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[] {
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri The Uri to query.
     * @param selection (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "Reaction_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        photoPath = image.getAbsolutePath();
        return image;
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(photoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
}
