package ngse.reactionapp.Services.TestFiles;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ListView;
import android.widget.TextView;

import ngse.reactionapp.R;

/**
 * Created by Maymeskul on 18.04.2016.
 */
public class AlertCustomDialog extends Dialog {
    Context context;
    String text;
    public AlertCustomDialog(Context context, String code) {
        super(context);
        this.context = context;
        this.text = code;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_custom_alert);

        ((TextView) findViewById(R.id.tv_alert_code)).setText(text);

        findViewById(R.id.btn_alert_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

    }
}