package ngse.reactionapp.Activities;

import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;

import com.NGSE.RequestLibrary.Query;
import com.NGSE.RequestLibrary.RequestInterface;
import com.NGSE.RequestLibrary.RequestParameters;

import org.apache.http.Header;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.message.BasicHeader;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnFocusChange;
import butterknife.OnTextChanged;
import ngse.reactionapp.Adapters.SuggestionsAdapter;
import ngse.reactionapp.Adapters.TicketFilesAdapter;
import ngse.reactionapp.Data.Company;
import ngse.reactionapp.Data.PreferencesData;
import ngse.reactionapp.Data.TicketFile;
import ngse.reactionapp.Enums.FILE_TYPE;
import ngse.reactionapp.R;
import ngse.reactionapp.Utility.Constants;
import ngse.reactionapp.Utility.CustomRequestHandler;
import ngse.reactionapp.Utility.DialogUtility;
import ngse.reactionapp.Utility.JsonParser;
import ngse.reactionapp.Utility.Utility;


public class ComposeTicketActivity extends AppCompatActivity {
    public static final String TAG = ComposeTicketActivity.class.getName();
    private static final String EXTRA_COMPANY_ID = "id";
    private static final int IMAGE_CAPTURE = 100;
    private static final int VIDEO_CAPTURE = 101;
    private static final int UPLOAD_FILE = 102;
    @Bind(R.id.activity_compose_ticket_toolbar)
    Toolbar activity_compose_ticket_toolbar;
    @Bind(R.id.activity_compose_ticket_subject)
    EditText activity_compose_ticket_subject;
    @Bind(R.id.activity_compose_ticket_company)
    AutoCompleteTextView activity_compose_ticket_company;
    @Bind(R.id.activity_compose_ticket_info)
    EditText activity_compose_ticket_info;
    @Bind(R.id.activity_compose_ticket_file_list)
    RecyclerView activity_compose_ticket_file_list;
    private MultipartEntityBuilder multipartEntity = MultipartEntityBuilder.create();
    private Drawable defaultNavigationIcon = null;
    private List<Company> companies = new ArrayList<>();
    private SuggestionsAdapter suggestionsAdapter;
    private List<TicketFile> ticketFiles = new ArrayList<>();
    private TicketFilesAdapter ticketFilesAdapter;
    private String lastPhotoPath;
    private long companyId;
    public static void newInstance(AppCompatActivity activity) {
        Intent intent = new Intent(activity, ComposeTicketActivity.class);
        ActivityCompat.startActivity(activity, intent, null);
    }

    public static void newInstance(AppCompatActivity activity, long companyId) {
        Intent intent = new Intent(activity, ComposeTicketActivity.class);
        intent.putExtra(EXTRA_COMPANY_ID, companyId);
        ActivityCompat.startActivity(activity, intent, null);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compose_ticket);
        ButterKnife.bind(this);
        companyId=getIntent().getLongExtra(EXTRA_COMPANY_ID,-1);
        setSupportActionBar(activity_compose_ticket_toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity_compose_ticket_file_list.setLayoutManager(new LinearLayoutManager(this));
        ticketFilesAdapter = new TicketFilesAdapter(this, ticketFiles);
        activity_compose_ticket_file_list.setAdapter(ticketFilesAdapter);
        defaultNavigationIcon = activity_compose_ticket_toolbar.getNavigationIcon();
        loadData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (isFill())
                    addTicket();
                else
                    supportFinishAfterTransition();
                // NavUtils.navigateUpFromSameTask(this);
                return true;
            case R.id.menu_action_take_picture:
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                    } catch (IOException ex) {
                    }
                    if (photoFile != null) {
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                Uri.fromFile(photoFile));
                        startActivityForResult(takePictureIntent, IMAGE_CAPTURE);
                    }
                }
             /*   Intent cameraIntent = new Intent(
                        android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, IMAGE_CAPTURE);*/
                return true;

            case R.id.menu_action_record_video:
                Intent videoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                startActivityForResult(videoIntent, VIDEO_CAPTURE);
                return true;
            case R.id.menu_action_upload_file:
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select File"), UPLOAD_FILE);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        lastPhotoPath =  image.getAbsolutePath();
        return image;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        activity_compose_ticket_toolbar.setTitle("");
        getMenuInflater().inflate(R.menu.menu_compose_ticket, menu);

        return super.onCreateOptionsMenu(menu);
    }




    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK)
            return;
        if (requestCode == IMAGE_CAPTURE) {
            File file = new File(lastPhotoPath);
            ticketFiles.add(new TicketFile(FILE_TYPE.IMAGE, file));
            ticketFilesAdapter.notifyDataSetChanged();
        }
        if (data==null) {
            return;
        }
        if (requestCode == VIDEO_CAPTURE) {
            Log.e("TAG", "data!!!!!!!!!!!!!!!!!!!!! =" + data.getData());
            Uri uri = data.getData();
            String path = getPath(uri);
            File file = new File(path);
            if (file.exists()) {
                ticketFiles.add(new TicketFile(FILE_TYPE.VIDEO, file));
                ticketFilesAdapter.notifyDataSetChanged();
            } else {
                Log.e(TAG, "File " + data.getData() + " doesn't exist!");
            }
        }
        if (requestCode == UPLOAD_FILE) {
            Uri uri = data.getData();
            String path = getPath(uri);

            File file = new File(path);
            if (uri.toString().contains("com.google.android"))
                try {
                    file = Utility.downloadFromDrive(ComposeTicketActivity.this,uri);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            Log.e("TAG", "path!!!!!!!!!!!!!!!!!!!!! =" + file.getAbsolutePath());
            String filenameArray[] = file.getAbsolutePath().split("\\.");
            String extension = filenameArray[filenameArray.length - 1];
            if (!Arrays.asList("doc", "docx", "xls", "pdf", "zip", "rar").contains(extension)) {
                Utility.showToast(this, "only " + Arrays.asList("doc", "docx", "xls", "pdf", "zip", "rar").toString());
                return;
            }

            if (file.exists()) {
                ticketFiles.add(new TicketFile(FILE_TYPE.DOC, file));
                ticketFilesAdapter.notifyDataSetChanged();
            } else {
                Log.e(TAG, "File " + data.getData() + " doesn't exist!");
            }
        }

    }

    public String getPath(Uri uri) {
        String path = null;
        String[] projection = {MediaStore.Files.FileColumns.DATA};
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);

        if (cursor == null) {
            path = uri.getPath();
        } else {
            cursor.moveToFirst();
            int column_index = cursor.getColumnIndexOrThrow(projection[0]);
            path = cursor.getString(column_index);
            cursor.close();
        }

        return ((path == null || path.isEmpty()) ? (uri.getPath()) : path);
    }

    @OnFocusChange(R.id.activity_compose_ticket_company)
    void onFocusChangeCompany(boolean focused) {
        if (!focused)
            return;
        String company = activity_compose_ticket_company.getText().toString();
        if (company.contains("Gilmor"))
            activity_compose_ticket_company.setText("");
    }

    @OnTextChanged(R.id.activity_compose_ticket_subject)
    void onTextChangedsubject() {
        checkFill();
    }

    @OnTextChanged(R.id.activity_compose_ticket_company)
    void onTextChangedCompany() {
        checkFill();
    }

    @OnTextChanged(R.id.activity_compose_ticket_info)
    void onTextChangedInfo() {
        checkFill();
    }

    private void checkFill() {
        if (isFill())
            activity_compose_ticket_toolbar.setNavigationIcon(R.drawable.ok_blue);
        else if (defaultNavigationIcon != null)
            activity_compose_ticket_toolbar.setNavigationIcon(defaultNavigationIcon);

    }

    private boolean isFill() {
        String subject = activity_compose_ticket_subject.getText().toString();
        String info = activity_compose_ticket_info.getText().toString();
        if (subject.length() > 0 &&  info.length() > 0)
            return true;
        return false;
    }
    private boolean checkSelectCompany() {
        String company = activity_compose_ticket_company.getText().toString();
        for (Company companyItem : companies) {
            if (companyItem.getName().equals(company))
                return true;
        }
        return false;
    }
    void addTicket() {
        String subject = null;
        String company;
        long managerId =0;
        String info = null;

        try {
            subject = new String(activity_compose_ticket_subject.getText().toString().getBytes(), "utf-8");
            company = activity_compose_ticket_company.getText().toString();
            for (Company companyItem : companies) {
                if (companyItem.getName().equals(company) && companyItem.getManagers().size() > 0) {
                    managerId = companyItem.getManagers().get(0).getId();
                    break;
                }
            }

            info = new String(activity_compose_ticket_info.getText().toString().getBytes(), "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        if (!isFill()) return;

        final Dialog dialog = DialogUtility.getWaitDialog(this, getString(R.string.wait), false);
        dialog.show();
        final String url = Constants.ROOT_TICKET;
        multipartEntity = MultipartEntityBuilder.create();
        for (TicketFile ticketFile:ticketFiles)
            switch (ticketFile.getFileType()) {
                case IMAGE:
                    multipartEntity.addBinaryBody("_images[]", ticketFile.getFile());
                    break;
                case VIDEO:
                    multipartEntity.addBinaryBody("_videos[]",  ticketFile.getFile());
                    break;
                case DOC:
                    multipartEntity.addBinaryBody("_files[]",  ticketFile.getFile());
                    break;
            }
        int status=0;
        if (checkSelectCompany())
        {
            status=1;
        }
        multipartEntity.addPart("user_id", new StringBody(Long.toString(managerId), ContentType.TEXT_PLAIN));
        multipartEntity.addPart("client_id", new StringBody(PreferencesData.getLoginID(this), ContentType.TEXT_PLAIN));
        multipartEntity.addPart("title", new StringBody(subject, ContentType.TEXT_PLAIN));
        multipartEntity.addPart("description", new StringBody(info, ContentType.TEXT_PLAIN));
        multipartEntity.addPart("status", new StringBody(Integer.toString(status), ContentType.TEXT_PLAIN));

        Log.e("TAG", "url= " + url);
        Log.e("TAG", "user_id= " + Long.toString(managerId));
        Log.e("TAG", "client_id= " + PreferencesData.getLoginID(this));
        Log.e("TAG", "title= " +subject);
        Log.e("TAG", "description= " +info);
        Log.e("TAG", "status= " + Long.toString(status));

        RequestParameters requestParameters = new RequestParameters(Query.TYPE_REQUEST.MULTI_PART, -1, multipartEntity, url);
        requestParameters.setHeader(Arrays.asList(new BasicHeader("X-Username", PreferencesData.getLoginName(this)),
                new BasicHeader("X-Password", PreferencesData.getLoginPassword(this))));

        Query.getInstance().setQuery(this, new CustomRequestHandler(this, true) {
            @Override
            public void processingRequest(RequestInterface requestInterface, Object response, int code, int type, Header[] headers) {
                Utility.closeDialog(dialog);
                super.processingRequest(requestInterface, response, code, type, headers);
                if (!errorInfo.isResult()) {
                    requestInterface.error(errorInfo);
                } else {
                    Log.e("TAG", "json= " + data);
                    Dialog dialog1=DialogUtility.getAlertDialog(ComposeTicketActivity.this, getString(R.string.activity_compose_ticket_create), new DialogUtility.OnDialogButtonClickListener() {
                        @Override
                        public void onPositiveClick() {
                            supportFinishAfterTransition();
                        }

                        @Override
                        public void onNegativeClick() {

                        }
                    });
                    dialog1.show();
                }
                ticketFiles.clear();
                ticketFilesAdapter.notifyDataSetChanged();
            }
        }, new RequestInterface() {
            @Override
            public void finish(Object obj) {

            }

            @Override
            public void error(Object obj) {
            }
        }, requestParameters);

    }

    private void loadData() {
        final Dialog dialog = DialogUtility.getWaitDialog(this, getString(R.string.wait), false);
        dialog.show();
        RequestParameters requestParameters = new RequestParameters(Query.TYPE_REQUEST.GET, -1, Constants.COMPANIES_WITH_MANAGERS);
        requestParameters.setHeader(Arrays.asList(new BasicHeader("X-Username", PreferencesData.getLoginName(this)),
                new BasicHeader("X-Password", PreferencesData.getLoginPassword(this))));
        Query.getInstance().setQuery(this, new CustomRequestHandler(this, true) {
            @Override
            public void processingRequest(RequestInterface requestInterface, Object response, int code, int type, Header[] headers) {
                Utility.closeDialog(dialog);
                super.processingRequest(requestInterface, response, code, type, headers);
                Log.e("TAG", "json= " + data);
                if (!errorInfo.isResult()) {
                    requestInterface.error(errorInfo);
                } else {
                    List<Company> companies = JsonParser.parseCompanies(data);
                    if (companyId!=-1)
                    for (Company company:companies)
                    {
                        if (companyId==company.getId())
                        {
                            activity_compose_ticket_company.setText(company.getName());
                            break;
                        }
                    }
                    requestInterface.finish(companies);
                }

            }
        }, new RequestInterface() {
            @Override
            public void finish(Object obj) {
                companies.clear();
                companies.addAll((List<Company>) obj);
                suggestionsAdapter = new SuggestionsAdapter(ComposeTicketActivity.this, R.layout.list_item_suggestions, companies);
                activity_compose_ticket_company.setAdapter(suggestionsAdapter);
            }

            @Override
            public void error(Object obj) {
            }
        }, requestParameters);
    }

}