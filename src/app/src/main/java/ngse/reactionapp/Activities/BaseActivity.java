package ngse.reactionapp.Activities;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import ngse.reactionapp.Applications.MainApplication;
import ngse.reactionapp.Data.PreferencesData;
import ngse.reactionapp.R;


public class BaseActivity extends AppCompatActivity
{

	private static final String TAG = BaseActivity.class.getName();
	private long backPressed;
	protected MainApplication application;
	protected FragmentManager currentFragmentManager;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		application= (MainApplication) getApplication();
		currentFragmentManager = getSupportFragmentManager();

	}

	public FragmentManager getCurrentFragmentManager()
	{
		if (currentFragmentManager == null)
			currentFragmentManager = getSupportFragmentManager();
		return currentFragmentManager;
	}

	@Override
	protected void onDestroy()
	{
		super.onDestroy();
	}

	@Override
	protected void onStop()
	{
		super.onStop();
	}

	@Override
	protected void onPause()
	{
		super.onPause();
	}

	@Override
	public void onBackPressed()
	{

		FragmentManager currentFragmentManager = getSupportFragmentManager();
		if (currentFragmentManager.getBackStackEntryCount() == 0)
		{
			if (backPressed + 2000 > System.currentTimeMillis())
			{
				super.onBackPressed();
				PreferencesData.setShowResolvedDialog(this,true);
				finish();
			}
			else
			{
				Toast.makeText(getBaseContext(), R.string.go_exit, Toast.LENGTH_SHORT).show();
			}
			backPressed = System.currentTimeMillis();
		}
		else
		{
			super.onBackPressed();
		}
	}
	public boolean popBackStack(int count)
	{
		boolean result = false;
		FragmentManager currentFragmentManager = getSupportFragmentManager();
		if (count != -1)
		{
			for (int i = 0; i < count; i++)
			{
				if (currentFragmentManager.getBackStackEntryCount() > 0)
				{
					currentFragmentManager.popBackStack();
				}
				else
				{
					break;
				}
			}
		}
		else
		{
			if (currentFragmentManager.getBackStackEntryCount() > 0)
			{
				currentFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
			}
		}
		result = currentFragmentManager.getBackStackEntryCount() > 0;
		return result;

	}

	public boolean checkFragment(String tag)
	{
		Fragment fragment =  currentFragmentManager.findFragmentByTag(tag);
		if (fragment != null && fragment.isVisible())
		{
			return true;
		}
		return false;
	}
	
	public boolean isServiceRunning(Class<?> serviceClass) {
	    ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
	    for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
	        if (serviceClass.getName().equals(service.service.getClassName())) {
	            return true;
	        }
	    }
	    return false;
	}

}
