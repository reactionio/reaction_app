package ngse.reactionapp.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ngse.reactionapp.Data.Ticket;
import ngse.reactionapp.R;
import ngse.reactionapp.Utility.Utility;


public class ShowTicketActivity extends AppCompatActivity {

    private static final String EXTRA_TICKET = "ticket";

    @Bind(R.id.activity_show_ticket_name)
    TextView activity_show_ticket_name;
    @Bind(R.id.activity_show_ticket_time)
    TextView activity_show_ticket_time;
    @Bind(R.id.activity_show_ticket_description)
    TextView activity_show_ticket_description;
    @Bind(R.id.activity_show_ticket_toolbar)
    Toolbar activity_show_ticket_toolbar;

    private Ticket ticket;

    public static void newInstance(AppCompatActivity activity,Ticket ticket) {
        Intent intent = new Intent(activity, ShowTicketActivity.class);
        intent.putExtra(EXTRA_TICKET, ticket);
        ActivityCompat.startActivity(activity, intent, null);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_ticket);
        ButterKnife.bind(this);
        setSupportActionBar(activity_show_ticket_toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ticket = (Ticket) getIntent().getSerializableExtra(EXTRA_TICKET);
        activity_show_ticket_name.setText(ticket.getTitle());
        activity_show_ticket_time.setText(ticket.getCreatedAt());
        activity_show_ticket_description.setText(ticket.getDescription());
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                supportFinishAfterTransition();
                return true;
            case R.id.menu_action_call:
                Utility.startCall(this, ticket.getManager().getPhone(), ticket.getManager().getId());
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        activity_show_ticket_toolbar.setTitle("");
        getMenuInflater().inflate(R.menu.menu_ticket, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @OnClick(R.id.activity_show_ticket_edit)
    void editTicket() {
        EditTicketActivity.newInstance(this,ticket);
    }
}