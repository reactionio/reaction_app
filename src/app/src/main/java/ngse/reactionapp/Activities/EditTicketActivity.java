package ngse.reactionapp.Activities;

import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Toast;

import com.NGSE.RequestLibrary.Query;
import com.NGSE.RequestLibrary.RequestInterface;
import com.NGSE.RequestLibrary.RequestParameters;

import org.apache.http.Header;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.message.BasicHeader;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnFocusChange;
import butterknife.OnTextChanged;
import ngse.reactionapp.Adapters.SuggestionsAdapter;
import ngse.reactionapp.Data.Company;
import ngse.reactionapp.Data.PreferencesData;
import ngse.reactionapp.Data.Ticket;
import ngse.reactionapp.R;
import ngse.reactionapp.Utility.Constants;
import ngse.reactionapp.Utility.CustomRequestHandler;
import ngse.reactionapp.Utility.DialogUtility;
import ngse.reactionapp.Utility.JsonParser;
import ngse.reactionapp.Utility.Utility;


public class EditTicketActivity extends AppCompatActivity {
    public static final String TAG = EditTicketActivity.class.getName();
    private static final int IMAGE_CAPTURE = 100;
    private static final int VIDEO_CAPTURE = 101;
    private static final int UPLOAD_FILE = 102;
    private static final String EXTRA_TICKET = "ticket";
    @Bind(R.id.activity_compose_ticket_toolbar)
    Toolbar activity_compose_ticket_toolbar;
    @Bind(R.id.activity_compose_ticket_subject)
    EditText activity_compose_ticket_subject;
    @Bind(R.id.activity_compose_ticket_company)
    AutoCompleteTextView activity_compose_ticket_company;
    @Bind(R.id.activity_compose_ticket_info)
    EditText activity_compose_ticket_info;
    MultipartEntityBuilder multipartEntity = MultipartEntityBuilder.create();
    Drawable defaultNavigationIcon = null;
    List<Company> companies=new ArrayList<>();
    private SuggestionsAdapter suggestionsAdapter;
    private Ticket ticket;
    public static void newInstance(AppCompatActivity activity,Ticket ticket) {
        Intent intent = new Intent(activity, EditTicketActivity.class);
        intent.putExtra(EXTRA_TICKET, ticket);
        ActivityCompat.startActivity(activity, intent, null);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compose_ticket);
        ButterKnife.bind(this);
        setSupportActionBar(activity_compose_ticket_toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ticket = (Ticket) getIntent().getSerializableExtra(EXTRA_TICKET);
        defaultNavigationIcon = activity_compose_ticket_toolbar.getNavigationIcon();
        activity_compose_ticket_subject.setText(ticket.getTitle());
        activity_compose_ticket_info.setText(ticket.getDescription());
        loadData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (isFill())
                    addTicket();
                else
                    supportFinishAfterTransition();
                return true;
            case R.id.menu_action_take_picture:
                Intent cameraIntent = new Intent(
                        MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, IMAGE_CAPTURE);
                return true;

            case R.id.menu_action_record_video:
                Intent videoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                startActivityForResult(videoIntent, VIDEO_CAPTURE);
                return true;
            case R.id.menu_action_upload_file:
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("file/");
                intent.addCategory(Intent.CATEGORY_DEFAULT);
                startActivityForResult(intent, UPLOAD_FILE);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        activity_compose_ticket_toolbar.setTitle("");
        getMenuInflater().inflate(R.menu.menu_compose_ticket, menu);

        return super.onCreateOptionsMenu(menu);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK)
            return;
        if (data == null)
            return;
        if (requestCode == IMAGE_CAPTURE) {
            try {
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                File f = new File(getCacheDir(), "image" + System.currentTimeMillis() + ".jpg");
                f.createNewFile();
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                photo.compress(Bitmap.CompressFormat.JPEG, 80 /*ignored for PNG*/, bos);
                byte[] bitmapdata = bos.toByteArray();
                FileOutputStream fos = new FileOutputStream(f);
                fos.write(bitmapdata);
                fos.flush();
                fos.close();
                multipartEntity.addBinaryBody("_images[]", f);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (requestCode == VIDEO_CAPTURE) {
            Log.e("TAG","data!!!!!!!!!!!!!!!!!!!!! ="+data.getData());
            Uri uri = data.getData();
            String path = getPath(uri);
            File file = new File(path);
            if (file.exists()) {
                if (resultCode == RESULT_OK) {
                    {

                        multipartEntity.addBinaryBody("_videos[]", file);
                    }
                } else if (resultCode == RESULT_CANCELED) {
                    Toast.makeText(this, "File picked cancelled.",
                            Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(this, "Failed picked file",
                            Toast.LENGTH_LONG).show();
                }
            } else {
                Log.e(TAG, "File " + data.getData() + " doesn't exist!");
            }
        }
        if (requestCode == UPLOAD_FILE) {
            Uri uri = data.getData();
            String path = getPath(uri);
            String filenameArray[] = path.split("\\.");
            String extension = filenameArray[filenameArray.length-1];
            if (!Arrays.asList("doc","docx","xls","pdf","zip","rar").contains(extension))
            {
                Utility.showToast(this,"only "+Arrays.asList("doc","docx","xls","pdf","zip","rar").toString());
                return;
            }
            File file = new File(path);
            if (file.exists()) {
                if (resultCode == RESULT_OK) {
                    {

                        multipartEntity.addBinaryBody("_files[]", file);
                    }
                } else if (resultCode == RESULT_CANCELED) {
                    Toast.makeText(this, "File picked cancelled.",
                            Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(this, "Failed picked file",
                            Toast.LENGTH_LONG).show();
                }
            } else {
                Log.e(TAG, "File " + data.getData() + " doesn't exist!");
            }
        }

    }
    public String getPath(Uri uri) {

        String path = null;
        String[] projection = { MediaStore.Files.FileColumns.DATA };
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);

        if(cursor == null){
            path = uri.getPath();
        }
        else{
            cursor.moveToFirst();
            int column_index = cursor.getColumnIndexOrThrow(projection[0]);
            path = cursor.getString(column_index);
            cursor.close();
        }

        return ((path == null || path.isEmpty()) ? (uri.getPath()) : path);
    }
    @OnFocusChange(R.id.activity_compose_ticket_company)
    void onFocusChangeCompany(boolean focused) {
        if (!focused)
            return;
        String company = activity_compose_ticket_company.getText().toString();
        if (company.contains("Gilmor"))
            activity_compose_ticket_company.setText("");
    }

    @OnTextChanged(R.id.activity_compose_ticket_subject)
    void onTextChangedsubject() {
        checkFill();
    }

    @OnTextChanged(R.id.activity_compose_ticket_company)
    void onTextChangedCompany() {
        checkFill();
    }

    @OnTextChanged(R.id.activity_compose_ticket_info)
    void onTextChangedInfo() {
        checkFill();
    }

    private void checkFill() {
        if (isFill())
            activity_compose_ticket_toolbar.setNavigationIcon(R.drawable.ok_blue);
        else if (defaultNavigationIcon != null)
            activity_compose_ticket_toolbar.setNavigationIcon(defaultNavigationIcon);

    }

    private boolean isFill() {
        String subject = activity_compose_ticket_subject.getText().toString();
        String company = activity_compose_ticket_company.getText().toString();
        String info = activity_compose_ticket_info.getText().toString();
        boolean selectCompany=false;
        for (Company companyItem:companies)
        {
            if (companyItem.getName().equals(company)) {
                selectCompany = true;
                break;
            }
        }
        if (subject.length() > 0 && selectCompany && info.length() > 0)
            return true;
        return false;
    }

    void addTicket() {
        String subject = null;
        String company;
        long managerId=-1;
        String info = null;

        try {
            subject = new String(activity_compose_ticket_subject.getText().toString().getBytes(), "utf-8");
            company = activity_compose_ticket_company.getText().toString();
            for (Company companyItem:companies)
            {
                if (companyItem.getName().equals(company) && companyItem.getManagers().size()>0) {
                    managerId=companyItem.getManagers().get(0).getId();
                    break;
                }
            }

            info = new String(activity_compose_ticket_info.getText().toString().getBytes(), "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        if (!isFill() || managerId==-1) return;

        final Dialog dialog = DialogUtility.getWaitDialog(this, getString(R.string.wait), false);
        dialog.show();
        final String url = Constants.ROOT_TICKET;
        //1: Process; 2: Resolved; 3: Closed; 0: (пустой статус, по умолчанию);
        multipartEntity.addPart("user_id", new StringBody(Long.toString(managerId), ContentType.TEXT_PLAIN));
        multipartEntity.addPart("client_id", new StringBody(PreferencesData.getLoginID(this), ContentType.TEXT_PLAIN));
        multipartEntity.addPart("title", new StringBody(subject, ContentType.TEXT_PLAIN));
        multipartEntity.addPart("description", new StringBody(info, ContentType.TEXT_PLAIN));
        multipartEntity.addPart("status", new StringBody("3", ContentType.TEXT_PLAIN));

        Log.e(TAG, "url= " + url);
        RequestParameters requestParameters = new RequestParameters(Query.TYPE_REQUEST.MULTI_PART, -1, multipartEntity, url);
        requestParameters.setHeader(Arrays.asList(new BasicHeader("X-Username", PreferencesData.getLoginName(this)),
                new BasicHeader("X-Password", PreferencesData.getLoginPassword(this))));

        Query.getInstance().setQuery(this, new CustomRequestHandler(this, true) {
            @Override
            public void processingRequest(RequestInterface requestInterface, Object response, int code, int type, Header[] headers) {
                Utility.closeDialog(dialog);
                super.processingRequest(requestInterface, response, code, type, headers);
                if (!errorInfo.isResult()) {
                    requestInterface.error(errorInfo);
                } else {
                    Log.e(TAG, "json= " + data);
                    Utility.showToast(EditTicketActivity.this,R.string.activity_compose_ticket_create);
                    supportFinishAfterTransition();
                }
                multipartEntity = MultipartEntityBuilder.create();


            }
        }, new RequestInterface() {
            @Override
            public void finish(Object obj) {

            }

            @Override
            public void error(Object obj) {
            }
        }, requestParameters);

    }

    private void loadData()  {
        final Dialog dialog = DialogUtility.getWaitDialog(this, getString(R.string.wait), false);
        dialog.show();

        RequestParameters requestParameters = new RequestParameters(Query.TYPE_REQUEST.GET, -1, Constants.COMPANIES_WITH_MANAGERS);
        //RequestParameters requestParameters = new RequestParameters(Query.TYPE_REQUEST.GET, -1,"http://reactionapp.artwebit.ru/api/company?with=feedbacks&filter=%7B%22user_id%22%3A%225%22%7D");
        requestParameters.setHeader(Arrays.asList(new BasicHeader("X-Username", PreferencesData.getLoginName(this)),
                new BasicHeader("X-Password", PreferencesData.getLoginPassword(this))));
        Query.getInstance().setQuery(this, new CustomRequestHandler(this, true) {
            @Override
            public void processingRequest(RequestInterface requestInterface, Object response, int code, int type, Header[] headers) {
                Utility.closeDialog(dialog);
                super.processingRequest(requestInterface, response, code, type, headers);
                if (!errorInfo.isResult()) {
                    requestInterface.error(errorInfo);
                } else {
                    List<Company> companies = JsonParser.parseCompanies(data);
                    requestInterface.finish(companies);

                }

            }
        }, new RequestInterface() {
            @Override
            public void finish(Object obj) {
                companies.clear();
                companies.addAll((List<Company>) obj);
                suggestionsAdapter=new SuggestionsAdapter(EditTicketActivity.this,R.layout.list_item_suggestions,companies);
                activity_compose_ticket_company.setAdapter(suggestionsAdapter);
            }

            @Override
            public void error(Object obj) {
            }
        }, requestParameters);
    }

}