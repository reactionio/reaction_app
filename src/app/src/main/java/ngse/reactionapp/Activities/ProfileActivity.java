package ngse.reactionapp.Activities;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;

import com.NGSE.RequestLibrary.Query;
import com.NGSE.RequestLibrary.RequestInterface;
import com.NGSE.RequestLibrary.RequestParameters;
import com.squareup.picasso.Picasso;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.sromku.simple.fb.listeners.OnProfileListener;
import com.sromku.simple.fb.utils.Attributes;
import com.sromku.simple.fb.utils.PictureAttributes;

import org.apache.http.Header;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.message.BasicHeader;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ngse.reactionapp.Applications.MainApplication;
import ngse.reactionapp.Data.PreferencesData;
import ngse.reactionapp.Data.Profile;
import ngse.reactionapp.R;
import ngse.reactionapp.Utility.Constants;
import ngse.reactionapp.Utility.CustomRequestHandler;
import ngse.reactionapp.Utility.DialogUtility;
import ngse.reactionapp.Utility.JsonParser;
import ngse.reactionapp.Utility.Utility;


public class ProfileActivity extends AppCompatActivity {

    public static final String TAG = ProfileActivity.class.getName();
    private static final String EXTRA_PROFILE = "extraProfile";
    private static final int IMAGE_TAKE = 99;
    private static final int IMAGE_CAPTURE = 100;
    @Bind(R.id.activity_profile_first_name)
    EditText activity_profile_first_name;
    @Bind(R.id.activity_profile_last_name)
    EditText activity_profile_last_name;
    @Bind(R.id.activity_profile_address)
    EditText activity_profile_address;
    final OnLoginListener onLoginListener = new OnLoginListener() {


        @Override
        public void onException(Throwable throwable) {
            Log.i("TAG", "onException");
        }

        @Override
        public void onFail(String s) {
            Log.i("TAG", "onFail");
        }

        @Override
        public void onLogin(String s, List<Permission> list, List<Permission> list1) {
            Log.i("TAG", "onLogin");
            getProfile();
        }

        @Override
        public void onCancel() {
            Log.i("TAG", "onCancel");
        }
    };
    @Bind(R.id.activity_profile_img)
    ImageView activity_profile_img;
    @Bind(R.id.activity_profile_toolbar)
    Toolbar activity_profile_toolbar;
    MultipartEntityBuilder multipartEntity = MultipartEntityBuilder.create();
    private SimpleFacebook simpleFacebook;

    public static void newInstance(AppCompatActivity activity, Profile profile) {
        Intent intent = new Intent(activity, ProfileActivity.class);
        intent.putExtra(EXTRA_PROFILE, profile);
        ActivityCompat.startActivity(activity, intent, null);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        simpleFacebook = SimpleFacebook.getInstance(this);
        ButterKnife.bind(this);
        setSupportActionBar(activity_profile_toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Profile profile = (Profile) getIntent().getSerializableExtra(EXTRA_PROFILE);
        if (profile == null)
            return;
        Picasso.with(activity_profile_img.getContext()).load(profile.getImageUrl()).into(activity_profile_img);
        activity_profile_first_name.setText(profile.getFirstname());
        activity_profile_last_name.setText(profile.getLastname());
        activity_profile_address.setText(profile.getAddress());

        activity_profile_first_name.setSelection(activity_profile_first_name.getText().length());
        activity_profile_last_name.setSelection(activity_profile_last_name.getText().length());
        activity_profile_address.setSelection(activity_profile_address.getText().length());
        try {
            PackageInfo info = getPackageManager().getPackageInfo("ngse.reactionapp", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String keyHash = Base64.encodeToString(md.digest(), Base64.DEFAULT);
                Log.e("TAG", "keyHash: " + keyHash);
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("TAG", "NameNotFoundException: " + e.getMessage());
        } catch (NoSuchAlgorithmException e) {
            Log.e("TAG", "NoSuchAlgorithmException: " + e.getMessage());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getSupportActionBar().setTitle(R.string.settings_fragment_profile);

        return super.onCreateOptionsMenu(menu);
    }

    @OnClick(R.id.activity_profile_ok)
    void update() {
        String firstname = null;
        String lastname = null;
        String address = null;
        try {
            firstname = new String(activity_profile_first_name.getText().toString().getBytes(), "utf-8");
            lastname = new String(activity_profile_last_name.getText().toString().getBytes(), "utf-8");
            address = new String(activity_profile_address.getText().toString().getBytes(), "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        if (firstname == null || firstname.length() == 0) return;
        if (lastname == null || lastname.length() == 0) return;
        if (address == null || address.length() == 0) return;

        final Dialog dialog = DialogUtility.getWaitDialog(this, getString(R.string.wait), false);
        dialog.show();
        final String url = Constants.CLIENT + "/" + PreferencesData.getLoginID(this);
        multipartEntity.addPart("firstname", new StringBody(firstname, ContentType.TEXT_PLAIN));
        multipartEntity.addPart("lastname", new StringBody(lastname, ContentType.TEXT_PLAIN));
        multipartEntity.addPart("address", new StringBody(address, ContentType.TEXT_PLAIN));
        Log.e(TAG, "url= " + url);
        RequestParameters requestParameters = new RequestParameters(Query.TYPE_REQUEST.MULTI_PART, -1, multipartEntity, url);//multipartEntity

        requestParameters.setHeader(Arrays.asList(new BasicHeader("X-Username", PreferencesData.getLoginName(this)),
                new BasicHeader("X-Password", PreferencesData.getLoginPassword(this))));
        // RequestPutMultiPart requestPut = new RequestPutMultiPart
        Query.getInstance().setQuery(this, new CustomRequestHandler(this, true) {
            @Override
            public void processingRequest(RequestInterface requestInterface, Object response, int code, int type, Header[] headers) {
                Utility.closeDialog(dialog);

                super.processingRequest(requestInterface, response, code, type, headers);
                if (!errorInfo.isResult()) {
                    requestInterface.error(errorInfo);
                } else {
                    Profile profile = JsonParser.parseProfile(data);
                    requestInterface.finish(profile);
                }
            }
        }, new RequestInterface() {
            @Override
            public void finish(Object obj) {
                MainApplication application = (MainApplication) getApplication();
                application.getPropertyChangeSupport().setProfile((Profile) obj);
                onBackPressed();

            }

            @Override
            public void error(Object obj) {
                multipartEntity = MultipartEntityBuilder.create();
            }
        }, requestParameters);

    }

    @OnClick(R.id.activity_profile_change_photo)
    void activity_profile_change_photo() {
        Dialog dialog = DialogUtility.getAlertDialogMedia(this,
                getString(R.string.makeYourChoice),
                new DialogUtility.OnDialogButtonClickListener() {
                    @Override
                    public void onPositiveClick() {
                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            startActivityForResult(Intent.createChooser(intent, "Select Picture"), IMAGE_TAKE);
                        } else {
                            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                            intent.addCategory(Intent.CATEGORY_OPENABLE);
                            intent.setType("image/*");
                            startActivityForResult(intent, IMAGE_TAKE);
                        }
                    }

                    @Override
                    public void onNegativeClick() {
                        Intent cameraIntent = new Intent(
                                android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, IMAGE_CAPTURE);
                    }
                });
        dialog.show();
    }

    @OnClick(R.id.activity_profile_remove_photo)
    void activity_profile_remove_photo() {
        multipartEntity = MultipartEntityBuilder.create();
        multipartEntity.addPart("image", new StringBody("", ContentType.TEXT_PLAIN));
        activity_profile_img.setImageBitmap(null);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //simpleFacebook.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK)
            return;
        if (data == null)
            return;

        if (requestCode == IMAGE_CAPTURE) {
            try {
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                activity_profile_img.setImageBitmap(photo);
                File f = new File(getCacheDir(), "image" + System.currentTimeMillis() + ".jpg");
                f.createNewFile();
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                photo.compress(Bitmap.CompressFormat.JPEG, 80 /*ignored for PNG*/, bos);
                byte[] bitmapdata = bos.toByteArray();
                FileOutputStream fos = new FileOutputStream(f);
                fos.write(bitmapdata);
                fos.flush();
                fos.close();
                if (!f.exists())
                    return;
                multipartEntity = MultipartEntityBuilder.create();
                multipartEntity.addBinaryBody("image", f);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == IMAGE_TAKE) {
            String path = Utility.getPath(this, data);
            File f = new File(path);
            if (!f.exists())
                return;
            BitmapFactory.Options options;
            Bitmap bmp = null;
            try {
                bmp = BitmapFactory.decodeFile(path);

            } catch (Exception e) {
                Log.d("filepath", "error" + e.toString());
                try {
                    options = new BitmapFactory.Options();
                    options.inSampleSize = 2;
                    bmp = BitmapFactory.decodeFile(path, options);

                } catch (Exception e1) {
                    Log.d("filepath", "error" + e.toString());
                }
            }
            activity_profile_img.setImageBitmap(bmp);
            multipartEntity = MultipartEntityBuilder.create();
            multipartEntity.addBinaryBody("image", f);
        }
    }

    @OnClick(R.id.activity_profile_facebook)
    void activity_profile_facebook() {
        if (simpleFacebook != null)
            if (!simpleFacebook.isLogin()) {

                simpleFacebook.login(onLoginListener);
            } else
                getProfile();
    }

    private void getProfile() {
        PictureAttributes pictureAttributes = Attributes.createPictureAttributes();
        pictureAttributes.setHeight(500);
        pictureAttributes.setWidth(500);
        com.sromku.simple.fb.entities.Profile.Properties properties = new com.sromku.simple.fb.entities.Profile.Properties.Builder()
                .add(com.sromku.simple.fb.entities.Profile.Properties.PICTURE, pictureAttributes)
                .add(com.sromku.simple.fb.entities.Profile.Properties.FIRST_NAME)
                .add(com.sromku.simple.fb.entities.Profile.Properties.LAST_NAME)
                .add(com.sromku.simple.fb.entities.Profile.Properties.LOCATION)
                .build();
        SimpleFacebook.getInstance().getProfile(properties, new OnProfileListener() {

            @Override
            public void onThinking() {

            }

            @Override
            public void onException(Throwable throwable) {

            }

            @Override
            public void onFail(String reason) {

            }

            @Override
            public void onComplete(com.sromku.simple.fb.entities.Profile response) {
                Log.e("TAG", "getProfile");
                Log.e("TAG", "getFirstName " + response.getFirstName());
                Log.e("TAG", "getLastName " + response.getLastName());
                Log.e("TAG", "getLocation " + response.getLocation());

                activity_profile_first_name.setText(response.getFirstName());
                activity_profile_last_name.setText(response.getLastName());
                if (response.getLocation() != null)
                    activity_profile_address.setText(response.getLocation().getCountry());

                activity_profile_first_name.setSelection(activity_profile_first_name.getText().length());
                activity_profile_last_name.setSelection(activity_profile_last_name.getText().length());
                activity_profile_address.setSelection(activity_profile_address.getText().length());
            }
        });

    }

}