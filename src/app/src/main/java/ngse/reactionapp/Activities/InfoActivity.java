package ngse.reactionapp.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import ngse.reactionapp.R;


public class InfoActivity extends AppCompatActivity {

    private static final String EXTRA_INFO = "extraInfo";
    @Bind(R.id.activity_info_text)
    TextView activity_info_text;
    @Bind(R.id.activity_info_toolbar)
    Toolbar activity_info_toolbar;
    String extraInfo;
    public static void newInstance(AppCompatActivity activity, String extraInfo) {
        Intent intent = new Intent(activity, InfoActivity.class);
        intent.putExtra(EXTRA_INFO, extraInfo);
        ActivityCompat.startActivity(activity, intent, null);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        ButterKnife.bind(this);
        setSupportActionBar(activity_info_toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        extraInfo = getIntent().getStringExtra(EXTRA_INFO);
        if (extraInfo==null)
            return;
        activity_info_toolbar.setTitle(extraInfo);
        if (extraInfo.equals(getString(R.string.activity_about_terms_of_service)))
            activity_info_text.setText(R.string.activity_info_terms_of_service_text);
        else   if (extraInfo.equals(getString(R.string.activity_about_privacy_policy)))
            activity_info_text.setText(R.string.activity_info_privacy_policy_text);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getSupportActionBar().setTitle(extraInfo);
        return super.onCreateOptionsMenu(menu);
    }
}