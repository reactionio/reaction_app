package ngse.reactionapp.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import ngse.reactionapp.Adapters.ImgPagerAdapter;
import ngse.reactionapp.R;


public class ImageCarouselActivity extends AppCompatActivity {

    private static final String EXTRA_MESSAGE_FILES = "messageFiles";
    private static final String EXTRA_CURRENT_MESSAGE_FILE = "currentMessageFile";
    @Bind(R.id.activity_image_carousel_view_pager)
    ViewPager activity_image_carousel_view_pager;
    @Bind(R.id.activity_image_carousel_toolbar)
    Toolbar activity_image_carousel_toolbar;


    private ArrayList<String> messageFiles = new ArrayList<>();
    private int currentMessageFile;
    public static void newInstance(AppCompatActivity activity,ArrayList<String> messageFiles, int currentMessageFile) {
        Intent intent = new Intent(activity, ImageCarouselActivity.class);
        intent.putStringArrayListExtra(EXTRA_MESSAGE_FILES, messageFiles);
        intent.putExtra(EXTRA_CURRENT_MESSAGE_FILE, currentMessageFile);
        ActivityCompat.startActivity(activity, intent, null);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_carousel);
        ButterKnife.bind(this);
        setSupportActionBar(activity_image_carousel_toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        messageFiles=getIntent().getStringArrayListExtra(EXTRA_MESSAGE_FILES);
        currentMessageFile = getIntent().getIntExtra(EXTRA_CURRENT_MESSAGE_FILE, -1);

        ImgPagerAdapter adapter = new ImgPagerAdapter(this, messageFiles, activity_image_carousel_view_pager);
        activity_image_carousel_view_pager.setAdapter(adapter);
        activity_image_carousel_view_pager.setCurrentItem(currentMessageFile + 1);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }
}