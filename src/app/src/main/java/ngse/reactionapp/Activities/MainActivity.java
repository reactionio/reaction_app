package ngse.reactionapp.Activities;

import android.app.Activity;
import android.app.Dialog;
import android.app.SearchManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.NGSE.RequestLibrary.Query;
import com.NGSE.RequestLibrary.RequestInterface;
import com.NGSE.RequestLibrary.RequestParameters;
import com.crashlytics.android.Crashlytics;

import org.apache.http.Header;
import org.apache.http.message.BasicHeader;

import java.util.Arrays;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.fabric.sdk.android.Fabric;
import ngse.reactionapp.Adapters.MainSectionsPagerAdapter;
import ngse.reactionapp.Applications.MainApplication;
import ngse.reactionapp.Data.PreferencesData;
import ngse.reactionapp.Data.Profile;
import ngse.reactionapp.GCM.GCMClientManager;
import ngse.reactionapp.R;
import ngse.reactionapp.Services.CallService;
import ngse.reactionapp.Services.NetworkServices.rest.PushService;
import ngse.reactionapp.Services.NetworkServices.rest.ServiceGenerator;
import ngse.reactionapp.Utility.Constants;
import ngse.reactionapp.Utility.CustomRequestHandler;
import ngse.reactionapp.Utility.DialogUtility;
import ngse.reactionapp.Utility.JsonParser;
import ngse.reactionapp.Utility.Utility;


public class MainActivity extends AppCompatActivity {
    MainSectionsPagerAdapter mainSectionsPagerAdapter;
    @Bind(R.id.main_activity_view_pager)
    ViewPager main_activity_view_pager;
    @Bind(R.id.main_activity_tab_layout)
    TabLayout main_activity_tab_layout;
    @Bind(R.id.main_activity_toolbar)
    Toolbar main_activity_toolbar;
    private long backPressed;

    public static void newInstance(Service service) {
        Intent intent = new Intent(service, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        service.startActivity(intent);
    }

    public static void newInstance(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public static void newInstance(AppCompatActivity activity, boolean splash) {
        Intent intent = new Intent(activity, MainActivity.class);
        ActivityCompat.startActivity(activity, intent, null);
        if (splash)
            SplashActivity.newInstance(activity);
    }

    public static void newInstance(AppCompatActivity activity, boolean splash, boolean social) {
        Intent intent = new Intent(activity, MainActivity.class);
        intent.putExtra("IS_SOCIAL", true);
        ActivityCompat.startActivity(activity, intent, null);
        if (splash)
            SplashActivity.newInstance(activity);
    }

    private final BroadcastReceiver abcd = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            finish();
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
//        if(getIntent().hasExtra("IS_SOCIAL")) {
//            if (!getIntent().getBooleanExtra("IS_SOCIAL", false)) {
//                loadProfile();
//            }
//        } else {
        loadProfile();
//        }
        //todo ticket
//        if (mainSectionsPagerAdapter != null) {
//            TicketPageFragment ticketPageFragment = mainSectionsPagerAdapter.getTicketPageFragment();
//            if (ticketPageFragment != null)
//                ticketPageFragment.updateTickets();
//        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Fabric.with(this, new Crashlytics());

        registerReceiver(abcd, new IntentFilter("xyz"));

        Utility.startCheckService(this);
        ButterKnife.bind(this);
        setSupportActionBar(main_activity_toolbar);
        main_activity_toolbar.setLogo(R.mipmap.ic_launcher);

        main_activity_tab_layout.addTab(main_activity_tab_layout.newTab().setIcon(R.drawable.phone_tab_selector));
//todo ticket       main_activity_tab_layout.addTab(main_activity_tab_layout.newTab().setIcon(R.drawable.ticket_tab_selector));
        main_activity_tab_layout.addTab(main_activity_tab_layout.newTab().setIcon(R.drawable.settings_tab_selector));
        mainSectionsPagerAdapter = new MainSectionsPagerAdapter(getSupportFragmentManager(), main_activity_tab_layout.getTabCount());
        main_activity_view_pager.setAdapter(mainSectionsPagerAdapter);


        main_activity_view_pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(main_activity_tab_layout));
        main_activity_tab_layout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                main_activity_view_pager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


       /* final WindowManager.LayoutParams param=new WindowManager.LayoutParams();
        param.flags=WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
        final View view=findViewById(R.id.main_activity_tab_layout);
        final ViewGroup parent=(ViewGroup)view.getParent();
        if(parent!=null)
            parent.removeView(view);
        param.format= PixelFormat.RGBA_8888;
        param.type=WindowManager.LayoutParams.TYPE_SYSTEM_ALERT;
        param.gravity= Gravity.TOP|Gravity.LEFT;
        param.width=parent!=null? LinearLayout.LayoutParams.WRAP_CONTENT:view.getLayoutParams().width;
        param.height=parent!=null? LinearLayout.LayoutParams.WRAP_CONTENT:view.getLayoutParams().height;
        final WindowManager wmgr=(WindowManager)getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        wmgr.addView(view,param);*/


        CallService.start(this);

//        try {
        new AsyncPush().execute();
//        } catch (Exception e) {
//            Log.e("GCM", "onCreate: ERROR WITH CONNECTING TO GCM");
//        }
    }

    class AsyncPush extends AsyncTask<String, String, Boolean> {
        @Override
        protected Boolean doInBackground(String... params) {
            String PROJECT_NUMBER = "491688429136";

            GCMClientManager pushClientManager = new GCMClientManager(MainActivity.this, PROJECT_NUMBER);
            pushClientManager.registerIfNeeded(new GCMClientManager.RegistrationCompletedHandler() {
                @Override
                public void onSuccess(String registrationId, boolean isNewRegistration) {

                    Log.d("registration id", registrationId);
                    //send this registrationId to your server

                    PushService pushService =
                            ServiceGenerator.createServiceAuth(
                                    PushService.class,
                                    PreferencesData.getLoginName(getApplicationContext()),
                                    PreferencesData.getLoginPassword(getApplicationContext())
                            );
                    try {
//                        String newToken = registrationId.substring(12, registrationId.length());
//
//                        Log.d("registration", "new token: " + newToken);

                        retrofit2.Call<PushService.Push> call = pushService.subscribePush(registrationId, "gcm");

                        PushService.Push push = call.execute().body();


                        Log.e("registration", "json = { result : " + push.result + " }");

                    } catch (Exception e) {
                        Log.e("retrofit", "e doInBackground: " + e.getMessage());
                    }
                }

                @Override
                public void onFailure(String ex) {
                    super.onFailure(ex);
                }
            });

            return true;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //CallService.stop(this);
        unregisterReceiver(abcd);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.menu_action_search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        //todo ticket
//        TicketPageFragment ticketPageFragment = mainSectionsPagerAdapter.getTicketPageFragment();
//        if (ticketPageFragment != null) {
//            if (ticketPageFragment.onTabBackPressed())
//                return;
//
//        }
        FragmentManager currentFragmentManager = getSupportFragmentManager();
        if (currentFragmentManager.getBackStackEntryCount() == 0) {
            if (backPressed + 2000 > System.currentTimeMillis()) {
                super.onBackPressed();
                finish();
            } else {
                Toast.makeText(getBaseContext(), R.string.go_exit, Toast.LENGTH_SHORT).show();
            }
            backPressed = System.currentTimeMillis();
        } else {
            super.onBackPressed();
        }
    }

    private void loadProfile() {
        final Dialog dialog = DialogUtility.getWaitDialog(this, getString(R.string.wait), false);
        dialog.show();
        RequestParameters requestParameters = new RequestParameters(Query.TYPE_REQUEST.GET, -1,
                Constants.CLIENT + "/" + PreferencesData.getLoginID(this));
        requestParameters.setHeader(Arrays.asList(new BasicHeader("X-Username", PreferencesData.getLoginName(this)),
                new BasicHeader("X-Password", PreferencesData.getLoginPassword(this))));
        Query.getInstance().setQuery(this, new CustomRequestHandler(this, false) {
            @Override
            public void processingRequest(RequestInterface requestInterface, Object response, int code, int type, Header[] headers) {
                Utility.closeDialog(dialog);
                super.processingRequest(requestInterface, response, code, type, headers);
                if (!errorInfo.isResult()) {
                    requestInterface.error(errorInfo);
                } else {
                    Profile profile = JsonParser.parseProfile(data);
                    requestInterface.finish(profile);
                }
            }
        }, new RequestInterface() {
            @Override
            public void finish(Object obj) {
                MainApplication application = (MainApplication) getApplication();
                application.getPropertyChangeSupport().setProfile((Profile) obj);
            }

            @Override
            public void error(Object obj) {
                Dialog dialog = DialogUtility.getAlertDialogTwoButtons(MainActivity.this,
                        getString(R.string.accessNetworkAttention),
                        new DialogUtility.OnDialogButtonClickListener() {
                            @Override
                            public void onPositiveClick() {
                                loadProfile();
                            }

                            @Override
                            public void onNegativeClick() {
                                MainActivity.this.finish();
                            }
                        });
                dialog.show();
            }
        }, requestParameters);
    }

  /*  @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case (PICK_CONTACT) :
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactUri = data.getData();
                    // We only need the NUMBER column, because there will be only one row in the result
                    String[] projection = {ContactsContract.CommonDataKinds.Phone.NUMBER};

                    Cursor cursor = getContentResolver().query(contactUri, projection, null, null, null);
                    cursor.moveToFirst();

                    int column = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                    String number = "tel:" + cursor.getString(column);
                    Toast.makeText(MainActivity.this, number, Toast.LENGTH_SHORT);

                    try {
                        Intent intent = new Intent(Intent.ACTION_CALL);
                        if (Utility.isPackageInstalled("com.android.phone", MainActivity.this))
                            intent.setClassName("com.android.phone", "com.android.phone.OutgoingCallBroadcaster");
                        intent.setData(Uri.parse(number));
                        startActivity(intent);
                    } catch (Exception e) {
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse(number));
                        startActivity(callIntent);
                    }

                }
                break;
        }
    }*/

}
