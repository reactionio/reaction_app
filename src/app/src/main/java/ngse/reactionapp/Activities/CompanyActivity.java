package ngse.reactionapp.Activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.transition.Slide;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.NGSE.RequestLibrary.Query;
import com.NGSE.RequestLibrary.RequestInterface;
import com.NGSE.RequestLibrary.RequestParameters;
import com.NGSE.textviewpluslib.TextViewPlus;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.apache.http.Header;
import org.apache.http.message.BasicHeader;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Arrays;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ngse.reactionapp.Adapters.FeedbacksRecycleViewAdapter;
import ngse.reactionapp.Data.Call;
import ngse.reactionapp.Data.Company;
import ngse.reactionapp.Data.Feedback;
import ngse.reactionapp.Data.Manager;
import ngse.reactionapp.Data.PreferencesData;
import ngse.reactionapp.R;
import ngse.reactionapp.Utility.Constants;
import ngse.reactionapp.Utility.CustomRequestHandler;
import ngse.reactionapp.Utility.DialogUtility;
import ngse.reactionapp.Utility.JsonParser;
import ngse.reactionapp.Utility.Utility;


public class CompanyActivity extends AppCompatActivity {

    private static final String EXTRA_MANAGER = "manager";
    private static final String EXTRA_COMPANY_ID = "id";
    @Bind(R.id.activity_company_about)
    TextViewPlus activity_company_about;
    @Bind(R.id.activity_company_image)
    ImageView activity_company_image;
    @Bind(R.id.activity_company_recycler_view_feedbacks)
    RecyclerView activity_company_recycler_view_feedbacks;
    @Bind(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsing_toolbar;
    @Bind(R.id.activity_company_ll_feedbacks)
    LinearLayout activity_company_ll_feedbacks;
    private  Company company;
    private FeedbacksRecycleViewAdapter feedbacksRecycleViewAdapter;
    private Manager manager;
    private long companyId;
    public static void newInstance(AppCompatActivity activity, View transitionImage, Call callLogsItem) {
        Intent intent = new Intent(activity, CompanyActivity.class);
        intent.putExtra(EXTRA_MANAGER, callLogsItem.getManager());
        ActivityCompat.startActivity(activity, intent, null);
    }
    public static void newInstance(AppCompatActivity activity, long companyId) {
        Intent intent = new Intent(activity, CompanyActivity.class);
        intent.putExtra(EXTRA_COMPANY_ID, companyId);
        ActivityCompat.startActivity(activity, intent, null);
    }
    @SuppressWarnings("ConstantConditions")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company);
        ButterKnife.bind(this);
        activity_company_recycler_view_feedbacks.setLayoutManager(new LinearLayoutManager(this));
        initActivityTransitions();
        manager = (Manager) getIntent().getSerializableExtra(EXTRA_MANAGER);
        companyId=getIntent().getLongExtra(EXTRA_COMPANY_ID,-1);
        try {
            if (manager!=null)
                loadData(manager.getId(),-1);
                        else
               loadData(-1,companyId);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        ViewCompat.setTransitionName(findViewById(R.id.app_bar_layout), EXTRA_MANAGER);
        supportPostponeEnterTransition();

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ViewCompat.setTransitionName(activity_company_image, EXTRA_MANAGER);
        supportStartPostponedEnterTransition();
    }

    @OnClick(R.id.activity_company_call)
    void call() {
        if (manager!=null)
            Utility.startCall(this, manager.getPhone(), manager.getId());
    }

    @OnClick(R.id.activity_company_chat)
    void chat() {
        ComposeTicketActivity.newInstance(this,company.getId());
        //ChatActivity.newInstance(this, activity_company_image, manager);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                supportFinishAfterTransition();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initActivityTransitions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Slide transition = new Slide();
            transition.excludeTarget(android.R.id.statusBarBackground, true);
            getWindow().setEnterTransition(transition);
            getWindow().setReturnTransition(transition);
        }
    }


    private void applyPalette(Palette palette) {
        int primary = getResources().getColor(R.color.color_primary);
        collapsing_toolbar.setContentScrimColor(palette.getMutedColor(primary));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(palette.getDarkMutedColor(primary));
        }

    }

    private void updateBackground(FloatingActionButton fab, Palette palette) {
        int lightVibrantColor = palette.getLightVibrantColor(getResources().getColor(android.R.color.white));
        int vibrantColor = palette.getVibrantColor(getResources().getColor(R.color.color_primary_dark));//accent
        fab.setRippleColor(lightVibrantColor);
        fab.setBackgroundTintList(ColorStateList.valueOf(vibrantColor));
    }

    private void loadData(long managerId,long companyId) throws UnsupportedEncodingException {
        final Dialog dialog = DialogUtility.getWaitDialog(this, getString(R.string.wait), false);
        dialog.show();
        RequestParameters requestParameters;
        if (managerId!=-1)
            requestParameters = new RequestParameters(Query.TYPE_REQUEST.GET, -1, Constants.COMPANY_WITH_FEEDBACKS + URLEncoder.encode("{\"user_id\"=\"" + managerId + "\"}", "utf-8"));
        else
            requestParameters = new RequestParameters(Query.TYPE_REQUEST.GET, -1, Constants.ROOT_COMPANY +"/"+companyId+Constants.WITH_FEEDBACKS +",manager,director");
        Log.d("TAG","url= "+requestParameters.getUrl());
        requestParameters.setHeader(Arrays.asList(new BasicHeader("X-Username", PreferencesData.getLoginName(this)),
                new BasicHeader("X-Password", PreferencesData.getLoginPassword(this))));
        Query.getInstance().setQuery(this, new CustomRequestHandler(this, true) {
            @Override
            public void processingRequest(RequestInterface requestInterface, Object response, int code, int type, Header[] headers) {
                Log.d("TAG","json= "+data);
                Utility.closeDialog(dialog);
                super.processingRequest(requestInterface, response, code, type, headers);
                if (!errorInfo.isResult()) {
                    requestInterface.error(errorInfo);
                } else {
                    Company company = JsonParser.parseCompany(data);
                    requestInterface.finish(company);

                }

            }
        }, new RequestInterface() {
            @Override
            public void finish(Object obj) {
                company = (Company) obj;
                if (company == null)
                    return;

                if (company.getFeedbacks() != null && company.getFeedbacks().size() > 0)
                    for (Feedback feedback : company.getFeedbacks()) {
                        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View view = inflater.inflate(R.layout.list_item_feedback, null);

                        ImageView list_item_feedback_img = (ImageView) view.findViewById(R.id.list_item_feedback_img);
                        TextViewPlus list_item_feedback_author = (TextViewPlus) view.findViewById(R.id.list_item_feedback_author);
                        TextViewPlus list_item_feedback_date = (TextViewPlus) view.findViewById(R.id.list_item_feedback_date);
                        TextViewPlus list_item_feedback_text = (TextViewPlus) view.findViewById(R.id.list_item_feedback_text);
                        list_item_feedback_text.setText(feedback.getFeedback());
                        list_item_feedback_date.setText(feedback.getCreatedAt());
                        activity_company_ll_feedbacks.addView(view);
                    }
                feedbacksRecycleViewAdapter = new FeedbacksRecycleViewAdapter(CompanyActivity.this, company.getFeedbacks());
                activity_company_recycler_view_feedbacks.setAdapter(feedbacksRecycleViewAdapter);
                collapsing_toolbar.setTitle(company.getName());
                activity_company_about.setText(company.getDescription());
                Picasso.with(CompanyActivity.this).load(company.getImageUrl()).into(activity_company_image, new Callback() {
                    @Override
                    public void onSuccess() {
                        Bitmap bitmap = ((BitmapDrawable) activity_company_image.getDrawable()).getBitmap();
                        Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {
                            public void onGenerated(Palette palette) {
                                applyPalette(palette);
                            }
                        });
                    }

                    @Override
                    public void onError() {

                    }
                });
                if (company.getManagers() != null && company.getManagers().size() > 0)
                    manager=company.getManagers().get(0);

            }

            @Override
            public void error(Object obj) {
            }
        }, requestParameters);
    }

}