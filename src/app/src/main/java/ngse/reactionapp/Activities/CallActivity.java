package ngse.reactionapp.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;
import ngse.reactionapp.Data.Call;
import ngse.reactionapp.R;


public class CallActivity extends AppCompatActivity {

    private static final String EXTRA_IMAGE = "extraImage";
    private static final String EXTRA_TITLE = "extraTitle";


    @Bind(R.id.activity_call_img)
    ImageView activity_call_img;
    @Bind(R.id.activity_call_status)
    TextView activity_call_status;
    @Bind(R.id.activity_call_name)
    TextView activity_call_name;
    @Bind(R.id.activity_call_number)
    TextView activity_call_number;

    public static void newInstance(AppCompatActivity activity, View transitionImage, Call callLogsItem) {
        Intent intent = new Intent(activity, CallActivity.class);
        intent.putExtra(EXTRA_IMAGE, callLogsItem.getManager().getImageUrl());
        intent.putExtra(EXTRA_TITLE, callLogsItem.getManager().getFirstname());

        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, transitionImage, EXTRA_IMAGE);
        ActivityCompat.startActivity(activity, intent, options.toBundle());
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call);
        ButterKnife.bind(this);
        String extraImage = getIntent().getStringExtra(EXTRA_IMAGE);
        Picasso.with(activity_call_img.getContext()).load(extraImage).
                into(activity_call_img);
        String itemTitle = getIntent().getStringExtra(EXTRA_TITLE);
        activity_call_name.setText(itemTitle);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


}