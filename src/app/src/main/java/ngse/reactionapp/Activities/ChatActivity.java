package ngse.reactionapp.Activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.NGSE.RequestLibrary.Query;
import com.NGSE.RequestLibrary.RequestInterface;
import com.NGSE.RequestLibrary.RequestParameters;

import org.apache.http.Header;
import org.apache.http.message.BasicHeader;
import org.json.JSONObject;
import org.kaazing.net.sse.SseEventReader;
import org.kaazing.net.sse.SseEventSource;
import org.kaazing.net.sse.SseEventSourceFactory;
import org.kaazing.net.sse.SseEventType;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;

import butterknife.Bind;
import butterknife.ButterKnife;
import ngse.reactionapp.Adapters.ChatSectionsPagerAdapter;
import ngse.reactionapp.Adapters.ChatStickyHeadersAdapter;
import ngse.reactionapp.Adapters.ImagesAdapter;
import ngse.reactionapp.Data.Message;
import ngse.reactionapp.Data.MessageFile;
import ngse.reactionapp.Data.PreferencesData;
import ngse.reactionapp.Data.Ticket;
import ngse.reactionapp.Data.TicketFile;
import ngse.reactionapp.Enums.FILE_TYPE;
import ngse.reactionapp.Enums.TICKET_STATE;
import ngse.reactionapp.R;
import ngse.reactionapp.Utility.CustomRequestHandler;
import ngse.reactionapp.Utility.DialogUtility;
import ngse.reactionapp.Utility.JsonParser;
import ngse.reactionapp.Utility.Utility;


public class ChatActivity extends AppCompatActivity {
    public static final String TAG = ChatActivity.class.getName();
    private static final String EXTRA_TICKET = "ticket";
    private static final int IMAGE_CAPTURE = 100;
    private static final int VIDEO_CAPTURE = 101;
    private static final int UPLOAD_FILE = 102;
    @Bind(R.id.activity_chat_toolbar)
    Toolbar activity_chat_toolbar;
    @Bind(R.id.activity_chat_view_pager)
    ViewPager activity_chat_view_pager;
    @Bind(R.id.activity_chat_tab_layout)
    TabLayout activity_chat_tab_layout;
    ChatSectionsPagerAdapter chatSectionsPagerAdapter;
    private List<TicketFile> chatFiles = new ArrayList<>();
    private ArrayList<String> messageFiles = new ArrayList<>();
    private ImagesAdapter imagesAdapter;
    private ChatStickyHeadersAdapter chatStickyHeadersAdapter;
    private Ticket ticket;
    private SseEventSource eventSource = null;
    private long lastSSECode = 0;
    private String lastPhotoPath;
    public static void newInstance(AppCompatActivity activity, Ticket ticket) {
        Intent intent = new Intent(activity, ChatActivity.class);
        intent.putExtra(EXTRA_TICKET, ticket);
        ActivityCompat.startActivity(activity, intent, null);
    }
    Thread sseEventReaderThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);
        setSupportActionBar(activity_chat_toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        chatStickyHeadersAdapter = new ChatStickyHeadersAdapter(this);
        ticket = (Ticket) getIntent().getSerializableExtra(EXTRA_TICKET);
        imagesAdapter = new ImagesAdapter(this, messageFiles, new ImagesAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                ImageCarouselActivity.newInstance(ChatActivity.this, messageFiles, position);
            }
        });
        activity_chat_tab_layout.addTab(activity_chat_tab_layout.newTab().setText(R.string.activity_chat_main_page));
        activity_chat_tab_layout.addTab(activity_chat_tab_layout.newTab().setText(R.string.activity_chat_summary_page));
        chatSectionsPagerAdapter = new ChatSectionsPagerAdapter(getSupportFragmentManager(), activity_chat_tab_layout.getTabCount());
        activity_chat_view_pager.setAdapter(chatSectionsPagerAdapter);
        activity_chat_view_pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(activity_chat_tab_layout));
        activity_chat_tab_layout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                activity_chat_view_pager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if (eventSource != null)
                eventSource.close();
        } catch (IOException e) {
            Log.e("TAG", "Exception: " + e.getMessage());
        }
    }

    public void setRecyclerView(RecyclerView recyclerView) {
        chatStickyHeadersAdapter.setRecyclerView(recyclerView);
    }

    public Ticket getTicket() {
        return ticket;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (ticket.getManager() != null)
            activity_chat_toolbar.setTitle(ticket.getManager().getFullname());
        getMenuInflater().inflate(R.menu.menu_chat, menu);
        MenuItem menu_action_list = menu.findItem(R.id.menu_action_list);
        if (ticket == null)
            return super.onCreateOptionsMenu(menu);
        if (ticket.getTICKET_state() == TICKET_STATE.PROCESS || ticket.getFeedback() != null)
            menu_action_list.setVisible(false);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                supportFinishAfterTransition();
                return true;
            case R.id.menu_action_call:
                Utility.startCall(this, ticket.getManager().getPhone(), ticket.getManager().getId());
                return true;

            case R.id.menu_action_list:
                activity_chat_view_pager.setCurrentItem(1);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void loadData() {
        Log.i("TAG", "data " + "ticketId=" + ticket.getId());
        final Dialog dialog = DialogUtility.getWaitDialog(this, getString(R.string.wait), false);
        dialog.show();
        chatStickyHeadersAdapter.clear();
        String url = "http://reactionapp.artwebit.ru/api/chat/index?ticketId=" + ticket.getId() + "&uid=1&fromId=" + PreferencesData.getLoginID(this);
        Log.e("TAG", "url " + url);
        RequestParameters requestParameters = new RequestParameters(Query.TYPE_REQUEST.GET, -1, url);
        requestParameters.setHeader(Arrays.asList(new BasicHeader("X-Username", PreferencesData.getLoginName(this)),
                new BasicHeader("X-Password", PreferencesData.getLoginPassword(this))));
        Query.getInstance().setQuery(this, new CustomRequestHandler(this, false) {
            @Override
            public void processingRequest(RequestInterface requestInterface, Object response, int code, int type, Header[] headers) {
                Utility.closeDialog(dialog);
                Log.e("TAG", "data " + data);
                super.processingRequest(requestInterface, response, code, type, headers);
                if (!errorInfo.isResult()) {
                    requestInterface.error(errorInfo);
                } else {
                    List<Message> messages = JsonParser.parseMessages(data);
                    Log.e("TAG", "data =" + data.toString());
                    requestInterface.finish(messages);
                }

            }
        }, new RequestInterface() {
            @Override
            public void finish(Object obj) {
                reconnectSSE();
                messageFiles.clear();
                messageFiles.addAll(ticket.getImages());
                List<Message> messages = (ArrayList<Message>) obj;
                for (Message message : messages) {
                    for (MessageFile messageFile : message.getMessageFiles()) {
                        String filenameArray[] = messageFile.getFileName().split("\\.");
                        String extension = filenameArray[filenameArray.length - 1];
                        if (Arrays.asList("jpeg", "jpg", "png", "gif").contains(extension)) {
                            messageFiles.add(messageFile.getUrl());
                        }
                        else
                        if (Arrays.asList("mp4","avi","3gp","flv","ogg","ogv","webm","mkv").contains(extension)) {
                            Log.d("TAG","LINK        "+messageFile.getUrl());
                            messageFiles.add(messageFile.getUrl().replace(extension,"jpg"));
                        }

                    }
                }
                imagesAdapter.notifyDataSetChanged();
                chatStickyHeadersAdapter.clear();
                chatStickyHeadersAdapter.addAll(messages);
                chatStickyHeadersAdapter.scrollToLastPosition();

            }

            @Override
            public void error(Object obj) {
            }
        }, requestParameters);


    }

    public String extractYoutubeId(String url) throws MalformedURLException {
        String query = new URL(url).getQuery();
        String[] param = query.split("&");
        String id = null;
        for (String row : param) {
            String[] param1 = row.split("=");
            if (param1[0].equals("v")) {
                id = param1[1];
            }
        }
        return id;
    }

    public void reconnectSSE() {
        new SSETask().execute();
        int timeBetweenChecks = 1000 * 30;
        Handler h = new Handler();
        h.postDelayed(new Runnable() {
            public void run() {
                reconnectSSE();
            }
        }, timeBetweenChecks);
    }

    public ChatStickyHeadersAdapter getChatStickyHeadersAdapter() {
        return chatStickyHeadersAdapter;
    }
    private void parseSSE(final long SSEcode)
    {
        try {

            SseEventReader reader = eventSource.getEventReader(); // Receive event stream

            SseEventType type = reader.next();
            //while (type != SseEventType.EOS) { // Wait until type is DATA
                Log.i("TAG2", "new event");
                switch (type) {
                    case DATA:
                        if (lastSSECode != SSEcode)
                            return;
                        // Return the payload of the last received event
                        Log.i("TAG2", "event" + reader.getData());
                        JSONObject jsonObject = new JSONObject(reader.getData().toString());
                        if (jsonObject == null)
                            break;
                        final Message resultMessage = JsonParser.parseMessageSSE(jsonObject);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                addMessageToList(resultMessage);
                            }
                        });

                        break;
                    case EMPTY:
                        Log.i("TAG2", "EMPTY");
                        break;
                }
            if ((type != SseEventType.EOS))
                parseSSE(SSEcode);
            else
                Log.e("TAG2", "sseEventReaderThread end");
            //}

        } catch (Exception e) {
            Log.e("TAG2", "sseEventReaderThread Exception: " + e.getMessage());
        }
    }
    private void read(final long SSEcode) {

        sseEventReaderThread = new Thread() {
            public void run() {
                Log.e("TAG2", "sseEventReaderThread run");
                parseSSE(SSEcode);
            }
        };
        sseEventReaderThread.start();
    }

    public void addMessageToList(Message resultMessage) {
        if (resultMessage == null)
            return;
        getChatStickyHeadersAdapter().add(resultMessage);
        getChatStickyHeadersAdapter().scrollToLastPosition();
        for (MessageFile messageFile : resultMessage.getMessageFiles())
            messageFiles.add(messageFile.getUrl());
        imagesAdapter.notifyDataSetChanged();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("TAG", "onActivityResult " );
        if (resultCode != Activity.RESULT_OK)
            return;
        Log.d("TAG", "RESULT_OK ");
        if (data==null) {
            if (requestCode == IMAGE_CAPTURE) {
                File f = new File(lastPhotoPath);
                chatFiles.add(new TicketFile(FILE_TYPE.DOC, f));
            }
            chatSectionsPagerAdapter.getChatFragment().addMessage();
            return;
        }
        Log.d("TAG", "requestCode " + requestCode);
        if (requestCode == VIDEO_CAPTURE) {
            Log.e("TAG", "data!!!!!!!!!!!!!!!!!!!!! =" + data.getData());
            Uri uri = data.getData();
            String path = getPath(uri);
            File file = new File(path);
            if (file.exists()) {
                chatFiles.add(new TicketFile(FILE_TYPE.DOC, file));
                chatSectionsPagerAdapter.getChatFragment().addMessage();
                return;
            } else {
                Log.e(TAG, "File " + data.getData() + " doesn't exist!");
            }
        }
        if (requestCode == UPLOAD_FILE) {
            Uri uri = data.getData();
            String path = getPath(uri);
            File file = new File(path);
            if (uri.toString().contains("com.google.android"))
                try {
                    file = Utility.downloadFromDrive(ChatActivity.this, uri);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            Log.e("TAG", "path!!!!!!!!!!!!!!!!!!!!! =" + file.getAbsolutePath());
            if (file.exists()) {
                chatFiles.add(new TicketFile(FILE_TYPE.DOC, file));
                chatSectionsPagerAdapter.getChatFragment().addMessage();
                return;
            } else {
                Log.e(TAG, "File " + data.getData() + " doesn't exist!");
            }

        }

    }

    public void takePhoto() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
            }
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, IMAGE_CAPTURE);
            }
        }
    }
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        lastPhotoPath =  image.getAbsolutePath();
        return image;
    }
    public void takeVideo()
    {
        Intent videoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        startActivityForResult(videoIntent, VIDEO_CAPTURE);
    }
    public void uploadFile() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select File"), UPLOAD_FILE);
    }

    public String getPath(Uri uri) {
        String path = null;
        String[] projection = {MediaStore.Files.FileColumns.DATA};
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);

        if (cursor == null) {
            path = uri.getPath();
        } else {
            cursor.moveToFirst();
            int column_index = cursor.getColumnIndexOrThrow(projection[0]);
            path = cursor.getString(column_index);
            cursor.close();
        }

        return ((path == null || path.isEmpty()) ? (uri.getPath()) : path);
    }

    public List<TicketFile> getChatFiles() {
        return chatFiles;
    }

    public void clearChatFiles() {
        chatFiles.clear();
    }

    public ImagesAdapter getImagesAdapter() {
        return imagesAdapter;
    }

    class SSETask extends AsyncTask<String, Void, Void> {
        protected Void doInBackground(String... urls) {
            try {
                SseEventSourceFactory factory = SseEventSourceFactory.createEventSourceFactory();
                String url = "http://reactionapp.artwebit.ru/api/chat/pusher?ticketId=" +
                        ticket.getId() + "&forId=" + PreferencesData.getLoginID(ChatActivity.this);
                Log.e("TAG", "SSE url: " + url);
                eventSource = factory.createEventSource(URI.create(url));
                eventSource.close();
                eventSource.connect();
                Random random = new Random();
                lastSSECode = random.nextLong();
                read(lastSSECode);

            } catch (URISyntaxException e) {
                Log.e("TAG", "Exception: " + e.getMessage());
            } catch (IOException e) {
                Log.e("TAG", "connect Exception: " + e.getMessage());
            }

            return null;
        }

        protected void onPostExecute(Void param) {

        }
    }
}