package ngse.reactionapp.Activities;

import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.NGSE.RequestLibrary.Query;
import com.NGSE.RequestLibrary.RequestInterface;
import com.NGSE.RequestLibrary.RequestParameters;

import org.apache.http.Header;
import org.apache.http.message.BasicHeader;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import ngse.reactionapp.Adapters.SearchResultRecycleViewAdapter;
import ngse.reactionapp.Data.PreferencesData;
import ngse.reactionapp.Data.SearchResult;
import ngse.reactionapp.R;
import ngse.reactionapp.Utility.Constants;
import ngse.reactionapp.Utility.CustomRequestHandler;
import ngse.reactionapp.Utility.DialogUtility;
import ngse.reactionapp.Utility.JsonParser;
import ngse.reactionapp.Utility.Utility;


public class SearchActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    private static final String EXTRA_QUERY = "extraQuery";

    @Bind(R.id.activity_search_recycler_view)
    RecyclerView activity_search_recycler_view;
    @Bind(R.id.activity_search_toolbar)
    Toolbar activity_search_toolbar;
    private String extraQuery;
    private SearchResultRecycleViewAdapter searchResultRecycleViewAdapter;
    private List<SearchResult> searchResults = new ArrayList<>();

    public static void newInstance(AppCompatActivity activity, String extraQuery) {
        Intent intent = new Intent(activity, SearchActivity.class);
        intent.putExtra(EXTRA_QUERY, extraQuery);
        ActivityCompat.startActivity(activity, intent, null);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);
        setSupportActionBar(activity_search_toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        extraQuery = getIntent().getStringExtra(EXTRA_QUERY);
        activity_search_recycler_view.setLayoutManager(new LinearLayoutManager(this));
        searchResultRecycleViewAdapter=new SearchResultRecycleViewAdapter(this, searchResults, new SearchResultRecycleViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, SearchResult searchResult) {
                if (searchResult.getType().toLowerCase().equals("company"))
                 CompanyActivity.newInstance(SearchActivity.this,searchResult.getId());
            }
        });
        activity_search_recycler_view.setAdapter(searchResultRecycleViewAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_action_search, menu);
        MenuItem searchItem = menu.findItem(R.id.menu_action_search);
        SearchManager searchManager = (SearchManager) SearchActivity.this.getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = null;
        if (searchItem != null)
            searchView = (SearchView) searchItem.getActionView();

        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(SearchActivity.this.getComponentName()));
            searchView.setQueryHint(getString(R.string.search));
            searchView.setOnQueryTextListener(this);
            searchView.setQuery(extraQuery, true);
            searchView.setIconified(false);

        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
       // Utility.showToast(this, query);
        loadData(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    private void loadData(String query) {
        final Dialog dialog = DialogUtility.getWaitDialog(this, getString(R.string.wait), false);
        dialog.show();
        searchResults.clear();
        searchResultRecycleViewAdapter.notifyDataSetChanged();
        RequestParameters requestParameters = null;
        try {
            requestParameters = new RequestParameters(Query.TYPE_REQUEST.GET, -1, Constants.SEARCH + URLEncoder.encode(query, "UTF-8"));
            requestParameters.setHeader(Arrays.asList(new BasicHeader("X-Username", PreferencesData.getLoginName(this)),
                    new BasicHeader("X-Password", PreferencesData.getLoginPassword(this))));
            Query.getInstance().setQuery(this, new CustomRequestHandler(this, false) {
                @Override
                public void processingRequest(RequestInterface requestInterface, Object response, int code, int type, Header[] headers) {
                    Utility.closeDialog(dialog);
                    super.processingRequest(requestInterface, response, code, type, headers);
                    if (!errorInfo.isResult()) {
                        requestInterface.error(errorInfo);
                    } else {
                        List<SearchResult> searchResults = JsonParser.parseSearchResults(data);
                        requestInterface.finish(searchResults);

                    }

                }
            }, new RequestInterface() {
                @Override
                public void finish(Object obj) {
                    searchResults.clear();
                    searchResults.addAll((ArrayList<SearchResult>) obj);
                    Collections.reverse(searchResults);
                    searchResultRecycleViewAdapter.notifyDataSetChanged();
                }

                @Override
                public void error(Object obj) {
                }
            }, requestParameters);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }
}