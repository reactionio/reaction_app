package ngse.reactionapp.Activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ngse.reactionapp.R;


public class SplashActivity extends AppCompatActivity {


    @Bind(R.id.activity_splash_continue)
    TextView activity_splash_continue;


    public static void newInstance(AppCompatActivity activity) {
        Intent intent = new Intent(activity, SplashActivity.class);
        ActivityCompat.startActivity(activity, intent, null);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);

    }
    @OnClick(R.id.activity_splash_continue)
    void setActivity_splash_continue()
    {
        NavUtils.navigateUpFromSameTask(this);
    }

}