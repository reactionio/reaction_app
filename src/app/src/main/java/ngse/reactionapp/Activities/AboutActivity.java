package ngse.reactionapp.Activities;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ngse.reactionapp.R;


public class AboutActivity extends AppCompatActivity {


    @Bind(R.id.activity_about_version)
    TextView activity_about_version;
    @Bind(R.id.activity_about_toolbar)
    Toolbar activity_about_toolbar;

    public static void newInstance(AppCompatActivity activity) {
        Intent intent = new Intent(activity, AboutActivity.class);
        ActivityCompat.startActivity(activity, intent, null);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        ButterKnife.bind(this);
        setSupportActionBar(activity_about_toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            activity_about_version.setText(pInfo.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

    }

    @OnClick(R.id.activity_about_terms_of_service)
    void activity_about_terms_of_service(TextView view) {
        InfoActivity.newInstance(this, view.getText().toString());
    }

    @OnClick(R.id.activity_about_privacy_policy)
    void activity_about_privacy_policy(TextView view) {
        InfoActivity.newInstance(this, view.getText().toString());
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getSupportActionBar().setTitle(R.string.settings_fragment_about);

        return super.onCreateOptionsMenu(menu);
    }
}