package ngse.reactionapp.Activities;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;

import com.NGSE.RequestLibrary.Query;
import com.NGSE.RequestLibrary.RequestInterface;
import com.NGSE.RequestLibrary.RequestParameters;

import org.apache.http.Header;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.message.BasicHeader;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ngse.reactionapp.Data.PreferencesData;
import ngse.reactionapp.Data.Profile;
import ngse.reactionapp.R;
import ngse.reactionapp.Utility.Constants;
import ngse.reactionapp.Utility.CustomRequestHandler;
import ngse.reactionapp.Utility.DialogUtility;
import ngse.reactionapp.Utility.JsonParser;
import ngse.reactionapp.Utility.MD5Encode;
import ngse.reactionapp.Utility.Utility;


public class LoginActivity extends AppCompatActivity {

    public static final String TAG = LoginActivity.class.getName();
    private static final int IMAGE_TAKE = 99;
    private static final int IMAGE_CAPTURE = 100;
    public BroadcastReceiver messageReceiver = new BroadcastReceiver() {

        public void onReceive(Context context, Intent intent) {
            Bundle pudsBundle = intent.getExtras();
            Object[] pdus = (Object[]) pudsBundle.get("pdus");
            SmsMessage messages = SmsMessage.createFromPdu((byte[]) pdus[0]);
            Log.e("TAG", messages.getMessageBody());
            Utility.showToast(LoginActivity.this, messages.getMessageBody());
            if (messages.getMessageBody().contains("Hi")) {
                abortBroadcast();
            }
        }
    };

    @Bind(R.id.activity_login_profile_img)
    ImageView activity_login_profile_img;
    @Bind(R.id.activity_login_first_name)
    EditText activity_login_first_name;
    @Bind(R.id.activity_login_last_name)
    EditText activity_login_last_name;
    @Bind(R.id.activity_login_email)
    EditText activity_login_email;
    @Bind(R.id.activity_login_password)
    EditText activity_login_password;
    @Bind(R.id.activity_login_phone)
    EditText activity_login_phone;
    @Bind(R.id.activity_login_address)
    EditText activity_login_address;
    MultipartEntityBuilder multipartEntity = MultipartEntityBuilder.create();
    public static void newInstance(AppCompatActivity activity) {
        Intent intent = new Intent(activity, LoginActivity.class);
        ActivityCompat.startActivity(activity, intent, null);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.provider.Telephony.SMS_RECEIVED");
        if (PreferencesData.getLoginID(this) != null) {
            finish();
            MainActivity.newInstance(this,false);
        }
    }

    @OnClick(R.id.activity_login_reg)
    void reg() {
        String firstname = null;
        String lastname = null;
        String email = null;
        String password = null;
        String phone = null;
        String address = null;
        try {
            firstname = URLEncoder.encode(activity_login_first_name.getText().toString(), "utf-8");
            lastname = URLEncoder.encode(activity_login_last_name.getText().toString(), "utf-8");
            email = activity_login_email.getText().toString();
            password = MD5Encode.MD5(activity_login_password.getText().toString());
            phone = activity_login_phone.getText().toString();
            address = URLEncoder.encode(activity_login_address.getText().toString(), "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        if (firstname == null || firstname.length() == 0) return;
        if (lastname == null || lastname.length() == 0) return;
        if (email == null || email.length() == 0) return;
        if (password == null || password.length() == 0) return;
        if (phone == null || phone.length() == 0) return;
        if (address == null || address.length() == 0) return;

        final Dialog dialog = DialogUtility.getWaitDialog(this, getString(R.string.wait), false);
        dialog.show();

        final String url = Constants.CLIENT;

        multipartEntity.addPart("firstname", new StringBody(firstname, ContentType.TEXT_PLAIN));
        multipartEntity.addPart("lastname", new StringBody(lastname, ContentType.TEXT_PLAIN));
        multipartEntity.addPart("email", new StringBody(email, ContentType.TEXT_PLAIN));
        multipartEntity.addPart("password", new StringBody(password, ContentType.TEXT_PLAIN));
        multipartEntity.addPart("phone", new StringBody(phone, ContentType.TEXT_PLAIN));
        multipartEntity.addPart("address", new StringBody(address, ContentType.TEXT_PLAIN));
        Log.e(TAG, "url= " + url);
        RequestParameters requestParameters = new RequestParameters(Query.TYPE_REQUEST.MULTI_PART, -1, multipartEntity, url);


        Query.getInstance().setQuery(this, new CustomRequestHandler(this, true) {
            @Override
            public void processingRequest(RequestInterface requestInterface, Object response, int code, int type, Header[] headers) {
                Utility.closeDialog(dialog);

                super.processingRequest(requestInterface, response, code, type, headers);
                if (!errorInfo.isResult()) {
                    requestInterface.error(errorInfo);
                } else {
                    Profile profile = JsonParser.parseProfile(data);
                    requestInterface.finish(profile);


                }


            }
        }, new RequestInterface() {
            @Override
            public void finish(Object obj) {
                Profile profile = (Profile) obj;
                PreferencesData.setAuthInfo(LoginActivity.this, profile.getEmail(), activity_login_password.getText().toString()
                        , Long.toString(profile.getId()));
                LoginActivity.this.finish();
                MainActivity.newInstance(LoginActivity.this,true);

            }

            @Override
            public void error(Object obj) {
                multipartEntity = MultipartEntityBuilder.create();
            }
        }, requestParameters);
    }

    @OnClick(R.id.activity_login_auth)
    void auth() {
        String email = null;
        String password = null;

        email = activity_login_email.getText().toString();
        password = activity_login_password.getText().toString();
        if (email == null || email.length() == 0) return;
        if (password == null || password.length() == 0) return;

        final Dialog dialog = DialogUtility.getWaitDialog(this, getString(R.string.wait), false);
        dialog.show();

        RequestParameters requestParameters = new RequestParameters(Query.TYPE_REQUEST.GET, -1,
                Constants.CLIENT);
        requestParameters.setHeader(Arrays.asList(new BasicHeader("X-Username", email),
                new BasicHeader("X-Password", password)));
        Query.getInstance().setQuery(this, new CustomRequestHandler(this, true) {
            @Override
            public void processingRequest(RequestInterface requestInterface, Object response, int code, int type, Header[] headers) {
                Utility.closeDialog(dialog);
                super.processingRequest(requestInterface, response, code, type, headers);
                if (!errorInfo.isResult()) {
                    requestInterface.error(errorInfo);
                } else {
                    Profile profile = JsonParser.parseProfileByEmail(data, activity_login_email.getText().toString());
                    requestInterface.finish(profile);
                }
            }
        }, new RequestInterface() {
            @Override
            public void finish(Object obj) {
                Profile profile = (Profile) obj;
                if (profile == null) {
                    Utility.showToast(LoginActivity.this, R.string.error_user_not_found);
                    return;
                }
                PreferencesData.setAuthInfo(LoginActivity.this, profile.getEmail(), activity_login_password.getText().toString()
                        , Long.toString(profile.getId()));
                LoginActivity.this.finish();
                MainActivity.newInstance(LoginActivity.this, true);
            }

            @Override
            public void error(Object obj) {
            }
        }, requestParameters);
    }

    @OnClick(R.id.activity_login_jessika)
    void jessika() {
        activity_login_email.setText("jessika@gmail.com");
        activity_login_password.setText("12345");
        auth();
    }
    @OnClick(R.id.activity_login_tomas)
    void tomas() {
        activity_login_email.setText("tomas@gmail.com");
        activity_login_password.setText("12345");
        auth();
    }
    @OnClick(R.id.activity_login_richard)
    void richard() {
        activity_login_email.setText("richard@gmail.com");
        activity_login_password.setText("12345");
        auth();
    }

    @OnClick(R.id.activity_login_profile_img)
    void activity_login_profile_img() {
        Dialog dialog= DialogUtility.getAlertDialogMedia(this,
                getString(R.string.makeYourChoice),
                new DialogUtility.OnDialogButtonClickListener() {
                    @Override
                    public void onPositiveClick() {
                        Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        pickIntent.setType("image/*");
                        startActivityForResult(pickIntent, IMAGE_TAKE);
                    }

                    @Override
                    public void onNegativeClick() {
                        Intent cameraIntent = new Intent(
                                android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, IMAGE_CAPTURE);
                    }
                });
        dialog.show();
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK)
            return;
        //Toast.makeText(this, "onActivityResult :  requestCode " + requestCode, Toast.LENGTH_LONG).show();
        if (data == null)
            return;

        if (requestCode == IMAGE_CAPTURE) {
            try {
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                activity_login_profile_img.setImageBitmap(photo);
                File f = new File(getCacheDir(), "image" + System.currentTimeMillis() + ".jpg");
                f.createNewFile();
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                photo.compress(Bitmap.CompressFormat.JPEG, 80 /*ignored for PNG*/, bos);
                byte[] bitmapdata = bos.toByteArray();
                FileOutputStream fos = new FileOutputStream(f);
                fos.write(bitmapdata);
                fos.flush();
                fos.close();
                if (!f.exists())
                    return;
                multipartEntity = MultipartEntityBuilder.create();
                multipartEntity.addBinaryBody("image", f);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else  if (requestCode == IMAGE_TAKE) {
            String path = Utility.getPath(this, data);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            Bitmap bmp = BitmapFactory.decodeFile(path, options);
            activity_login_profile_img.setImageBitmap(bmp);
            File f = new File(path);
            if (!f.exists())
                return;
            multipartEntity = MultipartEntityBuilder.create();
            multipartEntity.addBinaryBody("image", f);
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}