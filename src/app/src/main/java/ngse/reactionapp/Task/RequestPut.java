package ngse.reactionapp.Task;

import android.content.Context;

import com.NGSE.RequestLibrary.RequestBase;
import com.NGSE.RequestLibrary.RequestHandler;
import com.NGSE.RequestLibrary.RequestInterface;
import com.NGSE.RequestLibrary.RequestParameters;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.util.ArrayList;

/**
 * Created by Kulykovs on 28.09.2015.
 */
@SuppressWarnings("deprecation")
public class RequestPut extends RequestBase {


    public RequestPut(Context context, RequestHandler requestHandler, RequestInterface requestInterface, int type) {
        super(context, requestHandler, requestInterface, type);
    }

    protected ArrayList<Object> getUrlData(RequestParameters param)
            throws IOException, URISyntaxException, KeyManagementException,
            UnrecoverableKeyException, NoSuchAlgorithmException,
            KeyStoreException, UnsupportedEncodingException {
        int statusCode;
        ArrayList<Object> response = new ArrayList<Object>();
        HttpParams httpParameters = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParameters,
                param.getConnection_timeout());
        HttpConnectionParams.setSoTimeout(httpParameters,
                param.getConnection_timeout());

        return null;
//
//        SchemeRegistry scheme = new SchemeRegistry();
//
//        scheme.register(new Scheme("http", PlainSocketFactory
//                .getSocketFactory(), 80));
//        scheme.register(new Scheme("https", SSLSocketFactory
//                .getSocketFactory(), 443));
//        ClientConnectionManager conMgr = new ThreadSafeClientConnManager(
//                httpParameters, scheme);
//
//        DefaultHttpClient client = new DefaultHttpClient(conMgr, httpParameters);
//        HttpPut httpPut = new HttpPut(param.getUrl());
//        if (param.getHeader() != null) {
//            httpPut.setHeaders(param.getHeader().toArray(
//                    new Header[param.getHeader().size()]));
//        }
//        if (param.getBody() == null) {
//            UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(
//                    param.getParamsList());
//            httpPut.setEntity(urlEncodedFormEntity);
//        } else {
//            httpPut.setEntity(new StringEntity(param.getBody(), HTTP.UTF_8));
//        }
//
//        //set cookies
//        setCookies(httpPut);
//
//        HttpResponse res = client.execute(httpPut);
//
//        //save cookies
//        saveCookies(client);
//
//        statusCode = res.getStatusLine().getStatusCode();
//
//        response.add(statusCode);
//        if (param.isReturnString()) {
//            response.add(generateString((InputStream) res.getEntity()
//                    .getContent()));
//        } else {
//            response.add((InputStream) res.getEntity().getContent());
//        }
//        response.add(res.getAllHeaders());
//        return response;
    }
}