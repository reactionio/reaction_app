package ngse.reactionapp.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import ngse.reactionapp.Fragments.CallLogsFragment;
import ngse.reactionapp.Fragments.SettingsFragment;
import ngse.reactionapp.Fragments.TicketPageFragment;

/**
 * Created by Kulykov Anton on 04.11.15.
 */
public class MainSectionsPagerAdapter extends FragmentPagerAdapter {
    int numOfTabs;
    TicketPageFragment ticketPageFragment;
    public MainSectionsPagerAdapter(FragmentManager fm, int numOfTabs) {
        super(fm);
        this.numOfTabs = numOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new CallLogsFragment();
            case 1:
                ticketPageFragment=new TicketPageFragment();
                return ticketPageFragment;
            case 2:
            default:
                return new SettingsFragment();
        }

    }

    @Override
    public int getCount() {
        return numOfTabs;
    }

    public TicketPageFragment getTicketPageFragment() {
        return ticketPageFragment;
    }



}