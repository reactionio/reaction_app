package ngse.reactionapp.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.NGSE.textviewpluslib.TextViewPlus;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import ngse.reactionapp.Data.SearchResult;
import ngse.reactionapp.R;

public class SearchResultRecycleViewAdapter extends RecyclerView.Adapter<ViewHolder> implements View.OnClickListener {


    private Context context;
    private List<SearchResult> items = new ArrayList<>();
    private OnItemClickListener onItemClickListener;
    public SearchResultRecycleViewAdapter(Context context, List<SearchResult> items,OnItemClickListener onItemClickListener) {
        this.context = context;
        this.items = items;
        this.onItemClickListener=onItemClickListener;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int type) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item_search_result, viewGroup, false);
        v.setOnClickListener(this);
        return new VHItem(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        final VHItem vhItem = (VHItem) viewHolder;
        final SearchResult item = items.get(position);
        vhItem.itemView.setTag(item);
        vhItem.list_item_search_result_title.setText(item.getTitle());
        vhItem.list_item_search_result_type.setText(item.getType());
        vhItem.list_item_search_result_description.setText(item.getDescription());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onClick(View v) {
        if (onItemClickListener != null)
            onItemClickListener.onItemClick(v, (SearchResult) v.getTag());
    }


    class VHItem extends ViewHolder {
        @Bind(R.id.list_item_search_result_title)
        TextViewPlus list_item_search_result_title;
        @Bind(R.id.list_item_search_result_type)
        TextViewPlus list_item_search_result_type;
        @Bind(R.id.list_item_search_result_description)
        TextViewPlus list_item_search_result_description;

        public VHItem(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

    }
    public interface OnItemClickListener {

        void onItemClick(View view, SearchResult searchResult);

    }
}
