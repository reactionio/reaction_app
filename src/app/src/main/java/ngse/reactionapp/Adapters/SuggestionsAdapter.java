package ngse.reactionapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ngse.reactionapp.Data.Company;
import ngse.reactionapp.R;

/**
 * Created by Kulykov Anton on 07.10.15.
 */
public class SuggestionsAdapter extends ArrayAdapter<Company> {
    private final String TAG = SuggestionsAdapter.class.getName();
    private List<Company> items;
    private List<Company> itemsAll;
    private List<Company> suggestions;
    private int viewResourceId;

    public SuggestionsAdapter(Context context, int viewResourceId, List<Company> items) {
        super(context, viewResourceId, items);
        this.items = items;
        this.itemsAll= new ArrayList<>(items);
        this.suggestions = new ArrayList<>();
        this.viewResourceId = viewResourceId;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(viewResourceId, null);
        }
        Company item = items.get(position);
        if (item != null) {
            TextView customerNameLabel = (TextView) v.findViewById(R.id.suggestionsLabel);
            if (customerNameLabel != null) {
                customerNameLabel.setText(item.getName());
            }
        }
        return v;
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    Filter nameFilter = new Filter() {
        @Override
        public String convertResultToString(Object resultValue) {
            String str = ((Company)(resultValue)).getName();
            return str;
        }
        @Override
        protected Filter.FilterResults performFiltering(CharSequence constraint) {
            if(constraint != null) {
                suggestions.clear();
                    for (Company customer : itemsAll) {
                        if (customer.getName().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                            suggestions.add(customer);
                        }
                    }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<Company> filteredList = (ArrayList<Company>) results.values;
            if(results != null && results.count > 0) {
                clear();
                for (Company c : filteredList) {
                    add(c);
                }
                notifyDataSetChanged();
            }
        }
    };

}