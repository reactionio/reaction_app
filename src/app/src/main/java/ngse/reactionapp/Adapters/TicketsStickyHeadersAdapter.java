package ngse.reactionapp.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersAdapter;

import butterknife.Bind;
import butterknife.ButterKnife;
import ngse.reactionapp.Data.Ticket;
import ngse.reactionapp.R;

/**
 * Created by Kulykov Anton on 04.09.15.
 */
public class TicketsStickyHeadersAdapter extends BaseStickyRecyclerArrayAdapter<Ticket, RecyclerView.ViewHolder>
        implements StickyRecyclerHeadersAdapter<RecyclerView.ViewHolder>, View.OnClickListener {
    private OnItemClickListener onItemClickListener;

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_ticket, parent, false);
        v.setOnClickListener(this);
        v.findViewById(R.id.list_item_ticket_type).setOnClickListener(this);
        return new VHItem(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        VHItem vhItem = (VHItem) holder;
        Ticket item = getItem(position);
        if (item.getManager() != null && item.getManager().getImageUrl() != null && item.getManager().getImageUrl().length() > 0) {
            Picasso.with(vhItem.list_item_ticket_img.getContext()).load(item.getManager().getImageUrl()).
                    into(vhItem.list_item_ticket_img);
            vhItem.list_item_ticket_img.setVisibility(View.VISIBLE);
        } else
            vhItem.list_item_ticket_img.setVisibility(View.GONE);
        vhItem.list_item_ticket_name.setText(item.getTitle());
        if (item.getManager() != null)
            vhItem.list_item_ticket_company.setText(item.getManager().getFirstname());
        vhItem.list_item_ticket_date.setText(item.getUpdatedAt());
        vhItem.list_item_ticket_type.setVisibility(View.VISIBLE);
        vhItem.list_item_ticket_separator.setVisibility(View.GONE);
        switch (item.getTICKET_state()) {
            case CLOSED:
                vhItem.list_item_ticket_type.setImageResource(R.drawable.list);
                break;
            case PROCESS:

                vhItem.list_item_ticket_type.setVisibility(View.GONE);
                break;
            case RESOLVED:
                if (item.getFeedback()==null) {
                    vhItem.list_item_ticket_type.setImageResource(R.drawable.list_brown);
                    vhItem.list_item_ticket_separator.setVisibility(View.VISIBLE);
                }
                else
                    vhItem.list_item_ticket_type.setImageResource(R.drawable.list);
                break;
            case NONE:
                vhItem.list_item_ticket_type.setVisibility(View.GONE);
                break;
        }
        vhItem.itemView.setTag(item);
        vhItem.list_item_ticket_type.setTag(item);

    }

    @Override
    public long getHeaderId(int position) {
        return getItem(position).getTICKET_state().getValue();
    }

    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_header_ticket, parent, false);
        return new VHHeader(v);
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {
        VHHeader vhHeader = (VHHeader) holder;
        vhHeader.list_item_header_ticket.setText(String.valueOf(getItem(position).getStatusText()));

    }

    @Override
    public void onClick(View v) {
        if (onItemClickListener != null) {
            if (v.getId() == R.id.list_item_ticket_type)
                onItemClickListener.onRateClick((Ticket) v.getTag());
            else
                onItemClickListener.onItemClick((Ticket) v.getTag());
        }
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {

        void onItemClick(Ticket item);

        void onRateClick(Ticket item);

    }

    class VHItem extends RecyclerView.ViewHolder {

        @Bind(R.id.list_item_ticket_img)
        ImageView list_item_ticket_img;
        @Bind(R.id.list_item_ticket_name)
        TextView list_item_ticket_name;
        @Bind(R.id.list_item_ticket_company)
        TextView list_item_ticket_company;
        @Bind(R.id.list_item_ticket_date)
        TextView list_item_ticket_date;
        @Bind(R.id.list_item_ticket_separator)
        ImageView list_item_ticket_separator;
        @Bind(R.id.list_item_ticket_type)
        ImageView list_item_ticket_type;



        public VHItem(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    class VHHeader extends RecyclerView.ViewHolder {
        @Bind(R.id.list_item_header_ticket)
        TextView list_item_header_ticket;

        public VHHeader(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}