package ngse.reactionapp.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import ngse.reactionapp.Fragments.ChatFragment;
import ngse.reactionapp.Fragments.SummaryFragment;

/**
 * Created by Kulykov Anton on 04.11.15.
 */
public class ChatSectionsPagerAdapter extends FragmentPagerAdapter {
    int numOfTabs;
    private ChatFragment chatFragment;
    public ChatSectionsPagerAdapter(FragmentManager fm, int numOfTabs) {
        super(fm);
        this.numOfTabs = numOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                chatFragment=new ChatFragment();
                return chatFragment;
            case 1:
            default:
                return new SummaryFragment();
        }

    }

    @Override
    public int getCount() {
        return numOfTabs;
    }

    public ChatFragment getChatFragment() {
        return chatFragment;
    }
}