package ngse.reactionapp.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import ngse.reactionapp.R;

public class ImagesAdapter extends RecyclerView.Adapter<ViewHolder> {


    private Context context;
    private List<String> items;
    private OnItemClickListener onItemClickListener;


    public ImagesAdapter(Context context, List<String> items, OnItemClickListener onItemClickListener) {
        this.context = context;
        this.items = items;
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int type) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item_summary_image, viewGroup, false);
        return new VHItem(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        final VHItem vhItem = (VHItem) viewHolder;
        final String item = getItem(position);
        Log.e("TAG", "!!!!!!!!!url= " + item);
        Picasso.with(vhItem.list_item_chat_image.getContext()).load(item).into(vhItem.list_item_chat_image);

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public String getItem(int position) {
        return items.get(position);
    }


    public interface OnItemClickListener {
        void onItemClick(int position);

    }
    class VHItem extends ViewHolder implements View.OnClickListener {
        @Bind(R.id.list_item_chat_image)
        ImageView list_item_chat_image;

        public VHItem(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {
            onItemClickListener.onItemClick(getAdapterPosition());

        }
    }

}
