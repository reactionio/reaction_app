package ngse.reactionapp.Adapters;

/**
 * Created by Kulykov Anton on 13.11.15.
 */

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ImgPagerAdapter extends PagerAdapter {
    private Context context;
    private ArrayList<String> messageFiles=new ArrayList<>();
    private int count;

    public ImgPagerAdapter(Context context, ArrayList<String> messageFiles, final ViewPager pager) {
        this.context = context;
        // to circular
        if (messageFiles.size() > 0) {
            if (messageFiles.size() == 1) {
                count = messageFiles.size();
                this.messageFiles = messageFiles;
            } else {
                count = messageFiles.size() + 2;
                this.messageFiles.add(messageFiles.get(messageFiles.size() - 1));
                this.messageFiles.addAll(messageFiles);
                this.messageFiles.add(messageFiles.get(0));
                pager.setOnPageChangeListener(new OnPageChangeListener() {

                    public void onPageSelected(int position) {
                        int pageCount = getCount();
                        if (position == 0) {
                            pager.setCurrentItem(pageCount - 2, false);
                        } else if (position == pageCount - 1) {
                            pager.setCurrentItem(1, false);
                        }
                    }

                    public void onPageScrolled(int position, float positionOffset,
                                               int positionOffsetPixels) {
                    }

                    public void onPageScrollStateChanged(int state) {

                    }
                });
            }


        }
    }

    @Override
    public int getCount() {
        return count;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void setPrimaryItem(View container, final int position, Object object) {

    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        final ImageView imageView = new ImageView(context);
        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        final String url;
        url = messageFiles.get(position);
        Picasso.with(imageView.getContext()).load(url).into(imageView);
        container.addView(imageView, 0);

        return imageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((ImageView) object);
    }
}
