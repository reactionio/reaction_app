package ngse.reactionapp.Adapters;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.NGSE.textviewpluslib.TextViewPlus;
import com.squareup.picasso.Picasso;
import com.thin.downloadmanager.DefaultRetryPolicy;
import com.thin.downloadmanager.DownloadRequest;
import com.thin.downloadmanager.DownloadStatusListener;
import com.thin.downloadmanager.ThinDownloadManager;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersAdapter;

import butterknife.Bind;
import butterknife.ButterKnife;
import ngse.reactionapp.Data.Message;
import ngse.reactionapp.Data.MessageFile;
import ngse.reactionapp.Data.PreferencesData;
import ngse.reactionapp.Data.Ticket;
import ngse.reactionapp.R;
import ngse.reactionapp.Utility.Utility;

/**
 * Created by Kulykov Anton on 04.09.15.
 */
public class ChatStickyHeadersAdapter extends BaseStickyRecyclerArrayAdapter<Message, RecyclerView.ViewHolder>
        implements StickyRecyclerHeadersAdapter<RecyclerView.ViewHolder>, View.OnClickListener {
    private OnItemClickListener onItemClickListener;

    private ThinDownloadManager downloadManager;
    private static final int DOWNLOAD_THREAD_POOL_SIZE = 2;
    private Context context;
    private RecyclerView recyclerView;

    public ChatStickyHeadersAdapter(Context context) {
        this.context=context;
        downloadManager = new ThinDownloadManager(DOWNLOAD_THREAD_POOL_SIZE);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_chat, parent, false);
        return new VHItem(v);
    }

    public void setRecyclerView(RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
    }

    public RecyclerView getRecyclerView() {
        return recyclerView;
    }
    public void scrollToLastPosition()
    {
        if (recyclerView!=null)
        recyclerView.scrollToPosition(size() - 1);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        final VHItem vhItem = (VHItem) holder;
        final Message item = getItem(position);
        if (item.getSender() == null)
            return;
        if (item.getSender().getId() == Long.parseLong(PreferencesData.getLoginID(vhItem.list_item_chat_me.getContext()))) {
            if (item.getMessageBody().length()>0) {
                vhItem.list_item_chat_me.setVisibility(View.VISIBLE);
                vhItem.list_item_chat_my_message.setText(item.getMessageBody());
            }
            else
            {
                vhItem.list_item_chat_me.setVisibility(View.GONE);
            }
            vhItem.list_item_chat_interlocutor.setVisibility(View.GONE);

            Picasso.with(vhItem.list_item_chat_my_img.getContext()).load(item.getSender().getImage()).into(vhItem.list_item_chat_my_img);

            vhItem.list_item_chat_my_message_date.setVisibility(View.GONE);
            vhItem.list_item_chat_my_message.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    vhItem.list_item_chat_my_message_date.setText(item.getCreateAtTime());
                    vhItem.list_item_chat_my_message_date.setVisibility(View.VISIBLE);
                }
            });
        } else {
            if (item.getMessageBody().length()>0) {
                vhItem.list_item_chat_interlocutor.setVisibility(View.VISIBLE);
                vhItem.list_item_chat_interlocutor_message.setText(item.getMessageBody());
            }
            else
            {
                vhItem.list_item_chat_interlocutor.setVisibility(View.GONE);
            }
            vhItem.list_item_chat_me.setVisibility(View.GONE);

            Picasso.with(vhItem.list_item_chat_interlocutor_img.getContext()).load(item.getSender().getImage()).into(vhItem.list_item_chat_interlocutor_img);

            vhItem.list_item_chat_interlocutor_message_date.setVisibility(View.GONE);
            vhItem.list_item_chat_interlocutor_message.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    vhItem.list_item_chat_interlocutor_message_date.setText(item.getCreateAtTime());
                    vhItem.list_item_chat_interlocutor_message_date.setVisibility(View.VISIBLE);
                }
            });
            vhItem.list_item_chat_interlocutor_message_name.setText(item.getSender().getName());
        }
        if (item.getMessageFiles().size() == 0) {
            vhItem.list_item_chat_attachments.setVisibility(View.GONE);
            vhItem.list_item_chat_attachments_text.setVisibility(View.GONE);
        }
        else {
            vhItem.list_item_chat_attachments.setVisibility(View.VISIBLE);
            vhItem.list_item_chat_attachments_text.setVisibility(View.VISIBLE);
            vhItem.list_item_chat_attachments.removeAllViews();
            for (final MessageFile messageFile:item.getMessageFiles())
            {
                View child = LayoutInflater.from(holder.itemView.getContext()).inflate(R.layout.list_item_chat_attachment, null);
                TextView list_item_chat_attachment_name= (TextView) child.findViewById(R.id.list_item_chat_attachment_name);
                list_item_chat_attachment_name.setText(messageFile.getFileName());
                vhItem.list_item_chat_attachments.addView(child);
                child.findViewById(R.id.list_item_chat_attachment_download);
                child.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Uri downloadUri = Uri.parse(messageFile.getUrl());
                        Uri destinationUri = Uri.parse(context.getExternalCacheDir().toString() +"/"+ messageFile.getFileName());
                        DownloadRequest downloadRequest = new DownloadRequest(downloadUri)
                                .setRetryPolicy(new DefaultRetryPolicy())
                                .setDestinationURI(destinationUri).setPriority(DownloadRequest.Priority.HIGH)
                                .setDownloadListener(new DownloadStatusListener() {
                                    @Override
                                    public void onDownloadComplete(int id) {
                                        Utility.showToast(holder.itemView.getContext(), "download complete " + messageFile.getFileName());
                                    }

                                    @Override
                                    public void onDownloadFailed(int id, int errorCode, String errorMessage) {
                                        Utility.showToast(holder.itemView.getContext(), "download failed " + errorMessage);
                                    }

                                    @Override
                                    public void onProgress(int id, long totalBytes, long downlaodedBytes, int progress)
                                    {
                                            Log.i("TAG","progress "+progress);
                                    }
                                });
                        downloadManager.add(downloadRequest);
                        Utility.showToast(holder.itemView.getContext(), "start download " + messageFile.getFileName());
                    }
                });

            }
        }


    }

    @Override
    public long getHeaderId(int position) {
        return getItem(position).getDateInMillis();
    }

    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_header_chat, parent, false);
        return new VHHeader(v);
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {
        VHHeader vhHeader = (VHHeader) holder;
        vhHeader.list_item_header_chat.setText(getItem(position).getCreateAt());

    }

    @Override
    public void onClick(View v) {
        if (onItemClickListener != null) {
            if (v.getId() == R.id.list_item_ticket_type)
                onItemClickListener.onRateClick((Ticket) v.getTag());
            else
                onItemClickListener.onItemClick((Ticket) v.getTag());
        }
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {

        void onItemClick(Ticket item);

        void onRateClick(Ticket item);

    }

    class VHItem extends RecyclerView.ViewHolder {
        @Bind(R.id.list_item_chat_me)
        RelativeLayout list_item_chat_me;
        @Bind(R.id.list_item_chat_my_img)
        ImageView list_item_chat_my_img;
        @Bind(R.id.list_item_chat_my_message)
        TextViewPlus list_item_chat_my_message;
        @Bind(R.id.list_item_chat_my_message_date)
        TextViewPlus list_item_chat_my_message_date;

        @Bind(R.id.list_item_chat_interlocutor)
        RelativeLayout list_item_chat_interlocutor;
        @Bind(R.id.list_item_chat_interlocutor_img)
        ImageView list_item_chat_interlocutor_img;
        @Bind(R.id.list_item_chat_interlocutor_message)
        TextViewPlus list_item_chat_interlocutor_message;
        @Bind(R.id.list_item_chat_interlocutor_message_date)
        TextViewPlus list_item_chat_interlocutor_message_date;

        @Bind(R.id.list_item_chat_interlocutor_message_name)
        TextViewPlus list_item_chat_interlocutor_message_name;
        @Bind(R.id.list_item_chat_attachments)
        LinearLayout list_item_chat_attachments;
        @Bind(R.id.list_item_chat_attachments_text)
        View list_item_chat_attachments_text;

        public VHItem(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    class VHHeader extends RecyclerView.ViewHolder {
        @Bind(R.id.list_item_header_chat)
        TextView list_item_header_chat;
        public VHHeader(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}