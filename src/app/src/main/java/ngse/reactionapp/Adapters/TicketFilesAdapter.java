package ngse.reactionapp.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.NGSE.textviewpluslib.TextViewPlus;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import ngse.reactionapp.Data.TicketFile;
import ngse.reactionapp.Enums.FILE_TYPE;
import ngse.reactionapp.R;

public class TicketFilesAdapter extends
        RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = TicketFilesAdapter.class.getName();

    private List<TicketFile> items;
    private Context context;

    public TicketFilesAdapter(Context context, List<TicketFile> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup parent,
                                                      int viewType) {
        if (viewType == FILE_TYPE.IMAGE.getValue()) {
            return new VHImage(LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.list_item_image, parent, false));
        } else if (viewType == FILE_TYPE.VIDEO.getValue() || viewType == FILE_TYPE.DOC.getValue()) {
            return new VHFile(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_file, parent, false));
        }
        throw new RuntimeException("there is no type that matches the type "
                + viewType + " + make sure your using types correctly");
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        TicketFile ticketFile = getItem(position);
        if (holder instanceof VHImage) {
            VHImage vhImage = (VHImage) holder;
            Picasso.with(vhImage.list_item_image_img.getContext()).load(ticketFile.getFile()).into(vhImage.list_item_image_img);
        } else if (holder instanceof VHFile) {
            VHFile vhFile = (VHFile) holder;
            int typeTextId = 0;
            int typeImgId = 0;
            switch (ticketFile.getFileType())
            {
                case VIDEO:
                    typeTextId=R.string.activity_compose_ticket_video;
                    typeImgId=R.drawable.video_brown;
                    break;
                case DOC:
                    typeTextId=R.string.activity_compose_ticket_attached_file;
                    typeImgId=R.drawable.folder_brown;
                    break;
            }
            vhFile.list_item_file_type_img.setImageResource(typeImgId);
            vhFile.list_item_file_title.setText(ticketFile.getFile().getName());
            vhFile.list_item_file_type.setText(typeTextId);
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        return getItem(position).getFileType().getValue();
    }


    private TicketFile getItem(int position) {
        return items.get(position);
    }

    class VHImage extends RecyclerView.ViewHolder {

        @Bind(R.id.list_item_image_img)
        ImageView list_item_image_img;


        public VHImage(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class VHFile extends RecyclerView.ViewHolder {

        @Bind(R.id.list_item_file_type_img)
        ImageView list_item_file_type_img;
        @Bind(R.id.list_item_file_title)
        TextViewPlus list_item_file_title;
        @Bind(R.id.list_item_file_type)
        TextViewPlus list_item_file_type;

        public VHFile(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }


    }
}