package ngse.reactionapp.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.NGSE.textviewpluslib.TextViewPlus;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import ngse.reactionapp.Data.Feedback;
import ngse.reactionapp.R;

public class FeedbacksRecycleViewAdapter extends RecyclerView.Adapter<ViewHolder>  {


    private Context context;
    private List<Feedback> items = new ArrayList<>();

    public FeedbacksRecycleViewAdapter(Context context, List<Feedback> items) {
        this.context = context;
        this.items = items;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int type) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item_feedback, viewGroup, false);
        return new VHItem(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        final VHItem vhItem = (VHItem) viewHolder;
        final Feedback item = items.get(position);
        vhItem.list_item_feedback_text.setText(item.getFeedback());
        vhItem.list_item_feedback_date.setText(item.getCreatedAt());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }




    class VHItem extends ViewHolder {

        @Bind(R.id.list_item_feedback_img)
        ImageView list_item_feedback_img;
        @Bind(R.id.list_item_feedback_author)
        TextViewPlus list_item_feedback_author;
        @Bind(R.id.list_item_feedback_date)
        TextViewPlus list_item_feedback_date;
        @Bind(R.id.list_item_feedback_text)
        TextViewPlus list_item_feedback_text;
        public VHItem(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }


    }

}
