package ngse.reactionapp.Adapters;

import android.content.Context;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.NGSE.textviewpluslib.TextViewPlus;

import java.util.Comparator;

import butterknife.Bind;
import butterknife.ButterKnife;
import ngse.reactionapp.Data.Call;
import ngse.reactionapp.R;

public class CallLogsRecycleViewAdapter extends SortedListAdapter<Call> implements View.OnClickListener {

    private OnItemClickListener onItemClickListener;

    public CallLogsRecycleViewAdapter(Context context, Comparator<Call> comparator, OnItemClickListener onItemClickListener) {
        super(context, Call.class, comparator);
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    protected ViewHolder<? extends Call> onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.list_item_call_logs, parent, false);
        v.setOnClickListener(this);
        v.findViewById(R.id.list_item_call_logs_call).setOnClickListener(this);
        return new VHItem(v);
    }

    @Override
    protected boolean areItemsTheSame(Call item1, Call item2) {
        return item1.getId() == item2.getId();
    }

    @Override
    protected boolean areItemContentsTheSame(Call oldItem, Call newItem) {
        return oldItem.equals(newItem);
    }


    @Override
    public void onClick(final View v) {
        if (onItemClickListener != null)
            onItemClickListener.onItemClick(v, (Call) v.getTag());

    }

    public interface OnItemClickListener {
        void onItemClick(View view, Call callLogsItem);
    }

    class VHItem extends SortedListAdapter.ViewHolder<Call> {

        @Bind(R.id.list_item_call_logs_img)
        ImageView list_item_call_logs_img;
        @Bind(R.id.list_item_call_logs_name)
        TextViewPlus list_item_call_logs_name;
        @Bind(R.id.list_item_call_logs_company)
        TextViewPlus list_item_call_logs_company;
        @Bind(R.id.list_item_call_logs_type_call)
        TextViewPlus list_item_call_logs_type_call;
        @Bind(R.id.list_item_call_logs_call)
        ImageView list_item_call_logs_call;

        public VHItem(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

        @Override
        protected void performBind(Call item) {
            try {
                list_item_call_logs_name.setText(item.getContactName());
//            vhItem.list_item_call_logs_name.setText(item.getManager().getContact_name());
            } catch (Exception e) {
            }
            //vhItem.list_item_call_logs_img.setImageBitmap(null);

//        try {
//            Picasso.with(vhItem.list_item_call_logs_img.getContext())
//                    .load(item.getManager().getImageUrl())
//                    .into(vhItem.list_item_call_logs_img);
//        } catch (Exception e) {
//        }
            CharSequence relativeTime = DateUtils.getRelativeTimeSpanString(item.getCallDate(), System.currentTimeMillis(),
                    DateUtils.MINUTE_IN_MILLIS,
                    DateUtils.FORMAT_NO_NOON);

            list_item_call_logs_type_call.setText(item.getStatusText() + "   " + relativeTime);

            try {
                list_item_call_logs_company.setText(item.getPhone());
//            vhItem.list_item_call_logs_company.setText(item.getManager().getPhone());
            } catch (Exception e) {
            }
            if (item.getCallType() != 1) {
                list_item_call_logs_type_call.setCompoundDrawablesWithIntrinsicBounds(R.drawable.outcoming_call, 0, 0, 0);
                list_item_call_logs_type_call.setTextColor(context.getResources().getColor(R.color.gray_text_color));
            } else {
                list_item_call_logs_type_call.setCompoundDrawablesWithIntrinsicBounds(R.drawable.missed_call, 0, 0, 0);
                list_item_call_logs_type_call.setTextColor(context.getResources().getColor(R.color.error_text_color));
            }
            itemView.setTag(item);
            list_item_call_logs_call.setTag(item);
        }
    }

}
