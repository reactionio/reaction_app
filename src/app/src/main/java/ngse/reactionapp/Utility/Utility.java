package ngse.reactionapp.Utility;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import ngse.reactionapp.Data.PreferencesData;
import ngse.reactionapp.Services.CheckService;


public class Utility {
    private final static String TAG = Utility.class.getName();
    private static final String DOWNLOAD_FOLDER = "download";
    public static void showToast(Context context, int textId) {
        String text = null;
        try {
            text = context.getString(textId);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        showToast(context, text);
    }

    public static void showToast(Context context, String text) {
        try {
            Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void startCheckService(Context context)
    {
        Intent service = new Intent(context, CheckService.class);
        if (PreferencesData.getEnableNotifications(context))
            context.startService(service);
        else
            context.stopService(service);
    }
    public static String getFileName(String url) {
        String paths[] = url.split("/");
        return paths[paths.length - 1];
    }

    public static String generateString(InputStream stream) throws IOException {
        BufferedReader buffer = new BufferedReader(new InputStreamReader(stream));
        StringBuilder sb = new StringBuilder();
        String cur;

        while ((cur = buffer.readLine()) != null) {
            sb.append(cur + "\n");
        }
        buffer.close();
        return sb.toString();
    }

    public static Point getDisplaySize(Context context) {
        Point point = null;
        if (Build.VERSION.SDK_INT >= 13) {
            point = getDisplaySizeAFTER13(context);
        } else {
            point = getDisplaySizeBEFORE13(context);
        }
        Log.d(TAG, "DisplaySize x = " + point.x + " y = " + point.y);
        return point;
    }

    @SuppressWarnings("deprecation")
    private static Point getDisplaySizeBEFORE13(Context context) {
        WindowManager window = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = window.getDefaultDisplay();
        return new Point(display.getWidth(), display.getHeight());

    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private static Point getDisplaySizeAFTER13(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size;
    }

    public static void closeDialog(Dialog dialog) {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    public static String secondToTime(long seconds) {
        return String.format(Locale.getDefault(), "%02d:%02d", TimeUnit.SECONDS.toMinutes(seconds), TimeUnit.SECONDS.toSeconds(seconds) - (TimeUnit.SECONDS.toMinutes(seconds) * 60));
    }

    public static void openSite(Context context, String link) {
        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(link)));
    }

    public static String getPath(Context context, Intent imageReturnedIntent) {
        String full_path = "";

        Uri originalUri = imageReturnedIntent.getData();
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT && imageReturnedIntent != null) {
            originalUri = imageReturnedIntent.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = context.getContentResolver().query(originalUri,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            full_path = cursor.getString(columnIndex);
            cursor.close();
            Log.d("filepath", "< Build.VERSION_CODES.KITKAT     " +full_path);

        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && imageReturnedIntent != null) {
            originalUri = imageReturnedIntent.getData();

            final int takeFlags = imageReturnedIntent.getFlags()
                    & (Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            // Check for the freshest data.
            context.getContentResolver().takePersistableUriPermission(
                    originalUri, takeFlags);
            Log.d("Uri: ", originalUri.toString());

            full_path = getPath1(context, originalUri);

            Log.d("filepath", ">= Build.VERSION_CODES.KITKAT     " +full_path);

        }
       /* try {

            Uri selectedImageUri = imageReturnedIntent.getData();
            String selectedImage;
            if (selectedImageUri.getScheme().equalsIgnoreCase("content")) {
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = context.getContentResolver().query(
                        selectedImageUri, filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                selectedImage = cursor.getString(columnIndex);
                cursor.close();
            } else {
                if (selectedImageUri.getScheme().equalsIgnoreCase("file")) {
                    selectedImage = selectedImageUri.getPath();
                } else {
                    selectedImage = selectedImageUri.toString();
                }
            }
            full_path = selectedImage;
        } catch (Exception e) {
            //return "";
        }*/
        return full_path;
    }
    @SuppressLint("NewApi")
    public static String getPath1(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/"
                            + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"),
                        Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{split[1]};

                return getDataColumn(context, contentUri, selection,
                        selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }
    public static String getDataColumn(Context context, Uri uri,
                                       String selection, String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {column};

        try {
            cursor = context.getContentResolver().query(uri, projection,
                    selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }
    public static void startCall(Context context, String phone, long managerID) {
        Log.e("TAG","phone= "+ phone );
        Log.e("TAG","managerID= "+ managerID);
        if (phone==null || phone.length()==0)
            return;
        PreferencesData.setLastCallNumber(context, phone);
        PreferencesData.setLastCallManagerID(context, managerID);
        PreferencesData.setLastCallTime(context, System.currentTimeMillis());
        try {
            Intent intent = new Intent(Intent.ACTION_CALL);
            if (isPackageInstalled("com.android.phone",context))
            intent.setClassName("com.android.phone", "com.android.phone.OutgoingCallBroadcaster");
            intent.setData(Uri.parse("tel:" + phone));
            context.startActivity(intent);
        } catch (Exception e) {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + phone));
            context.startActivity(callIntent);
        }
    }
    public static boolean isPackageInstalled(String packagename, Context context) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(packagename, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }
	/*public static String getMD5(String s) {
		try {
			// Create MD5 Hash
			MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
			digest.update(s.getBytes());
			byte messageDigest[] = digest.digest();

			// Create Hex String
			StringBuffer hexString = new StringBuffer();
			for (int i=0; i<messageDigest.length; i++)
				hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
			return hexString.toString();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return "";
	}*/
    public static void sendEmail(Context context,String email,String subject,String text_send,String text_chooser)
    {
        Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
        emailIntent.setType("text/plain");

        if (email.length()>0)
            emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,new String[] {email});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        emailIntent.putExtra(Intent.EXTRA_TEXT, text_send);
        if (isPackageInstalled("com.google.android.gm",context)) {
            emailIntent.setClassName("com.google.android.gm", "com.google.android.gm.ComposeActivityGmail");
            context.startActivity(emailIntent);
        }
        else
            context.startActivity(Intent.createChooser(emailIntent,text_chooser));

    }
    public static File downloadFromDrive(Context context,Uri uri) throws IOException {

        Cursor cursor = context.getContentResolver().query(uri, new String[]{MediaStore.Files.FileColumns.DISPLAY_NAME}, null, null, null);
        cursor.moveToFirst();
        String name = cursor.getString(0);
        Log.d("TAG",name);
        InputStream is = context.getContentResolver().openInputStream(uri);
        File file = new File(context.getCacheDir(), name);// + ".pdf");
        OutputStream os = new FileOutputStream(file.getPath());
        copyStream(is, os);
        return file;
    }
    private static void copyStream(InputStream input, OutputStream output)
            throws IOException {
        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
        output.flush();
        output.close();
    }

    public static String getDownloadPath(Context context)
    {
        StringBuilder path = new StringBuilder();
        path.append(getRootFolder(context));
        path.append(DOWNLOAD_FOLDER);

        File file = new File(path + File.separator + "logNEW.txt");
        if (!file.exists())
        {
            try
            {
                file.createNewFile();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        return path.toString();
    }
    public static String getRootFolder(Context context)
    {
        String packageNameString = context != null ? context.getApplicationContext().getPackageName() : "com.NGSE.Temp";
        return getRootFolderWithoutPackage(context) + packageNameString + File.separator;
    }
    public static String getRootFolderWithoutPackage(Context context)
    {
        String state = Environment.getExternalStorageState();
        boolean isTempStorageAccessibile = Environment.MEDIA_MOUNTED.equals(state);

        if (isTempStorageAccessibile || context == null)
        {
            return Environment.getExternalStorageDirectory().getAbsolutePath() + "/Android/data/";
        }
        else
        {
            ContextWrapper cw = new ContextWrapper(context);
            File directory = cw.getDir("media", Context.MODE_PRIVATE);
            return directory.getAbsolutePath();
        }
    }

}
