package ngse.reactionapp.Utility;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import ngse.reactionapp.Data.Call;
import ngse.reactionapp.Data.Company;
import ngse.reactionapp.Data.ErrorInfo;
import ngse.reactionapp.Data.Feedback;
import ngse.reactionapp.Data.Manager;
import ngse.reactionapp.Data.Message;
import ngse.reactionapp.Data.MessageFile;
import ngse.reactionapp.Data.Notification;
import ngse.reactionapp.Data.Profile;
import ngse.reactionapp.Data.SearchResult;
import ngse.reactionapp.Data.Sender;
import ngse.reactionapp.Data.Ticket;

public class JsonParser {
    private final static String TAG = JsonParser.class.getName();

    public static void baseParse(String json, ParseResultInterface parseResultInterface) {
        Log.e(TAG, "json= " + json);
        if (parseResultInterface == null) {
            return;
        }
        try {
            Object json1 = new JSONTokener(json).nextValue();
            JSONObject jsonObject = null;
            boolean result = true;
            if (json1 instanceof JSONObject) {
                jsonObject = new JSONObject(json);
            } else if (json1 instanceof JSONArray) {
                jsonObject = new JSONObject();
                JSONArray jsonArray = new JSONArray(json);
                jsonObject.put("array", jsonArray);
            }
            ErrorInfo errorInfo = new ErrorInfo(result, -1, null);
            if (jsonObject == null || jsonObject.has("error") || jsonObject.has("errors")) {

                result = false;
                if (jsonObject != null)
                    if (jsonObject.has("error"))
                        errorInfo.setText(jsonObject.getString("error"));
                    else if (jsonObject.has("errors"))
                        errorInfo.setText(jsonObject.getString("errors"));
                    else
                        errorInfo.setText("Error executing the query");
                errorInfo.setResult(result);
            }

            parseResultInterface.error(errorInfo);
            parseResultInterface.data(jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static Profile parseProfileByEmail(JSONObject jsonObject, String email) {
        try {
            if (jsonObject.has("array")) {
                // Log.e(TAG,"jsonObject.has(\"array\")");
                JSONArray jsonArray = jsonObject.getJSONArray("array");
                for (int i = 0; i < jsonArray.length(); i++) {


                    JSONObject jsonObjectProfile = jsonArray.getJSONObject(i);
                    Profile profile = parseProfile(jsonObjectProfile);
                    if (profile != null && profile.getEmail().equals(email))
                        return profile;
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static Profile parseProfile(JSONObject jsonObject) {
        Profile profile = new Profile();
        try {
            if (jsonObject.has(JsonFields.PROFILE_FIRST_NAME))
                profile.setFirstname(jsonObject.getString(JsonFields.PROFILE_FIRST_NAME));
            if (jsonObject.has(JsonFields.PROFILE_LAST_NAME))
                profile.setLastname(jsonObject.getString(JsonFields.PROFILE_LAST_NAME));
            if (jsonObject.has(JsonFields.PROFILE_PHONE))
                profile.setPhone(jsonObject.getString(JsonFields.PROFILE_PHONE));
            else if (jsonObject.has(JsonFields.PROFILE_PHONE2))
                profile.setPhone(jsonObject.getString(JsonFields.PROFILE_PHONE2));

            if (jsonObject.has(JsonFields.PROFILE_ID))
                profile.setId(jsonObject.getLong(JsonFields.PROFILE_ID));
            else if (jsonObject.has(JsonFields.PROFILE_ID_2))
                profile.setId(jsonObject.getLong(JsonFields.PROFILE_ID_2));

            if (jsonObject.has(JsonFields.PROFILE_IMAGE_URL))
                profile.setImageUrl(jsonObject.getString(JsonFields.PROFILE_IMAGE_URL));

            if (jsonObject.has(JsonFields.PROFILE_EMAIL))
                profile.setEmail(jsonObject.getString(JsonFields.PROFILE_EMAIL));

            if (jsonObject.has(JsonFields.PROFILE_ADDRESS))
                profile.setAddress(jsonObject.getString(JsonFields.PROFILE_ADDRESS));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return profile;
    }

    public static Manager parseManager(JSONObject jsonObject) {
        Manager manager = new Manager();
        manager.setProfileData(parseProfile(jsonObject));
        try {

            if (jsonObject.has(JsonFields.MANAGER_ABOUT))
                manager.setAbout(jsonObject.getString(JsonFields.MANAGER_ABOUT));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return manager;
    }

    public static List<Call> parseCalls(JSONObject jsonObject) {
        //Log.e(TAG,"jsonObject "+jsonObject.toString());
        List<Call> calls = new ArrayList<>();
        try {
            if (jsonObject.has("array")) {
                // Log.e(TAG,"jsonObject.has(\"array\")");
                JSONArray jsonArray = jsonObject.getJSONArray("array");
                for (int i = 0; i < jsonArray.length(); i++) {

                    Call call = new Call();
                    JSONObject jsonObjectCall = jsonArray.getJSONObject(i);
                    //Log.e(TAG,"jsonObjectCall "+jsonObjectCall.toString());
                    if (jsonObjectCall.has(JsonFields.CALL_ID))
                        call.setId(jsonObjectCall.getLong(JsonFields.CALL_ID));
                    if (jsonObjectCall.has(JsonFields.CALL_USER_ID))
                        call.setUserId(jsonObjectCall.getLong(JsonFields.CALL_USER_ID));
                    if (jsonObjectCall.has(JsonFields.CALL_CLIENT_ID))
                        call.setClientId(jsonObjectCall.getLong(JsonFields.CALL_CLIENT_ID));
                    if (jsonObjectCall.has(JsonFields.CALL_STATUS_TEXT))
                        call.setStatusText(jsonObjectCall.getString(JsonFields.CALL_STATUS_TEXT));
                    if (jsonObjectCall.has(JsonFields.CALL_CREATED_AT))
                        call.setCreatedAt(jsonObjectCall.getString(JsonFields.CALL_CREATED_AT));
                    if (jsonObjectCall.has(JsonFields.MANAGER))
                        call.setManager(parseManager(jsonObjectCall.getJSONObject(JsonFields.MANAGER)));
                    Log.e(TAG, "call " + call.getStatusText());
                    calls.add(call);
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return calls;
    }

    public static List<Ticket> parseTickets(JSONObject jsonObject) {
        //Log.e(TAG,"jsonObject "+jsonObject.toString());
        List<Ticket> tickets = new ArrayList<>();
        try {
            if (jsonObject.has("array")) {
                // Log.e(TAG,"jsonObject.has(\"array\")");
                JSONArray jsonArray = jsonObject.getJSONArray("array");
                for (int i = 0; i < jsonArray.length(); i++) {

                    Ticket ticket = new Ticket();
                    JSONObject jsonObjectCall = jsonArray.getJSONObject(i);
                    //Log.e(TAG,"jsonObjectCall "+jsonObjectCall.toString());
                    if (jsonObjectCall.has(JsonFields.TICKET_ID))
                        ticket.setId(jsonObjectCall.getLong(JsonFields.TICKET_ID));
                    if (jsonObjectCall.has(JsonFields.TICKET_USER_ID))
                        ticket.setUserId(jsonObjectCall.getLong(JsonFields.TICKET_USER_ID));
                    if (jsonObjectCall.has(JsonFields.TICKET_CLIENT_ID))
                        ticket.setClientId(jsonObjectCall.getLong(JsonFields.TICKET_CLIENT_ID));
                    if (jsonObjectCall.has(JsonFields.TICKET_TITLE))
                        ticket.setTitle(jsonObjectCall.getString(JsonFields.TICKET_TITLE));
                    if (jsonObjectCall.has(JsonFields.TICKET_DESCRIPTION))
                        ticket.setDescription(jsonObjectCall.getString(JsonFields.TICKET_DESCRIPTION));
                    if (jsonObjectCall.has(JsonFields.TICKET_STATUS_TEXT))
                        ticket.setStatusText(jsonObjectCall.getString(JsonFields.TICKET_STATUS_TEXT));
                    if (jsonObjectCall.has(JsonFields.TICKET_CREATED_AT))
                        ticket.setCreatedAt(jsonObjectCall.getString(JsonFields.TICKET_CREATED_AT));
                    if (jsonObjectCall.has(JsonFields.TICKET_UPDATED_AT))
                        ticket.setUpdatedAt(jsonObjectCall.getString(JsonFields.TICKET_UPDATED_AT));

                    if (jsonObjectCall.has(JsonFields.MANAGER)) {
                        try {
                            JSONObject jsonObjectManager = jsonObjectCall.getJSONObject(JsonFields.MANAGER);
                            ticket.setManager(parseManager(jsonObjectManager));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if (jsonObjectCall.has(JsonFields.FEEDBACKS)) {
                        JSONArray jsonArrayFeedbacks = jsonObjectCall.getJSONArray(JsonFields.FEEDBACKS);
                        List<Feedback> feedbacks = new ArrayList<>();
                        for (int j = 0; j< jsonArrayFeedbacks.length(); j++) {
                            //feedbacks.add(parseFeedback(jsonArrayFeedbacks.getJSONObject(j)));
                           // Log.i("TAG", "feedback= " + feedbacks.get(j).getFeedback());
                            ticket.setFeedback(parseFeedback(jsonArrayFeedbacks.getJSONObject(j)));
                            Log.i("TAG", "feedback= " + ticket.getFeedback().getFeedback());
                            break;
                        }

                    }
                    if (jsonObjectCall.has("images")) {
                        JSONArray jsonArrayFeedbacks = jsonObjectCall.getJSONArray("images");
                        ArrayList<String> images = new ArrayList<>();
                        for (int j = 0; j< jsonArrayFeedbacks.length(); j++) {
                            //feedbacks.add(parseFeedback(jsonArrayFeedbacks.getJSONObject(j)));
                            // Log.i("TAG", "feedback= " + feedbacks.get(j).getFeedback());
                            images.add(parseImage(jsonArrayFeedbacks.getJSONObject(j)));
                        }
                        ticket.setImages(images);

                    }
                    /*if (jsonObjectCall.has(JsonFields.MANAGER)) {
                        try {
                            JSONObject jsonObjectManager = jsonObjectCall.getJSONObject(JsonFields.MANAGER);
                            ticket.setManager(parseManager(jsonObjectManager));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }*/
                    //Log.e(TAG,"call "+call.getStatusText());
                    tickets.add(ticket);
                }

            }
        } catch (JSONException e) {
            Log.i("TAG", "JSONException= " +e.getMessage());
            e.printStackTrace();
        }

        return tickets;
    }

    public static List<Notification> parseNotifications(JSONObject jsonObject) {
        List<Notification> notifications = new ArrayList<>();
        try {
            if (jsonObject.has("array")) {
                JSONArray jsonArray = jsonObject.getJSONArray("array");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObjectCNotification = jsonArray.getJSONObject(i);
                    notifications.add(parseNotification(jsonObjectCNotification));
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return notifications;
    }

    public static List<Company> parseCompanies(JSONObject jsonObject) {
        List<Company> companies = new ArrayList<>();
        try {
            if (jsonObject.has("array")) {
                JSONArray jsonArray = jsonObject.getJSONArray("array");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObjectCompany = jsonArray.getJSONObject(i);
                    companies.add(parseCompany(jsonObjectCompany));
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return companies;
    }

    public static Company parseCompany(JSONObject jsonObject) {
        Company company = new Company();
        try {
            if (jsonObject.has("array")) {
                JSONArray jsonArray = jsonObject.getJSONArray("array");
                if (jsonArray.length() != 0)
                    jsonObject = jsonArray.getJSONObject(0);
                else
                    return null;
            }
            if (jsonObject.has(JsonFields.COMPANY_ID))
                company.setId(jsonObject.getLong(JsonFields.COMPANY_ID));
            if (jsonObject.has(JsonFields.COMPANY_USER_ID))
                company.setUserId(jsonObject.getLong(JsonFields.COMPANY_USER_ID));
            if (jsonObject.has(JsonFields.COMPANY_NAME))
                company.setName(jsonObject.getString(JsonFields.COMPANY_NAME));
            if (jsonObject.has(JsonFields.COMPANY_EMAIL))
                company.setEmail(jsonObject.getString(JsonFields.COMPANY_EMAIL));
            if (jsonObject.has(JsonFields.COMPANY_DESCRIPTION))
                company.setDescription(jsonObject.getString(JsonFields.COMPANY_DESCRIPTION));
            if (jsonObject.has(JsonFields.COMPANY_IMAGE_URL))
                company.setImageUrl(jsonObject.getString(JsonFields.COMPANY_IMAGE_URL));
            if (jsonObject.has(JsonFields.COMPANY_CREATED_AT))
                company.setCreatedAt(jsonObject.getString(JsonFields.COMPANY_CREATED_AT));
            if (jsonObject.has(JsonFields.FEEDBACKS)) {
                JSONArray jsonArray = jsonObject.getJSONArray(JsonFields.FEEDBACKS);
                List<Feedback> feedbacks = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {

                    feedbacks.add(parseFeedback(jsonArray.getJSONObject(i)));
                    Log.e(TAG, "feedback= " + feedbacks.get(i).getFeedback());
                }

                company.setFeedbacks(feedbacks);
            }
            if (jsonObject.has(JsonFields.MANAGER)) {
                JSONArray jsonArray = jsonObject.getJSONArray(JsonFields.MANAGER);
                List<Manager> managers = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {

                    managers.add(parseManager(jsonArray.getJSONObject(i)));
                    Log.e("TAG", "manager= " + managers.get(i).getFirstname() + " id " + managers.get(i).getId() + " phone " + managers.get(i).getPhone());
                }
                company.setManagers(managers);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return company;
    }
    public static String parseImage(JSONObject jsonObject) {
       // Image image = new Image();
        String url = null;
        try {
          /*  if (jsonObject.has("id"))
                image.setId(jsonObject.getLong("id"));
            if (jsonObject.has("imageUrl"))
                image.setUrl(jsonObject.getString("imageUrl"));
            if (jsonObject.has("caption"))
                image.setName("caption");*/
            if (jsonObject.has("imageUrl"))
                url=jsonObject.getString("imageUrl");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return url;
    }
    public static Feedback parseFeedback(JSONObject jsonObject) {
        Feedback feedback = new Feedback();
        try {
            if (jsonObject.has(JsonFields.FEEDBACK_ID))
                feedback.setId(jsonObject.getLong(JsonFields.FEEDBACK_ID));
            if (jsonObject.has(JsonFields.FEEDBACK_FEEDBACK))
                feedback.setFeedback(jsonObject.getString(JsonFields.FEEDBACK_FEEDBACK));
            if (jsonObject.has(JsonFields.FEEDBACK_MARK))
                feedback.setMark(jsonObject.getInt(JsonFields.FEEDBACK_MARK));
            if (jsonObject.has(JsonFields.FEEDBACK_MARK_TEXT))
                feedback.setMarkText(jsonObject.getString(JsonFields.FEEDBACK_MARK_TEXT));
            if (jsonObject.has(JsonFields.FEEDBACK_CREATED_AT))
                feedback.setCreatedAt(jsonObject.getString(JsonFields.FEEDBACK_CREATED_AT));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return feedback;
    }

    public static Notification parseNotification(JSONObject jsonObject) {
        Notification notification = new Notification();
        try {

            if (jsonObject.has(JsonFields.NOTIFICATION_BODY))
                notification.setNotification(jsonObject.getString(JsonFields.NOTIFICATION_BODY));
            if (jsonObject.has(JsonFields.NOTIFICATION_CREATED))
                notification.setCreated(jsonObject.getString(JsonFields.NOTIFICATION_CREATED));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return notification;
    }

    public static List<Message> parseMessages(JSONObject jsonObject) {
        //Log.e("TAG","data= "+jsonObject);
        List<Message> messages = new ArrayList<>();
        try {
            if (jsonObject.has(JsonFields.ITEMS)) {
                JSONArray jsonArray = jsonObject.getJSONArray(JsonFields.ITEMS);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObjectSearchResult = jsonArray.getJSONObject(i);
                    messages.add(parseMessage(jsonObjectSearchResult));
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return messages;
    }

    public static Message parseMessage(JSONObject jsonObject) {
        Message message = new Message();
        try {
            if (jsonObject.has(JsonFields.MESSAGE_SENDER))
                message.setSender(parseSender(jsonObject.getJSONObject(JsonFields.MESSAGE_SENDER)));
            if (jsonObject.has(JsonFields.MESSAGE_BODY))
                message.setMessageBody(jsonObject.getString(JsonFields.MESSAGE_BODY));
            if (jsonObject.has(JsonFields.MESSAGE_CREATED_AT))
                message.setCreateAt(jsonObject.getString(JsonFields.MESSAGE_CREATED_AT));
            if (jsonObject.has(JsonFields.MESSAGE_FILE_LIST)) {
                try {
                    JSONObject jsonObjectFileList = jsonObject.getJSONObject(JsonFields.MESSAGE_FILE_LIST);
                    Iterator<String> iter = jsonObjectFileList.keys();
                    while (iter.hasNext()) {
                        String key = iter.next();
                        String value = jsonObjectFileList.getString(key);
                        MessageFile messageFile = new MessageFile();
                        messageFile.setName(key);
                        messageFile.setUrl(value);
                        Log.i("TAG", "key " + key);
                        Log.i("TAG", "value " + value);
                        message.addMessageFiles(messageFile);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    //Log.i("TAG", "error " + e.getMessage());
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
            //Log.i("TAG", "error " + e.getMessage());
            return null;
        }

        return message;
    }

    public static Message parseMessageSSE(JSONObject jsonObject) {
        try {
            if (jsonObject.has(JsonFields.MESSAGE))
                jsonObject = jsonObject.getJSONObject(JsonFields.MESSAGE);
            return parseMessage(jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Sender parseSender(JSONObject jsonObject) {
        Sender sender = new Sender();
        try {
            if (jsonObject.has(JsonFields.SENDER_ID))
                sender.setId(jsonObject.getLong(JsonFields.SENDER_ID));
            if (jsonObject.has(JsonFields.SENDER_CLASS))
                sender.setSenderClass(jsonObject.getString(JsonFields.SENDER_CLASS));
            if (jsonObject.has(JsonFields.SENDER_IMAGE))
                sender.setImage(jsonObject.getString(JsonFields.SENDER_IMAGE));
            if (jsonObject.has(JsonFields.SENDER_NAME))
                sender.setName(jsonObject.getString(JsonFields.SENDER_NAME));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return sender;
    }

    public static List<SearchResult> parseSearchResults(JSONObject jsonObject) {
        //Log.e("TAG","data= "+jsonObject);
        List<SearchResult> searchResults = new ArrayList<>();
        try {
            if (jsonObject.has(JsonFields.ITEMS)) {
                JSONArray jsonArray = jsonObject.getJSONArray(JsonFields.ITEMS);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObjectSearchResult = jsonArray.getJSONObject(i);
                    searchResults.add(parseSearchResult(jsonObjectSearchResult));
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return searchResults;
    }

    public static SearchResult parseSearchResult(JSONObject jsonObject) {
        SearchResult searchResult = new SearchResult();
        try {
            if (jsonObject.has(JsonFields.SEARCH_ID))
                searchResult.setId(jsonObject.getLong(JsonFields.SEARCH_ID));
            if (jsonObject.has(JsonFields.SEARCH_TITLE))
                searchResult.setTitle(jsonObject.getString(JsonFields.SEARCH_TITLE));
            if (jsonObject.has(JsonFields.SEARCH_DESCRIPTION))
                searchResult.setDescription(jsonObject.getString(JsonFields.SEARCH_DESCRIPTION));
            if (jsonObject.has(JsonFields.SEARCH_TYPE))
                searchResult.setType(jsonObject.getString(JsonFields.SEARCH_TYPE));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return searchResult;
    }

    ////////////////////////////
    public static String parseSingleItem(String json, String param) {
        if (json == null)
            return "'";
        try {
            JSONObject jsonObject = new JSONObject(json);
            if (jsonObject.has(param)) {
                return jsonObject.getString(param);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static int parseIntItem(String json, String param) {
        try {
            JSONObject jsonObject = new JSONObject(json);
            if (jsonObject.has(param)) {
                return jsonObject.getInt(param);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public static boolean parseBooleanItem(String json, String param) {
        try {
            JSONObject jsonObject = new JSONObject(json);
            if (jsonObject.has(param)) {
                return jsonObject.getBoolean(param);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

    public interface ParseResultInterface {
        void error(ErrorInfo errorInfo);

        void data(JSONObject data);
    }


}
