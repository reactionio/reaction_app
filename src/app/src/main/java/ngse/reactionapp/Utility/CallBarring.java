package ngse.reactionapp.Utility;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.TextView;

import com.android.internal.telephony.ITelephony;

import java.lang.reflect.Method;

import ngse.reactionapp.R;

// Extend the class from BroadcastReceiver to listen when there is a incoming call
public class CallBarring extends BroadcastReceiver {
    // This String will hold the incoming phone number

    TelephonyManager telephonyManager;
    PhoneStateListener listener;
    Context context;
    private String number;
    private View widgetView;

    @Override
    public void onReceive(final Context context, final Intent intent) {
        // If, the received action is not a type of "Phone_State", ignore it

        if (!intent.getAction().equals("android.intent.action.PHONE_STATE"))
            return;


            // Else, try to do some action
      /*  else {

            this.context = context;
            // Fetch the number of incoming call
            try{
                String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
                Log.e("TAG -----------------", "state  "+state);

            }
            catch(Exception e){e.printStackTrace();}

            telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

            listener = new PhoneStateListener() {
                @Override
                public void onCallStateChanged(int state, String incomingNumber) {
                    switch (state) {
                        case TelephonyManager.CALL_STATE_IDLE:
                            Log.e("TAG", "CALL_STATE_IDLE");
                            hide();
                            //dialog.show();
                            break;
                        case TelephonyManager.CALL_STATE_OFFHOOK:
                            Log.e("TAG", "CALL_STATE_OFFHOOK");

                            String newNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
                            Log.e("TAG", "newNumber "+newNumber);
                            Log.e("TAG", "number "+number);
                            if (newNumber == null )
                                break;
                            if (number!=null &&number.equals(newNumber))
                                break;
                            //initWidget(newNumber);

                            break;
                        case TelephonyManager.CALL_STATE_RINGING:
                            Log.e("TAG", "CALL_STATE_RINGING");
                            // dialog.show();
                            break;
                    }
                    //Toast.makeText(context, stateString,Toast.LENGTH_LONG).show();
                }
            };

            // Register the listener with the telephony manager

            telephonyManager.listen(listener, PhoneStateListener.LISTEN_CALL_STATE);
            Log.e("TAG", " telephonyManager.listen");

        }*/
    }

    // Method to disconnect phone automatically and programmatically
    // Keep this method as it is
    @SuppressWarnings({"rawtypes", "unchecked"})
    private void disconnectPhoneItelephony(Context context) {
        ITelephony telephonyService;
        TelephonyManager telephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        try {
            Class c = Class.forName(telephony.getClass().getName());
            Method m = c.getDeclaredMethod("getITelephony");
            m.setAccessible(true);
            telephonyService = (ITelephony) m.invoke(telephony);
            telephonyService.endCall();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initWidget(String number) {
            widgetView = LayoutInflater.from(context).inflate(R.layout.activity_call, null);

            widgetView.findViewById(R.id.activity_call_end).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    disconnectPhoneItelephony(context);
                }
            });
            TextView activity_call_number = (TextView) widgetView.findViewById(R.id.activity_call_number);
            activity_call_number.setText(context.getString(R.string.activity_call_number, number));
            show();

	/*	final ViewGroup parent=(ViewGroup)widgetView.getParent();
		if(parent!=null)
			parent.removeView(widgetView);*/

    }

    private void show() {
        Log.e("TAG", "show widget");
        if (widgetView == null)
            return;

        final WindowManager.LayoutParams param = new WindowManager.LayoutParams();
        param.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
        param.format = PixelFormat.RGBA_8888;
        param.type = WindowManager.LayoutParams.TYPE_SYSTEM_ALERT;
        param.gravity = Gravity.TOP | Gravity.LEFT;

        Point screenSize = Utility.getDisplaySize(context);
        param.height = screenSize.y / 2;

        final WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);

        windowManager.addView(widgetView, param);
        widgetView.setOnTouchListener(new View.OnTouchListener() {
            private int initialX;
            private int initialY;
            private float initialTouchX;
            private float initialTouchY;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        initialX = param.x;
                        initialY = param.y;
                        initialTouchX = event.getRawX();
                        initialTouchY = event.getRawY();
                        return true;
                    case MotionEvent.ACTION_UP:
                        return true;
                    case MotionEvent.ACTION_MOVE:
                        param.x = initialX + (int) (event.getRawX() - initialTouchX);
                        param.y = initialY + (int) (event.getRawY() - initialTouchY);
                        windowManager.updateViewLayout(widgetView, param);

                        return true;
                }
                return false;
            }
        });
    }

    private void hide() {
        Log.e("TAG", "hide widget");
        number = null;
/*		if (widgetView==null)
			return;*/
        final WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        try {
            windowManager.removeView(widgetView);
        } catch (Exception e) {
            e.printStackTrace();
        }
       // widgetView=null;
    }


}
