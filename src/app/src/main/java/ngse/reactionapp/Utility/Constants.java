package ngse.reactionapp.Utility;





public class Constants
{

	public static final String GOOGLE_PLAY="https://play.google.com/store/apps/details?id=";

	public static final String ROOT_API="http://reactionapp.artwebit.ru";
    public static final String QUERY="?query=";
	public static final String WITH_MANAGER="?with=manager";//&order=t.id DESC
	public static final String DESC="&order=t.id+DESC";
    public static final String WITH_FEEDBACKS ="?with=feedbacks";
	public static final String WITH_FEEDBACKS_AND_MANAGER = WITH_FEEDBACKS +",manager,images,files,videos";//",manager,images,files,videos";
	public static final String FILTER="&filter=";
	//-------------------------------------------------------------------
	public static final String API =ROOT_API+"/api";
	public static final String CHAT_BY_TICKET =API+"/api/chat/index?ticketId=";
    public static final String ROOT_NOTIFY =API+"/notify";
    public static final String ROOT_SEARCH =API+"/search";
	public static final String ROOT_TICKET =API+"/ticket";
	public static final String ROOT_COMPANY =API+"/company";
	public static final String ROOT_CALL =API+"/call";
	public static final String COMPANY_WITH_FEEDBACKS =ROOT_COMPANY+ WITH_FEEDBACKS +FILTER;
	public static final String COMPANIES_WITH_MANAGERS =ROOT_COMPANY+WITH_MANAGER;
	//public static final String TICKET =ROOT_TICKET+ WITH_FEEDBACKS_AND_MANAGER;
	public static final String TICKET =ROOT_TICKET+ WITH_FEEDBACKS_AND_MANAGER;
	public static final String CLIENT =API+"/client";
	public static final String CALL =ROOT_CALL+WITH_MANAGER;
	public static final String FEEDBACK =API+"/feedback";
    public static final String SEARCH =ROOT_SEARCH+QUERY;

	public static final String STATUS_CLOSED = "Closed";
	public static final String STATUS_PROCESS = "Process";
	public static final String STATUS_RESOLVED = "Resolved";

}