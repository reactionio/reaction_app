package ngse.reactionapp.Utility;

public class JsonFields
{
	private final static String TAG = JsonFields.class.getName();

    public static final String ITEMS= "items";

	public static final String PROFILE_FIRST_NAME = "firstname";
	public static final String PROFILE_LAST_NAME = "lastname";
	public static final String PROFILE_EMAIL = "email";
	public static final String PROFILE_PHONE = "phone";
 public static final String PROFILE_PHONE2 = "phone_number";
	public static final String PROFILE_ADDRESS = "address";
	public static final String PROFILE_ID = "id";
    public static final String PROFILE_ID_2 = "user_id";
    public static final String PROFILE_IMAGE_URL = "imageUrl";

    public static final String CALL_ID = "id";
    public static final String CALL_USER_ID = "user_id";
    public static final String CALL_CLIENT_ID = "client_id";
    public static final String CALL_STATUS_TEXT = "statusText";
    public static final String CALL_CREATED_AT = "created_at";

   /* public static final String MANAGER_ID = "id";
    public static final String MANAGER_EMAIL = "email";
    public static final String MANAGER_FIRST_NAME = "firstname";
    public static final String MANAGER_LAST_NAME = "lastname";
    public static final String MANAGER_PHONE = "phone";*/
    public static final String MANAGER= "manager";
   // public static final String MANAGER_IMAGE_URL = "imageUrl";
    public static final String MANAGER_ABOUT = "about";


    public static final String TICKET_ID = "id";
    public static final String TICKET_USER_ID = "user_id";
    public static final String TICKET_CLIENT_ID = "client_id";
    public static final String TICKET_TITLE = "title";
    public static final String TICKET_DESCRIPTION = "description";
    public static final String TICKET_STATUS_TEXT = "statusText";
    public static final String TICKET_CREATED_AT = "created_at";
    public static final String TICKET_UPDATED_AT = "updated_at";

    public static final String FEEDBACKS= "feedbacks";
    public static final String FEEDBACK_ID = "id";
    public static final String FEEDBACK_FEEDBACK = "feedback";
    public static final String FEEDBACK_MARK = "mark";
    public static final String FEEDBACK_MARK_TEXT = "markText";
    public static final String FEEDBACK_CREATED_AT = "created_at";

    public static final String COMPANY_ID = "id";
    public static final String COMPANY_USER_ID = "user_id";
    public static final String COMPANY_NAME = "name";
    public static final String COMPANY_EMAIL = "email";
    public static final String COMPANY_DESCRIPTION = "description";
    public static final String COMPANY_IMAGE_URL = "imageUrl";
    public static final String COMPANY_CREATED_AT = "created_at";

    public static final String SEARCH_ID = "id";
    public static final String SEARCH_TITLE = "title";
    public static final String SEARCH_DESCRIPTION = "description";
    public static final String SEARCH_TYPE = "type";

    public static final String NOTIFICATION_BODY = "notification";
    public static final String NOTIFICATION_CREATED = "created";

    public static final String MESSAGE = "message";
    public static final String MESSAGE_SENDER = "sender";
    public static final String MESSAGE_BODY = "message";
    public static final String MESSAGE_CREATED_AT = "created_at";
    public static final String MESSAGE_FILE_LIST= "fileList";

    public static final String SENDER_ID = "id";
    public static final String SENDER_CLASS = "class";
    public static final String SENDER_IMAGE = "image";
    public static final String SENDER_NAME = "name";





}
