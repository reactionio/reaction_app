package ngse.reactionapp.Utility;

import android.app.Dialog;
import android.content.Context;

import com.NGSE.RequestLibrary.RequestHandler;
import com.NGSE.RequestLibrary.RequestInterface;

import org.apache.http.Header;
import org.json.JSONObject;

import ngse.reactionapp.Data.ErrorInfo;


public class CustomRequestHandler extends RequestHandler {

    private Context context;
    private boolean showError;
    protected ErrorInfo errorInfo;
    private Dialog dialog;
    protected JSONObject data;

    public CustomRequestHandler(Context context, boolean showError) {
        this.context = context;
        this.showError = showError;
    }

    @Override
    public void processingRequest(RequestInterface requestInterface, Object response, int code, int type, Header[] headers) {
        JsonParser.baseParse((String) response, new JsonParser.ParseResultInterface() {
            @Override
            public void error(ErrorInfo errorInfo) {
                CustomRequestHandler.this.errorInfo = errorInfo;
            }

            @Override
            public void data(JSONObject data) {
                CustomRequestHandler.this.data = data;
            }


        });
        if (errorInfo != null) {
            errorInfo.setStatus(code);
        } else {
            errorInfo = new ErrorInfo(false, -1,"");
        }
        if (!errorInfo.isResult()) {
            if (showError) {
                dialog = DialogUtility.getRequestErrorAlertDialog(context, errorInfo);
                dialog.show();
            }
        }
    }
}
