package ngse.reactionapp.Utility;


import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.TextView;

import ngse.Utility.ProgressBarCircular;
import ngse.reactionapp.Data.ErrorInfo;
import ngse.reactionapp.R;


public class DialogUtility
{

	public interface OnDialogButtonClickListener {
		void onPositiveClick();
		void onNegativeClick();
	}

	public static Dialog getRequestErrorAlertDialog(Context context, ErrorInfo errorInfo) {
		if (errorInfo.getText()==null)
			errorInfo.setText(context.getString(R.string.error_001));
		return new AlertDialog.Builder(context)//
				.setTitle(R.string.error)//
				.setMessage(errorInfo.getText())
						.setCancelable(false)
						.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.dismiss();
							}
						}).create();
	}
	public static Dialog getAlertDialog(Context context, View view) {
		return new AlertDialog.Builder(context)//
				.setCancelable(true)
				.setView(view)
				.create();
	}
	public static Dialog getAlertDialog(Context context, String text, final OnDialogButtonClickListener onDialogButtonClickListener) {
		return new AlertDialog.Builder(context)//
				.setTitle(R.string.attention)//
				.setMessage(text)
				.setCancelable(false)
				.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (onDialogButtonClickListener != null)
							onDialogButtonClickListener.onPositiveClick();
						dialog.dismiss();
					}
				}).create();
	}
	public static Dialog getCheckBoxDialog(Context context, String text, final OnDialogButtonClickListener onDialogButtonClickListener) {
		CheckBox checkBox=new CheckBox(context);
		checkBox.setText(text);
		return new AlertDialog.Builder(context)//
				.setTitle(R.string.attention)//
				.setView(checkBox)
				.setCancelable(true)
				.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (onDialogButtonClickListener != null)
							onDialogButtonClickListener.onPositiveClick();
						dialog.dismiss();
					}
				}).create();
	}
	public static Dialog getAlertDialogTwoButtons(Context context, String text, final OnDialogButtonClickListener onDialogButtonClickListener) {
		return new AlertDialog.Builder(context)//
				.setTitle(R.string.attention)//
				.setMessage(text)
				.setCancelable(false)
				.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (onDialogButtonClickListener != null)
							onDialogButtonClickListener.onPositiveClick();
						dialog.dismiss();
					}
				})
				.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (onDialogButtonClickListener != null)
							onDialogButtonClickListener.onNegativeClick();
						dialog.dismiss();
					}
				}).create();
	}
	public static Dialog getAlertDialogMedia(Context context, String text, final OnDialogButtonClickListener onDialogButtonClickListener) {
		return new AlertDialog.Builder(context)//
				.setTitle(R.string.attention)//
				.setMessage(text)
				.setCancelable(true)
				.setPositiveButton(R.string.fromGallery, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (onDialogButtonClickListener!=null)
							onDialogButtonClickListener.onPositiveClick();
						dialog.dismiss();
					}
				})
				.setNegativeButton(R.string.fromCamera, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (onDialogButtonClickListener!=null)
							onDialogButtonClickListener.onNegativeClick();
						dialog.dismiss();
					}
				}).create();
	}
	public static Dialog getWaitDialog(Context context, String text, boolean isShow)
	{
		return getWaitDialog(context, text, isShow, false);
	}

	public static Dialog getWaitDialog(Context context, String text, boolean isShow, boolean cancelable)
	{
		Dialog dialog = new Dialog(context, R.style.Dialog);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_wait);
		dialog.setCancelable(cancelable);
		ProgressBarCircular progressBarCircular = (ProgressBarCircular) dialog.findViewById(R.id.progressBarCircular);
		progressBarCircular.setBackgroundColor(context.getResources().getColor(R.color.color_primary_dark));

		TextView textTextView = (TextView) dialog.findViewById(R.id.textTextView);
		if (text != null && text.length() != 0)
		{
			textTextView.setVisibility(View.VISIBLE);
			textTextView.setText(text);
		}
		else
		{
			textTextView.setVisibility(View.GONE);
		}
		try
		{
			if (isShow)
			{
				try
				{
					dialog.show();
				}
				catch (Exception e)
				{

				}

			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}

		return dialog;
	}


}
