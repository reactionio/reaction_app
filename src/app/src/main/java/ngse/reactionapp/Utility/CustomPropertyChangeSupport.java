package ngse.reactionapp.Utility;

import android.content.Context;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import ngse.reactionapp.Data.Profile;

public class CustomPropertyChangeSupport {
    private static final String TAG = CustomPropertyChangeSupport.class.getName();
    private PropertyChangeSupport propertyChangeSupport;
    private Profile profile;


    public CustomPropertyChangeSupport(final Context context) {
        this.propertyChangeSupport = new PropertyChangeSupport(context);
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }

    public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(propertyName, listener);
    }

    public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(propertyName, listener);
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        if (profile == null) {
            this.profile = null;
            return;
        }
        if (this.profile != null) {
            this.profile.update(profile);
        } else
            this.profile = profile;

        fireProfileChange();
    }

    public void fireProfileChange() {
        propertyChangeSupport.firePropertyChange(Profile.class.getName(), true, profile);
    }


}
