package ngse.reactionapp.Fragments;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ngse.reactionapp.Data.Ticket;
import ngse.reactionapp.R;


public class TicketPageFragment extends BaseFragment {

	public static final String TAG = TicketPageFragment.class.getName();
	protected FragmentManager currentFragmentManager;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		currentFragmentManager = getChildFragmentManager();
	}
	@Override
	public void onResume() {
		super.onResume();
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_ticket_page, container, false);
	}

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
		showMainPageListFragment();


    }

	public synchronized void showMainPageListFragment() {
		FragmentManager fragmentManager = getChildFragmentManager();
		if (fragmentManager.findFragmentByTag(TicketsFragment.TAG) != null)
			return;
		FragmentTransaction ft = getChildFragmentManager().beginTransaction();
		ft.replace(R.id.main_page_fragment_frame_layout, new TicketsFragment(), TicketsFragment.TAG);
		ft.commitAllowingStateLoss();
	}
	public synchronized void showTicketRatingFragment(Ticket ticket) {
		FragmentManager fragmentManager = getChildFragmentManager();
		if (fragmentManager.findFragmentByTag(TicketRatingFragment.TAG) != null)
			return;
		FragmentTransaction ft = getChildFragmentManager().beginTransaction();
		ft.addToBackStack(TicketRatingFragment.TAG);
		ft.add(R.id.main_page_fragment_frame_layout, TicketRatingFragment.newInstance(ticket), TicketRatingFragment.TAG);
		ft.commitAllowingStateLoss();
	}
	public synchronized void updateTickets() {
		FragmentManager fragmentManager = getChildFragmentManager();
		TicketsFragment ticketsFragment= (TicketsFragment) fragmentManager.findFragmentByTag(TicketsFragment.TAG);
		if (ticketsFragment != null)
		{
			ticketsFragment.loadData();
		}
	}
	public boolean onTabBackPressed()
	{

		currentFragmentManager = getChildFragmentManager();
		if (currentFragmentManager.getBackStackEntryCount() > 0)
		{
			currentFragmentManager.popBackStack();
			return true;
		}
		return false;
	}

}
