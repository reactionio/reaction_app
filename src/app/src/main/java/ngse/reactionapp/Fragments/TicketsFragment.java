package ngse.reactionapp.Fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.NGSE.RequestLibrary.Query;
import com.NGSE.RequestLibrary.RequestInterface;
import com.NGSE.RequestLibrary.RequestParameters;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersDecoration;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersTouchListener;

import org.apache.http.Header;
import org.apache.http.message.BasicHeader;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import ngse.reactionapp.Activities.ChatActivity;
import ngse.reactionapp.Activities.ComposeTicketActivity;
import ngse.reactionapp.Adapters.TicketsStickyHeadersAdapter;
import ngse.reactionapp.Data.PreferencesData;
import ngse.reactionapp.Data.Ticket;
import ngse.reactionapp.Enums.TICKET_STATE;
import ngse.reactionapp.R;
import ngse.reactionapp.Utility.Constants;
import ngse.reactionapp.Utility.CustomRequestHandler;
import ngse.reactionapp.Utility.DialogUtility;
import ngse.reactionapp.Utility.JsonParser;
import ngse.reactionapp.Widgets.EmptyRecyclerView;


public class TicketsFragment extends BaseFragment {

	public static final String TAG = TicketsFragment.class.getName();

	@Bind(R.id.fragment_tickets_swipe_refresh_layout)
	SwipeRefreshLayout fragment_tickets_swipe_refresh_layout;
	@Bind(R.id.fragment_tickets_recycler_view)
    EmptyRecyclerView fragment_tickets_recycler_view;
    @Bind(R.id.empty_view_body)
    View empty_view_body;

	TicketsStickyHeadersAdapter ticketsStickyHeadersAdapter = new TicketsStickyHeadersAdapter();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	@Override
	public void onResume() {
		super.onResume();
        loadData();
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_tickets, container, false);
	}

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fragment_tickets_recycler_view.setLayoutManager(new LinearLayoutManager(getActivity()));
		fragment_tickets_swipe_refresh_layout.setColorSchemeColors(getResources().getIntArray(R.array.main_refresh_scheme));


        fragment_tickets_recycler_view.setAdapter(ticketsStickyHeadersAdapter);
        final StickyRecyclerHeadersDecoration headersDecor = new StickyRecyclerHeadersDecoration(ticketsStickyHeadersAdapter);
        fragment_tickets_recycler_view.addItemDecoration(headersDecor);
        fragment_tickets_recycler_view.setEmptyView(empty_view_body);
        fragment_tickets_swipe_refresh_layout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData();
            }
        });

        fragment_tickets_swipe_refresh_layout.post(new Runnable() {
            @Override
            public void run() {
                loadDataWithRefreshing();
            }
        });
        // Add touch listeners
        StickyRecyclerHeadersTouchListener touchListener =
                new StickyRecyclerHeadersTouchListener(fragment_tickets_recycler_view, headersDecor);
        touchListener.setOnHeaderClickListener(
                new StickyRecyclerHeadersTouchListener.OnHeaderClickListener() {
                    @Override
                    public void onHeaderClick(View header, int position, long headerId) {
                      //  Toast.makeText(activity, "Header position: " + position + ", id: " + headerId, Toast.LENGTH_SHORT).show();
                    }
                });
        fragment_tickets_recycler_view.addOnItemTouchListener(touchListener);
        ticketsStickyHeadersAdapter.setOnItemClickListener(new TicketsStickyHeadersAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Ticket item) {
                switch (item.getTICKET_state())
                {
                    case CLOSED:
                    case PROCESS:
                    case RESOLVED:
                        ChatActivity.newInstance(activity, item);
                        break;
                    case NONE:
                        break;
                }

            }

            @Override
            public void onRateClick(final Ticket item) {
                switch (item.getTICKET_state())
                {
                    case CLOSED:
                        break;
                    case PROCESS:
                        break;
                    case RESOLVED:
                        if (item.getFeedback()==null) {
                            Dialog dialog = DialogUtility.getAlertDialog(activity, getString(R.string.close_ticket_message), new DialogUtility.OnDialogButtonClickListener() {
                                @Override
                                public void onPositiveClick() {
                                    TicketPageFragment ticketPageFragment = (TicketPageFragment) getParentFragment();
                                    if (ticketPageFragment != null)
                                        ticketPageFragment.showTicketRatingFragment(item);
                                }

                                @Override
                                public void onNegativeClick() {

                                }
                            });
                            dialog.show();
                        }
                       /* */
                        break;
                    case NONE:
                        break;
                }
            }

        });

        ticketsStickyHeadersAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                headersDecor.invalidateHeaders();
            }
        });
    }


	@OnClick(R.id.fragment_tickets_add_button)
	void add()
	{
        ComposeTicketActivity.newInstance(activity);
    }

    public void loadDataWithRefreshing() {
        fragment_tickets_swipe_refresh_layout.setRefreshing(true);
        loadData();
    }
    public void loadData() {

       String url=Constants.TICKET+Constants.DESC;
        Log.e("TAG", "url " + url);
        RequestParameters requestParameters = new RequestParameters(Query.TYPE_REQUEST.GET, -1, url);
        requestParameters.setHeader(Arrays.asList(new BasicHeader("X-Username", PreferencesData.getLoginName(activity)),
                new BasicHeader("X-Password", PreferencesData.getLoginPassword(activity))));
        Query.getInstance().setQuery(activity, new CustomRequestHandler(activity, false) {
            @Override
            public void processingRequest(RequestInterface requestInterface, Object response, int code, int type, Header[] headers) {
//                fragment_tickets_swipe_refresh_layout.setRefreshing(false);
                super.processingRequest(requestInterface, response, code, type, headers);
                Log.e("TAG", "data " + data);
                if (!errorInfo.isResult()) {
                    requestInterface.error(errorInfo);
                } else {
                    List<Ticket> tickets = JsonParser.parseTickets(data);
                    requestInterface.finish(tickets);
                }

            }
        }, new RequestInterface() {
            @Override
            public void finish(Object obj) {
                List<Ticket> tickets=(ArrayList<Ticket>) obj;
                ticketsStickyHeadersAdapter.clear();
                ticketsStickyHeadersAdapter.addAll(tickets);
                if (PreferencesData.isShowResolvedDialog(getActivity())) {
                    PreferencesData.setShowResolvedDialog(activity,false);
                    for (Ticket ticket : tickets) {
                        if (ticket.getTICKET_state() == TICKET_STATE.RESOLVED && ticket.getFeedback() == null) {
                            Dialog dialog = DialogUtility.getAlertDialog(activity, getString(R.string.you_have_tickets_awaiting_rating),
                                    new DialogUtility.OnDialogButtonClickListener() {
                                        @Override
                                        public void onPositiveClick() {

                                        }

                                        @Override
                                        public void onNegativeClick() {

                                        }
                                    });
                            dialog.show();
                            break;
                        }
                    }
                }

            }

            @Override
            public void error(Object obj) {
            }
        }, requestParameters);
    }
}
