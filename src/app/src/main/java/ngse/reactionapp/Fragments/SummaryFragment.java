package ngse.reactionapp.Fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.NGSE.RequestLibrary.Query;
import com.NGSE.RequestLibrary.RequestInterface;
import com.NGSE.RequestLibrary.RequestParameters;

import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ngse.reactionapp.Activities.ChatActivity;
import ngse.reactionapp.Data.Feedback;
import ngse.reactionapp.Data.PreferencesData;
import ngse.reactionapp.Data.Ticket;
import ngse.reactionapp.Enums.TICKET_STATE;
import ngse.reactionapp.R;
import ngse.reactionapp.Utility.Constants;
import ngse.reactionapp.Utility.CustomRequestHandler;
import ngse.reactionapp.Utility.DialogUtility;
import ngse.reactionapp.Utility.Utility;
import ngse.reactionapp.Widgets.EmptyRecyclerView;


public class SummaryFragment extends Fragment {

    public static final String TAG = SummaryFragment.class.getName();
    /*   @Bind(R.id.fragment_summary_img)
       RoundedImageView fragment_summary_img;
       @Bind(R.id.fragment_summary_name)
       TextView fragment_summary_name;*/
    ChatActivity chatActivity;
    @Bind(R.id.activity_chat_summary_subject)
    TextView activity_chat_summary_subject;
    @Bind(R.id.activity_chat_summary_description)
    TextView activity_chat_summary_description;
    @Bind(R.id.fragment_summary_rating_feedback)
    EditText fragment_summary_rating_feedback;
    @Bind(R.id.fragment_summary_mark_3)
    ImageView fragment_summary_mark_3;
    @Bind(R.id.fragment_summary_mark_2)
    ImageView fragment_summary_mark_2;
    @Bind(R.id.fragment_summary_mark_1)
    ImageView fragment_summary_mark_1;
    @Bind(R.id.fragment_summary_rating)
    View fragment_summary_rating;
    @Bind(R.id.fragment_summary_rating_submit)
    TextView fragment_summary_rating_submit;
    @Bind(R.id.fragment_summary_image_list)
    EmptyRecyclerView fragment_summary_image_list;
    @Bind(R.id.empty_view_body)
    View empty_view_body;
    private int mark = -1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_summary, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        chatActivity = (ChatActivity) getActivity();
        Ticket ticket = chatActivity.getTicket();
        if (ticket == null)
            return;
        activity_chat_summary_subject.setText(chatActivity.getTicket().getTitle());
        activity_chat_summary_description.setText(chatActivity.getTicket().getDescription());

        fragment_summary_image_list.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        fragment_summary_image_list.setEmptyView(empty_view_body);
        fragment_summary_image_list.setAdapter(chatActivity.getImagesAdapter());
        updateRatingControls();

    }
    private void updateRatingControls()
    {
        if (chatActivity.getTicket().getTICKET_state() == TICKET_STATE.PROCESS) {
            fragment_summary_rating.setVisibility(View.GONE);
        } else if (chatActivity.getTicket().getFeedback() != null) {
            Feedback feedback = chatActivity.getTicket().getFeedback();
            mark = feedback.getMark();
            fragment_summary_rating_feedback.setText(URLDecoder.decode(feedback.getFeedback()));//feedback.getFeedback());
            fragment_summary_rating_feedback.setKeyListener(null);
            fragment_summary_mark_1.setEnabled(false);
            fragment_summary_mark_2.setEnabled(false);
            fragment_summary_mark_3.setEnabled(false);
            fragment_summary_rating_submit.setVisibility(View.GONE);
        }
        updateCurrentSmile();
    }
    private void updateCurrentSmile() {
        fragment_summary_mark_1.setImageResource(R.drawable.smile);
        fragment_summary_mark_2.setImageResource(R.drawable.meh);
        fragment_summary_mark_3.setImageResource(R.drawable.frown);
        switch (mark) {
            case 1:
                fragment_summary_mark_1.setImageResource(R.drawable.smile_selected);
                break;
            case 2:
                fragment_summary_mark_2.setImageResource(R.drawable.meh_selected);
                break;
            case 3:
                fragment_summary_mark_3.setImageResource(R.drawable.frown_selected);
                break;
        }
    }

    @OnClick(R.id.fragment_summary_rating_submit)
    void fragment_summary_rating_submit() {
        if (fragment_summary_rating_feedback.getText().toString().length()==0 || mark==-1)
            return;
        Dialog dialog = DialogUtility.getAlertDialogTwoButtons(getActivity(), getString(R.string.close_ticket_message), new DialogUtility.OnDialogButtonClickListener() {
            @Override
            public void onPositiveClick() {
                feedback();
            }

            @Override
            public void onNegativeClick() {

            }
        });
        dialog.show();

    }
    @OnClick(R.id.fragment_summary_mark_1)
    void fragment_summary_mark_1() {
        mark = 1;
        updateCurrentSmile();
    }

    @OnClick(R.id.fragment_summary_mark_2)
    void fragment_summary_mark_2() {
        mark = 2;
        updateCurrentSmile();
    }

    @OnClick(R.id.fragment_summary_mark_3)
    void fragment_summary_mark_3() {
        mark = 3;
        updateCurrentSmile();
    }


    void feedback() {
        String feedback = null;
        try {
            feedback = URLEncoder.encode(fragment_summary_rating_feedback.getText().toString(), "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        if (feedback == null || feedback.length() == 0) return;

        final Dialog dialog = DialogUtility.getWaitDialog(getActivity(), getString(R.string.wait), false);
        dialog.show();
        final String url = Constants.FEEDBACK;
        List<NameValuePair> paramsList = new ArrayList<>();
        paramsList.addAll(Arrays.asList(
                new BasicNameValuePair("client_id", PreferencesData.getLoginID(getActivity())),
                new BasicNameValuePair("ticket_id", Long.toString(chatActivity.getTicket().getId())),
                new BasicNameValuePair("feedback", feedback),
                new BasicNameValuePair("mark", Integer.toString(mark))));
        //1: Fine; 2: Good; 3: Bad;
        Log.e(TAG, "url= " + url);
        RequestParameters requestParameters = new RequestParameters(Query.TYPE_REQUEST.POST, -1, paramsList, url);
        requestParameters.setHeader(Arrays.asList(new BasicHeader("X-Username", PreferencesData.getLoginName(getActivity())),
                new BasicHeader("X-Password", PreferencesData.getLoginPassword(getActivity()))));

        Query.getInstance().setQuery(getActivity(), new CustomRequestHandler(getActivity(), true) {
            @Override
            public void processingRequest(RequestInterface requestInterface, Object response, int code, int type, Header[] headers) {
                Utility.closeDialog(dialog);
                super.processingRequest(requestInterface, response, code, type, headers);
                if (!errorInfo.isResult()) {
                    requestInterface.error(errorInfo);
                } else {
                    getActivity().onBackPressed();
                    //updateRatingControls();
                }


            }
        }, new RequestInterface() {
            @Override
            public void finish(Object obj) {

            }

            @Override
            public void error(Object obj) {
            }
        }, requestParameters);

    }

}
