package ngse.reactionapp.Fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.NGSE.RequestLibrary.Query;
import com.NGSE.RequestLibrary.RequestInterface;
import com.NGSE.RequestLibrary.RequestParameters;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersDecoration;

import org.apache.http.Header;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.message.BasicHeader;

import java.net.URLEncoder;
import java.util.Arrays;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ngse.reactionapp.Activities.ChatActivity;
import ngse.reactionapp.Data.Manager;
import ngse.reactionapp.Data.Message;
import ngse.reactionapp.Data.PreferencesData;
import ngse.reactionapp.Data.Ticket;
import ngse.reactionapp.Data.TicketFile;
import ngse.reactionapp.R;
import ngse.reactionapp.Utility.CustomRequestHandler;
import ngse.reactionapp.Utility.DialogUtility;
import ngse.reactionapp.Utility.JsonParser;
import ngse.reactionapp.Utility.Utility;


public class ChatFragment extends Fragment {

    public static final String TAG = ChatFragment.class.getName();


    @Bind(R.id.activity_chat_recycler_view)
    RecyclerView activity_chat_recycler_view;
    @Bind(R.id.activity_chat_message_body)
    EditText activity_chat_message_body;
    @Bind(R.id.activity_chat_attachment)
    ImageView activity_chat_attachment;
    private ChatActivity chatActivity;
    private Manager manager;
    private Ticket ticket;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_chat, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        chatActivity = (ChatActivity) getActivity();
        ticket = chatActivity.getTicket();
        chatActivity.setRecyclerView(activity_chat_recycler_view);
        activity_chat_recycler_view.setLayoutManager(new LinearLayoutManager(getActivity()));
        // chatRecycleViewAdapter = new ChatRecycleViewAdapter(getActivity(), messages);


        activity_chat_recycler_view.setAdapter(chatActivity.getChatStickyHeadersAdapter());
        final StickyRecyclerHeadersDecoration headersDecor = new StickyRecyclerHeadersDecoration(chatActivity.getChatStickyHeadersAdapter());
        activity_chat_recycler_view.addItemDecoration(headersDecor);
        if (ticket != null) {
            manager = ticket.getManager();
            chatActivity.loadData();

        }
        chatActivity.getChatStickyHeadersAdapter().registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                headersDecor.invalidateHeaders();
            }
        });
        if (chatActivity.getTicket().getFeedback()!=null)
        {
            activity_chat_message_body.setKeyListener(null);
            activity_chat_message_body.setHint(R.string.chat_closed);
            activity_chat_attachment.setEnabled(false);
        }
    }


    @OnClick(R.id.activity_chat_attachment)
    void addFile() {
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_attachment, null);
        TextView take_photo= (TextView) view.findViewById(R.id.take_photo);
        TextView take_a_video= (TextView) view.findViewById(R.id.take_a_video);
        TextView attach_a_files= (TextView) view.findViewById(R.id.attach_a_files);
        final Dialog dialog=DialogUtility.getAlertDialog(getActivity(),view);
        dialog.show();
        take_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chatActivity.takePhoto();
                Utility.closeDialog(dialog);
            }
        });
        take_a_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chatActivity.takeVideo();
                Utility.closeDialog(dialog);
            }
        });
        attach_a_files.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chatActivity.uploadFile();
                Utility.closeDialog(dialog);
            }
        });


        //
    }

    @OnClick(R.id.activity_chat_send)
    void postMessage() {
        addMessage();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }


    private void addMessageToList(Message resultMessage) {
        chatActivity.addMessageToList(resultMessage);
    }


    public void addMessage() {
        final Dialog dialog = DialogUtility.getWaitDialog(getActivity(), getString(R.string.wait), false);
        try {
            String message = URLEncoder.encode(activity_chat_message_body.getText().toString(), "utf-8");

            if ((message.length() == 0 && chatActivity.getChatFiles().size()==0) || ticket == null)
                return;

            dialog.show();
            String url = "http://reactionapp.artwebit.ru/api/chat/post?Message[to_id]=" + manager.getId() + "&Message[ticket_id]=" + ticket.getId() +
                    "&Message[from_id]=" + PreferencesData.getLoginID(getActivity()) + "&Message[message]=" + message;
            Log.e("TAG", "url " + url);


            MultipartEntityBuilder multipartEntity = MultipartEntityBuilder.create();
            for (TicketFile ticketFile : chatActivity.getChatFiles()) {
                Log.i("TAG", ticketFile.getFile().getAbsolutePath());

                switch (ticketFile.getFileType()) {
                    case IMAGE:
                        multipartEntity.addBinaryBody("_images[]", ticketFile.getFile());
                        break;
                    case VIDEO:
                        multipartEntity.addBinaryBody("_videos[]", ticketFile.getFile());
                        break;
                    case DOC:
                        multipartEntity.addBinaryBody("_files[]", ticketFile.getFile());
                        break;
                }

            }

            activity_chat_message_body.setText("");
            RequestParameters requestParameters = new RequestParameters(Query.TYPE_REQUEST.MULTI_PART, -1, multipartEntity, url);
            requestParameters.setHeader(Arrays.asList(new BasicHeader("X-Username", PreferencesData.getLoginName(chatActivity)),
                    new BasicHeader("X-Password", PreferencesData.getLoginPassword(chatActivity))));
            Log.d("TAG", "url= " + requestParameters.getUrl());
            Query.getInstance().setQuery(chatActivity, new CustomRequestHandler(chatActivity, true) {
                @Override
                public void processingRequest(RequestInterface requestInterface, Object response, int code, int type, Header[] headers) {
                    Utility.closeDialog(dialog);
                    super.processingRequest(requestInterface, response, code, type, headers);
                    if (!errorInfo.isResult()) {
                        requestInterface.error(errorInfo);
                    } else {
                        Log.e("TAG", "json= " + data);
                        Message resultMessage = JsonParser.parseMessage(data);
                        requestInterface.finish(resultMessage);
                    }
                    chatActivity.clearChatFiles();
                }
            }, new RequestInterface() {
                @Override
                public void finish(Object obj) {
                    Message resultMessage = (Message) obj;
                    addMessageToList(resultMessage);
                }

                @Override
                public void error(Object obj) {
                }
            }, requestParameters);

        } catch (Exception e) {
            Utility.closeDialog(dialog);
            Log.e("TAG", "Exception= " + e.getMessage());
        }
    }


}
