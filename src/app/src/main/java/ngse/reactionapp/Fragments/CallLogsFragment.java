package ngse.reactionapp.Fragments;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.NGSE.RequestLibrary.Query;
import com.NGSE.RequestLibrary.RequestInterface;
import com.NGSE.RequestLibrary.RequestParameters;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;

import org.apache.http.Header;
import org.apache.http.message.BasicHeader;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import ngse.reactionapp.Activities.CompanyActivity;
import ngse.reactionapp.Activities.MainActivity;
import ngse.reactionapp.Adapters.CallLogsRecycleViewAdapter;
import ngse.reactionapp.Data.Call;
import ngse.reactionapp.Data.PreferencesData;
import ngse.reactionapp.R;
import ngse.reactionapp.Utility.Constants;
import ngse.reactionapp.Utility.CustomRequestHandler;
import ngse.reactionapp.Widgets.EmptyRecyclerView;
import ngse.reactionapp.Utility.JsonParser;
import ngse.reactionapp.Utility.Utility;


public class CallLogsFragment extends BaseFragment implements CallLogsRecycleViewAdapter.OnItemClickListener, SearchView.OnQueryTextListener {

    public static final int PICK_CONTACT = 3001;

    public static final String TAG = CallLogsFragment.class.getName();
    @Bind(R.id.call_fragment_swipe_refresh_layout)
    SwipeRefreshLayout call_fragment_swipe_refresh_layout;
    @Bind(R.id.call_fragment_recycler_view)
    EmptyRecyclerView call_fragment_recycler_view;
    @Bind(R.id.empty_view_body)
    View empty_view_body;
    @Bind(R.id.no_call_logs)
    LinearLayout no_call_logs;
    @Bind(R.id.tv_click_to_call)
    TextView tv_click_to_call;

    @Bind(R.id.multiple_actions_up)
    FloatingActionsMenu floatingMenu;
    @Bind(R.id.button_contacts)
    FloatingActionButton buttonContacts;
    @Bind(R.id.button_add_new_contact)
    FloatingActionButton buttonNewContact;
    @Bind(R.id.button_dial_number)
    FloatingActionButton buttonDialNumber;

    boolean contactsgranted = false;

    private List<Call> callLogsItems = new ArrayList<>();
    private CallLogsRecycleViewAdapter callLogsRecycleViewAdapter;

    private static final Comparator<Call> DATE_COMPARATOR = new Comparator<Call>() {
        @Override
        public int compare(Call a, Call b) {
            return b.getCreatedAt().compareTo(a.getCreatedAt());
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.CALL_PHONE}, 10003);

        }
//        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.SYSTEM_ALERT_WINDOW)
//                != PackageManager.PERMISSION_GRANTED) {
//
//            ActivityCompat.requestPermissions(getActivity(),
//                    new String[]{Manifest.permission.SYSTEM_ALERT_WINDOW}, 10004);
//
//        }

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && !Settings.canDrawOverlays(getActivity())) {

            /** if not construct intent to request permission */
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + getActivity().getPackageName()));
            /** request permission via start activity for result */
            getActivity().startActivityForResult(intent, 10005);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        call_fragment_swipe_refresh_layout.setRefreshing(true);
        loadData();

    }

    private static List<Call> filter(List<Call> models, String query) {
        final String lowerCaseQuery = query.toLowerCase();

        final List<Call> filteredModelList = new ArrayList<>();
        for (Call model : models) {
            final String phoneString = model.getPhone().toLowerCase();
            String nameString = model.getContactName();
            if (nameString != null) {
                nameString = nameString.toLowerCase();
                if (phoneString.contains(lowerCaseQuery) || nameString.contains(lowerCaseQuery) ) {
                    filteredModelList.add(model);
                }
            } else {
                if (phoneString.contains(lowerCaseQuery)) {
                    filteredModelList.add(model);
                }
            }

        }
        return filteredModelList;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_action_search, menu);
        MenuItem item = menu.findItem(R.id.menu_action_search);
        SearchView searchView = new SearchView(((MainActivity) getContext()).getSupportActionBar().getThemedContext());
        MenuItemCompat.setShowAsAction(item, MenuItemCompat.SHOW_AS_ACTION_ALWAYS);
        MenuItemCompat.setActionView(item, searchView);
        searchView.setOnQueryTextListener(this);

        searchView.setOnClickListener(new View.OnClickListener() {
                                          @Override
                                          public void onClick(View v) {
                                              floatingMenu.collapse();
                                          }
                                      }
        );

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        final List<Call> filteredModelList = filter(callLogsItems, newText);
        callLogsRecycleViewAdapter.edit()
                .replaceAll(filteredModelList)
                .commit();
        call_fragment_recycler_view.scrollToPosition(0);
        return true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_call_logs, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        call_fragment_recycler_view.setLayoutManager(new LinearLayoutManager(getActivity()));
        call_fragment_swipe_refresh_layout.setColorSchemeColors(getResources().getIntArray(R.array.main_refresh_scheme));
        callLogsRecycleViewAdapter = new CallLogsRecycleViewAdapter(activity, DATE_COMPARATOR, this);
        call_fragment_recycler_view.setAdapter(callLogsRecycleViewAdapter);
        call_fragment_swipe_refresh_layout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData();
            }
        });
        call_fragment_recycler_view.setEmptyView(empty_view_body);

        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.SYSTEM_ALERT_WINDOW) != PackageManager.PERMISSION_GRANTED) {
//            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
//                    Manifest.permission.SYSTEM_ALERT_WINDOW)) {
//            } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.SYSTEM_ALERT_WINDOW}, 1);
//            }
        }
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
//            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
//                    Manifest.permission.CALL_PHONE)) {
//            } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.CALL_PHONE}, 2);
//            }
        }

        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.READ_CALL_LOG}, 3);
        }
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 4);
        }
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_NETWORK_STATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_NETWORK_STATE}, 4);
        }
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.INTERNET}, 4);
        }
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.READ_PHONE_STATE}, 5);
        }


        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.READ_CONTACTS}, 6);
        } else {
            contactsgranted = true;
        }

        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.WRITE_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.WRITE_CONTACTS}, 7);
        }

        buttonContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.READ_CONTACTS}, 6);
                } else {
                    floatingMenu.collapse();
                    Intent pickContactIntent = new Intent(Intent.ACTION_PICK, Uri.parse("content://contacts"));
                    pickContactIntent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE); // Show user only contacts w/ phone numbers
                    startActivityForResult(pickContactIntent, PICK_CONTACT);
                }
            }
        });

        buttonNewContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.WRITE_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.WRITE_CONTACTS}, 7);
                } else {
                    floatingMenu.collapse();
                    Intent intent = new Intent(Intent.ACTION_INSERT);
                    intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
                    startActivityForResult(intent, 3002);
                }
            }
        });

        buttonDialNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO
                floatingMenu.collapse();
                Intent i = new Intent(Intent.ACTION_DIAL, null);
                startActivityForResult(i, 3003);
            }
        });

    }

    private void loadData() {
        callLogsItems.clear();
        //callLogsRecycleViewAdapter.notifyDataSetChanged();
        RequestParameters requestParameters = new RequestParameters(Query.TYPE_REQUEST.GET, -1, Constants.CALL);
        requestParameters.setHeader(
                Arrays.asList(
                        new BasicHeader("X-Username", PreferencesData.getLoginName(activity)),
                        new BasicHeader("X-Password", PreferencesData.getLoginPassword(activity))
                )
        );
        Query.getInstance().setQuery(activity, new CustomRequestHandler(activity, false) {
            @Override
            public void processingRequest(RequestInterface requestInterface, Object response, int code, int type, Header[] headers) {
                if (call_fragment_swipe_refresh_layout != null) {
                    call_fragment_swipe_refresh_layout.setRefreshing(false);
                }
                super.processingRequest(requestInterface, response, code, type, headers);
                if (!errorInfo.isResult()) {
                    requestInterface.error(errorInfo);
                } else {
                    List<Call> calls = JsonParser.parseCalls(data);
                    requestInterface.finish(calls);
                }

            }
        }, new RequestInterface() {
            @Override
            public void finish(Object obj) {
                //TODO
                callLogsItems.clear();
                callLogsItems.addAll((ArrayList<Call>) obj);
                callLogsItems.addAll(getStandartCallList());

                Collections.sort(callLogsItems, new Comparator<Call>() {
                    @Override
                    public int compare(Call o1, Call o2) {
                        return o2.getCallDate().compareTo(o1.getCallDate());
                    }
                });

                if (callLogsItems.isEmpty()) {
                    no_call_logs.setVisibility(View.VISIBLE);
                } else {
                    if (no_call_logs != null) no_call_logs.setVisibility(View.GONE);
                }

                Collections.reverse(callLogsItems);
                callLogsRecycleViewAdapter.edit().removeAll().add(callLogsItems).commit();
            }

            @Override
            public void error(Object obj) {
            }
        }, requestParameters);
    }


    @Override
    public void onItemClick(View view, Call callLogsItem) {
        if (view.getId() != R.id.list_item_call_logs_call) {
            CompanyActivity.newInstance(activity, view.findViewById(R.id.list_item_call_logs_img), callLogsItem);
        } else {
            Utility.startCall(activity, callLogsItem.getPhone(), callLogsItem.getId());
//            Utility.startCall(activity, callLogsItem.getManager().getPhone(), callLogsItem.getManager().getId());
        }
    }

    @OnClick(R.id.tv_click_to_call)
    void clickToCall() {
//        Utility.startCall(activity, "", 0);
        try {
            Intent intent = new Intent(Intent.ACTION_CALL);
            if (Utility.isPackageInstalled("com.android.phone", getContext()))
                intent.setClassName("com.android.phone", "com.android.phone.OutgoingCallBroadcaster");
            intent.setData(Uri.parse("tel:8006890717"));
            this.startActivity(intent);
        } catch (Exception e) {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:8006890717"));
            this.startActivity(callIntent);
        }
    }

    private List<Call> getStandartCallList() {
        List<Call> standartCallList = new ArrayList<>();
        //StringBuffer sb = new StringBuffer();
        Cursor managedCursor = getActivity().managedQuery(CallLog.Calls.CONTENT_URI, null, null, null, null);
        int id = managedCursor.getColumnIndex(CallLog.Calls._ID);
        int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
        int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
        int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
        int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);
        //sb.append("Call Details :");
        while (managedCursor.moveToNext()) {
            Call call = new Call();
            call.setId(managedCursor.getLong(id));
            call.setPhone(managedCursor.getString(number));
            String callType = managedCursor.getString(type);
            //call.setCallType(managedCursor.getString(type));
            call.setDuration(Integer.valueOf(managedCursor.getString(duration)));
            if (contactsgranted)
                call.setContactName(getContactName(getContext(), call.getPhone()));

            call.setCallDate(managedCursor.getLong(date));


            int dircode = Integer.parseInt(callType);
            switch (dircode) {
                case CallLog.Calls.OUTGOING_TYPE:
                    call.setCallType(2);
                    break;

                case CallLog.Calls.INCOMING_TYPE:
                    call.setCallType(1);
                    break;

                case CallLog.Calls.MISSED_TYPE:
                    call.setCallType(3);
                    break;
            }
            standartCallList.add(call);
        }
        //managedCursor.close();
        return standartCallList;
    }

    public static String getContactName(Context context, String phoneNumber) {
        ContentResolver cr = context.getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
        Cursor cursor = cr.query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);
        if (cursor == null) {
            return null;
        }
        String contactName = null;
        if(cursor.moveToFirst()) {
            contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
        }

        if(cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return contactName;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        floatingMenu.collapse();
        switch (requestCode) {
            case (PICK_CONTACT) :
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactData = data.getData();
                    Cursor c =  getActivity().managedQuery(contactData, null, null, null, null);
                    getActivity().startManagingCursor(c);
                    if (c.moveToFirst()) {
                        Uri contactUri = data.getData();
                        // We only need the NUMBER column, because there will be only one row in the result
                        String[] projection = {ContactsContract.CommonDataKinds.Phone.NUMBER};

                        // Perform the query on the contact to get the NUMBER column
                        // We don't need a selection or sort order (there's only one result for the given URI)
                        // CAUTION: The query() method should be called from a separate thread to avoid blocking
                        // your app's UI thread. (For simplicity of the sample, this code doesn't do that.)
                        // Consider using CursorLoader to perform the query.
                        Cursor cursor = getActivity().getContentResolver()
                                .query(contactUri, projection, null, null, null);
                        cursor.moveToFirst();

                        // Retrieve the phone number from the NUMBER column
                        int column = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                        String number = "tel:" + cursor.getString(column);
                        Log.d("Logger", number);

                        try {
                            Intent intent = new Intent(Intent.ACTION_CALL);
                            if (Utility.isPackageInstalled("com.android.phone", getContext()))
                                intent.setClassName("com.android.phone", "com.android.phone.OutgoingCallBroadcaster");
                            intent.setData(Uri.parse(number));
                            this.startActivity(intent);
                        } catch (Exception e) {
                            Intent callIntent = new Intent(Intent.ACTION_CALL);
                            callIntent.setData(Uri.parse(number));
                            this.startActivity(callIntent);
                        }

                    }
                }
                break;
        }
    }
}
