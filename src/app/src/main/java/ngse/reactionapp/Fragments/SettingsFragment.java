package ngse.reactionapp.Fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;

import com.NGSE.textviewpluslib.TextViewPlus;
import com.squareup.picasso.Picasso;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import butterknife.Bind;
import butterknife.OnClick;
import ngse.reactionapp.Activities.AboutActivity;
import ngse.reactionapp.Activities.LoginActivity;
import ngse.reactionapp.Activities.ProfileActivity;
import ngse.reactionapp.Applications.MainApplication;
import ngse.reactionapp.Data.PreferencesData;
import ngse.reactionapp.Data.Profile;
import ngse.reactionapp.R;
import ngse.reactionapp.Utility.DialogUtility;
import ngse.reactionapp.Utility.Utility;


public class SettingsFragment extends BaseFragment {

    public static final String TAG = SettingsFragment.class.getName();
    @Bind(R.id.settings_fragment_profile_name)
    TextViewPlus settings_fragment_profile_name;
    @Bind(R.id.settings_fragment_profile_location)
    TextViewPlus settings_fragment_profile_location;
    @Bind(R.id.settings_fragment_profile_img)
    ImageView settings_fragment_profile_img;
    private PropertyChangeListener propertyChangeListener = new PropertyChangeListener() {
        @Override
        public void propertyChange(PropertyChangeEvent event) {
            if (isAdded()) {
                updateView();
            }

        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MainApplication application = (MainApplication) activity.getApplication();
        application.getPropertyChangeSupport().addPropertyChangeListener(
                Profile.class.getName(),
                propertyChangeListener);
    }

    @Override
    public void onResume() {
        super.onResume();
        updateView();

    }

    private void updateView() {
        MainApplication application = (MainApplication) activity.getApplication();
        Profile profile = application.getPropertyChangeSupport().getProfile();
        if (profile == null)
            return;
        settings_fragment_profile_name.setText(profile.getFirstname() + " " + profile.getLastname());
        settings_fragment_profile_location.setText(profile.getAddress());
        Picasso.with(settings_fragment_profile_img.getContext()).load(profile.getImageUrl()).into(settings_fragment_profile_img);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_settings, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    @OnClick(R.id.settings_fragment_profile)
    void profile() {
        MainApplication application = (MainApplication) activity.getApplication();
        Profile profile = application.getPropertyChangeSupport().getProfile();
        if (profile != null)
            ProfileActivity.newInstance(activity, profile);
    }

    @OnClick(R.id.settings_fragment_notification)
    void notification() {
        final CheckBox checkBox = new CheckBox(activity);
        checkBox.setText(getString(R.string.settings_fragment_enable_notification));
        checkBox.setChecked(PreferencesData.getEnableNotifications(activity));
        Dialog dialog = new AlertDialog.Builder(activity)//
                .setTitle(R.string.settings_fragment_notification)//
                .setView(checkBox)
                .setCancelable(true)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        PreferencesData.setEnableNotifications(activity, checkBox.isChecked());
                        Utility.startCheckService(activity);
                        dialog.dismiss();
                    }
                }).create();

        dialog.show();
    }

    @OnClick(R.id.settings_fragment_about)
    void about() {
        AboutActivity.newInstance(activity);
    }
    @OnClick(R.id.settings_fragment_feedback)
    void feedback() {
        Utility.sendEmail(activity, getString(R.string.email),
                getString(R.string.app_name), getString(R.string.review),
                getString(R.string.select_app_to_send));
    }

    @OnClick(R.id.settings_fragment_exit)
    void exit() {
        Dialog dialog = DialogUtility.getAlertDialogTwoButtons(activity,
                getString(R.string.exitAttention),
                new DialogUtility.OnDialogButtonClickListener() {
                    @Override
                    public void onPositiveClick() {
                        PreferencesData.resetAuthInfo(activity);
                        activity.finish();
                        LoginActivity.newInstance(activity);
                    }

                    @Override
                    public void onNegativeClick() {

                    }
                });
        dialog.show();
    }

    @OnClick(R.id.settings_fragment_share_rl)
    void share() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "Reaction app Share");
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        MainApplication application = (MainApplication) activity.getApplication();
        application.getPropertyChangeSupport().removePropertyChangeListener(Profile.class.getName(),
                propertyChangeListener);
    }

}
