package ngse.reactionapp.Fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.NGSE.RequestLibrary.Query;
import com.NGSE.RequestLibrary.RequestInterface;
import com.NGSE.RequestLibrary.RequestParameters;
import com.NGSE.textviewpluslib.TextViewPlus;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import ngse.reactionapp.Data.PreferencesData;
import ngse.reactionapp.Data.Ticket;
import ngse.reactionapp.Activities.ChatActivity;
import ngse.reactionapp.R;
import ngse.reactionapp.Utility.Constants;
import ngse.reactionapp.Utility.CustomRequestHandler;
import ngse.reactionapp.Utility.DialogUtility;
import ngse.reactionapp.Utility.Utility;


public class TicketRatingFragment extends BaseFragment {

    public static final String TAG = TicketRatingFragment.class.getName();
    private static final String KEY_TICKET = "ticket";
    @Bind(R.id.fragment_ticket_rating_img)
    RoundedImageView fragment_ticket_rating_img;
    @Bind(R.id.fragment_ticket_rating_name)
    TextViewPlus fragment_ticket_rating_name;
    @Bind(R.id.fragment_ticket_rating_company)
    TextViewPlus fragment_ticket_rating_company;
    @Bind(R.id.fragment_ticket_rating_date)
    TextViewPlus fragment_ticket_rating_date;
    @Bind(R.id.fragment_ticket_rating_type)
    ImageView fragment_ticket_rating_type;
    @Bind(R.id.fragment_ticket_rating_ll_1)
    LinearLayout fragment_ticket_rating_ll_1;
    @Bind(R.id.fragment_ticket_rating_ll_2)
    LinearLayout fragment_ticket_rating_ll_2;
    @Bind(R.id.fragment_ticket_rating_next)
    TextViewPlus fragment_ticket_rating_next;
    @Bind(R.id.fragment_ticket_rating_submit)
    TextViewPlus fragment_ticket_rating_submit;
    @Bind(R.id.fragment_ticket_rating_feedback)
    EditText fragment_ticket_rating_feedback;

    @Bind(R.id.fragment_mark_3)
    ImageView fragment_mark_3;
    @Bind(R.id.fragment_mark_2)
    ImageView fragment_mark_2;
    @Bind(R.id.fragment_mark_1)
    ImageView fragment_mark_1;
    private Ticket ticket;
    private int mark=1;
    public static TicketRatingFragment newInstance(Ticket ticket) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(KEY_TICKET, ticket);
        TicketRatingFragment fragment = new TicketRatingFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            ticket = (Ticket) args.getSerializable(KEY_TICKET);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_ticket_rating, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.setOnClickListener(null);
        if (ticket == null)
            return;
        if (ticket.getManager()!=null &&ticket.getManager().getImageUrl() != null && ticket.getManager().getImageUrl().length() > 0) {
            Picasso.with(fragment_ticket_rating_img.getContext()).load(ticket.getManager().getImageUrl()).
                    into(fragment_ticket_rating_img);

        }
        if (ticket.getManager()!=null)
        fragment_ticket_rating_name.setText(ticket.getTitle());
        if (ticket.getManager()!=null)
        fragment_ticket_rating_company.setText(ticket.getManager().getFirstname());
        else
            fragment_ticket_rating_company.setText("");
        fragment_ticket_rating_date.setText(ticket.getUpdatedAt());
        updateCurrentSmile();
    }

    @OnClick(R.id.fragment_ticket_rating_next)
    void fragment_ticket_rating_next() {
        String body=fragment_ticket_rating_feedback.getText().toString();
        if (body==null || body.length()==0)
            return;
        fragment_ticket_rating_next.setVisibility(View.GONE);
        fragment_ticket_rating_ll_1.setVisibility(View.GONE);
        fragment_ticket_rating_submit.setVisibility(View.VISIBLE);
        fragment_ticket_rating_ll_2.setVisibility(View.VISIBLE);
    }
    private void updateCurrentSmile()
    {
        fragment_mark_1.setImageResource(R.drawable.smile);
        fragment_mark_2.setImageResource(R.drawable.meh);
        fragment_mark_3.setImageResource(R.drawable.frown);
        switch (mark)
        {
            case 1:
                fragment_mark_1.setImageResource(R.drawable.smile_selected);
                break;
            case 2:
                fragment_mark_2.setImageResource(R.drawable.meh_selected);
                break;
            case 3:
                fragment_mark_3.setImageResource(R.drawable.frown_selected);
                break;
        }
    }
    @OnClick(R.id.fragment_mark_1)
    void fragment_mark_1() {
        mark = 1;
        updateCurrentSmile();
    }
    @OnClick(R.id.fragment_mark_2)
    void fragment_mark_2() {
        mark=2;
        updateCurrentSmile();
    }
    @OnClick(R.id.fragment_mark_3)
    void fragment_mark_3() {
        mark = 3;
        updateCurrentSmile();
    }
    @OnClick(R.id.fragment_ticket_rating_submit)
    void fragment_ticket_rating_submit() {
        feedback();
    }

    @OnClick(R.id.fragment_ticket_body)
    void fragment_ticket_body() {
        if (ticket != null)
            ChatActivity.newInstance(activity, ticket);
    }

    void feedback() {
        String feedback = null;

        try {
            feedback = URLEncoder.encode(fragment_ticket_rating_feedback.getText().toString(), "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        if (feedback == null || feedback.length() == 0) return;

        final Dialog dialog = DialogUtility.getWaitDialog(activity, getString(R.string.wait), false);
        dialog.show();
        final String url = Constants.FEEDBACK;
        List<NameValuePair> paramsList = new ArrayList<>();
        paramsList.addAll(Arrays.asList(
                new BasicNameValuePair("client_id", PreferencesData.getLoginID(activity)),
                new BasicNameValuePair("ticket_id", Long.toString(ticket.getId())),
                new BasicNameValuePair("feedback", feedback),
                new BasicNameValuePair("mark", Integer.toString(mark))));
        //1: Fine; 2: Good; 3: Bad;
        Log.e(TAG, "url= " + url);
        RequestParameters requestParameters = new RequestParameters(Query.TYPE_REQUEST.POST, -1, paramsList, url);
        requestParameters.setHeader(Arrays.asList(new BasicHeader("X-Username", PreferencesData.getLoginName(activity)),
                new BasicHeader("X-Password", PreferencesData.getLoginPassword(activity))));

        Query.getInstance().setQuery(activity, new CustomRequestHandler(activity, true) {
            @Override
            public void processingRequest(RequestInterface requestInterface, Object response, int code, int type, Header[] headers) {
                Utility.closeDialog(dialog);
                super.processingRequest(requestInterface, response, code, type, headers);
                if (!errorInfo.isResult()) {
                    requestInterface.error(errorInfo);
                } else {
                    Log.e(TAG, "json= " + data);
                    TicketPageFragment ticketPageFragment= (TicketPageFragment) getParentFragment();
                    ticketPageFragment.onBackPressed();
                    ticketPageFragment.updateTickets();
                }


            }
        }, new RequestInterface() {
            @Override
            public void finish(Object obj) {

            }

            @Override
            public void error(Object obj) {
            }
        }, requestParameters);

    }

}
