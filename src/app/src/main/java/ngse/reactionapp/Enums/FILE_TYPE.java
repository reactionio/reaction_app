package ngse.reactionapp.Enums;


public enum FILE_TYPE {
    IMAGE(1), VIDEO(2), DOC(0);

    private int value;

    FILE_TYPE(int value) {
        this.value = value;
    }

    public static FILE_TYPE fromValue(int i) {
        for (FILE_TYPE type : FILE_TYPE.values()) {
            if (type.getValue() == i) {
                return type;
            }
        }
        return DOC;
    }

    public int getValue() {
        return value;
    }
}
