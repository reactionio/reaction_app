package ngse.reactionapp.Enums;

public enum TICKET_STATE {
    CLOSED(1), PROCESS(2), RESOLVED(3), NONE(0);

    private int value;

    TICKET_STATE(int value) {
        this.value = value;
    }

    public static TICKET_STATE fromValue(int i) {
        for (TICKET_STATE streamStatus : TICKET_STATE.values()) {
            if (streamStatus.getValue() == i) {
                return streamStatus;
            }
        }
        return NONE;
    }

    public int getValue() {
        return value;
    }
}
