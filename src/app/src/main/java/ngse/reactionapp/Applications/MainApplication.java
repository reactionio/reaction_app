package ngse.reactionapp.Applications;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.facebook.login.DefaultAudience;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.SimpleFacebookConfiguration;

import io.fabric.sdk.android.Fabric;
import ngse.reactionapp.Utility.CustomPropertyChangeSupport;


/**
 * Created by Kulykov Anton on 04.09.15.
 */
public class MainApplication extends Application {

    private CustomPropertyChangeSupport propertyChangeSupport;
    private static final String APP_ID = "1506369706320558";
    private static final String APP_NAMESPACE = "sp_socapp";
    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        propertyChangeSupport = new CustomPropertyChangeSupport(this);
        Picasso.Builder builder = new Picasso.Builder(this);
        builder.downloader(new OkHttpDownloader(this, Integer.MAX_VALUE));
        Picasso built = builder.build();
        built.setIndicatorsEnabled(false);
        built.setLoggingEnabled(true);
        Picasso.setSingletonInstance(built);



        // initialize facebook configuration
        Permission[] permissions = new Permission[] {
                Permission.USER_ABOUT_ME,
                Permission.EMAIL,
                Permission.USER_EVENTS,
                Permission.USER_ACTIONS_MUSIC,
                Permission.USER_FRIENDS,
                Permission.USER_GAMES_ACTIVITY,
                Permission.USER_BIRTHDAY,
                Permission.USER_LOCATION,
                //Permission.READ_STREAM,
                // Permission.USER_GROUPS,
                Permission.PUBLISH_ACTION
        };

        SimpleFacebookConfiguration configuration = new SimpleFacebookConfiguration.Builder()
                .setAppId(APP_ID)
                .setNamespace(APP_NAMESPACE)
                .setPermissions(permissions)
                .setDefaultAudience(DefaultAudience.FRIENDS)
                .setAskForAllPermissionsAtOnce(false)
                .build();

        SimpleFacebook.setConfiguration(configuration);


    }
    public CustomPropertyChangeSupport getPropertyChangeSupport() {
        return propertyChangeSupport;
    }




}