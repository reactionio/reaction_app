package ngse.reactionapp.Data;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import ngse.reactionapp.Enums.TICKET_STATE;
import ngse.reactionapp.Utility.Constants;

public class Ticket implements Serializable {

    private long id;
    private long userId;
    private long clientId;
    private String title;
    private String description;
    private String statusText;
    private String createdAt;
    private String updatedAt;
    private Manager manager;
    private Feedback feedback;
    private ArrayList<String> images=new ArrayList<>();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getClientId() {
        return clientId;
    }

    public void setClientId(long clientId) {
        this.clientId = clientId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        if (statusText.toLowerCase().equals("none"))
            statusText="Send";
        this.statusText = statusText;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return formatDate(updatedAt);
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public void setManager(Manager manager) {
        this.manager = manager;
    }

    public Manager getManager() {
        return manager;
    }

    public Feedback getFeedback() {
        return feedback;
    }

    public void setFeedback(Feedback feedback) {
        this.feedback = feedback;
    }

    public TICKET_STATE getTICKET_state() {
        if (statusText.equals(Constants.STATUS_RESOLVED))
            return  TICKET_STATE.RESOLVED;
        if (statusText.equals(Constants.STATUS_CLOSED))
            return  TICKET_STATE.CLOSED;
        if (statusText.equals(Constants.STATUS_PROCESS))
            return  TICKET_STATE.PROCESS;

        return TICKET_STATE.NONE;
    }
    public String getCreatedAt() {
        return formatDate(createdAt);
    }
    private String formatDate(String date)
    {
        String outDate = "";
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date newDate = format.parse(date);
            format = new SimpleDateFormat("yyyy.MM.dd");
            outDate = format.format(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return outDate;
    }
}
