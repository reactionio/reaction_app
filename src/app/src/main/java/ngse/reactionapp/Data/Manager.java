package ngse.reactionapp.Data;

import java.io.Serializable;

/**
 * Created by Kulykov on 22.09.2015.
 */
public class Manager extends Profile implements Serializable {
    //private String imageUrl;
    private String about;

    public void setProfileData(Profile profile)
    {
        setFirstname(profile.getFirstname());
        setLastname(profile.getLastname());
        setEmail(profile.getEmail());
        setPhone(profile.getPhone());
        setAddress(profile.getAddress());
        setId(profile.getId());
        setImageUrl(profile.getImageUrl());
    }

  /*  public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
*/
    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }
}
