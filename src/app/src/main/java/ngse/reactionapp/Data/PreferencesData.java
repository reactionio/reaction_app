package ngse.reactionapp.Data;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

public class PreferencesData {

    private static final String TAG = PreferencesData.class.getName();
    private static final String TAG_LAST_CALL_NUMBER = "lastCallNumber";
    private static final String TAG_LAST_CALL_TIME = "lastCallTime";
    private static final String TAG_LAST_CALL_MANAGER_ID = "lastCallManagerID";
    private static final String TAG_LOGIN_NAME = "loginName";
    private static final String TAG_LOGIN_PASSWORD = "loginPassword";
    private static final String TAG_LOGIN_ID = "loginID";

    private static final String TAG_NOTIFICATIONS_ENABLE = "notificationsEnable";
    private static final String TAG_SHOW_RESOLVED_DIALOG = "showResolvedDialog";


    public static String getLastCallNumber(Context context) {
        return getString(context, TAG_LAST_CALL_NUMBER, "");
    }

    public static void setLastCallNumber(Context context, String value) {
        save(context, TAG_LAST_CALL_NUMBER, value);
    }

    public static long getLastCallTime(Context context) {
        return getLong(context, TAG_LAST_CALL_TIME, 0);
    }

    public static void setLastCallTime(Context context, long value) {
        save(context, TAG_LAST_CALL_TIME, value);
    }

    public static long getLastCallManagerID(Context context) {
        return getLong(context, TAG_LAST_CALL_MANAGER_ID, 0);
    }
    public static void setLastCallManagerID(Context context, long value) {
        save(context, TAG_LAST_CALL_MANAGER_ID, value);
    }

    public static boolean getEnableNotifications(Context context) {
        return getBoolean(context, TAG_NOTIFICATIONS_ENABLE, true);
    }
    public static void setEnableNotifications(Context context, boolean enable) {
        save(context, TAG_NOTIFICATIONS_ENABLE, enable);
    }

    public static boolean isShowResolvedDialog(Context context) {
        return getBoolean(context, TAG_SHOW_RESOLVED_DIALOG, true);
    }
    public static void setShowResolvedDialog(Context context, boolean enable) {
        save(context, TAG_SHOW_RESOLVED_DIALOG, enable);
    }

    public static void setAuthInfo(Context context, String loginName, String loginPassword, String loginID) {
        save(context, TAG_LOGIN_NAME, loginName);
        save(context, TAG_LOGIN_PASSWORD, loginPassword);
        save(context, TAG_LOGIN_ID, loginID);
    }

    public static void resetAuthInfo(Context context) {
        save(context, TAG_LOGIN_NAME, null);
        save(context, TAG_LOGIN_PASSWORD, null);
        save(context, TAG_LOGIN_ID, null);
    }

    public static String getLoginName(Context context) {
        return getString(context, TAG_LOGIN_NAME, null);
    }

    public static String getLoginPassword(Context context) {
        return getString(context, TAG_LOGIN_PASSWORD, null);
    }

    public static String getLoginID(Context context) {
        return getString(context, TAG_LOGIN_ID, null);
    }


    ////////////////////
    public static int getInt(Context context, String key, int defValue) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getInt(key, defValue);
    }

    public static long getLong(Context context, String key, long defValue) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getLong(key, defValue);
    }

    public static String getString(Context context, String key, String defValue) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString(key, defValue);
    }

    public static boolean getBoolean(Context context, String key, boolean defValue) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getBoolean(key, defValue);
    }

    public static void save(Context context, String key, String value) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static void save(Context context, String key, long value) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = prefs.edit();
        editor.putLong(key, value);
        editor.commit();
    }

    public static void save(Context context, String key, int value) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = prefs.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public static void save(Context context, String key, boolean value) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = prefs.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

}
