package ngse.reactionapp.Data;

import android.text.format.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Kulykov Anton on 08.10.15.
 */
public class Message {
    private String messageBody;
    private String createAt;
    private Sender sender;
    private long dateInMillis;
    private List<MessageFile> messageFiles=new ArrayList<>();
    public String getMessageBody() {
        return messageBody;
    }

    public void setMessageBody(String messageBody) {
        this.messageBody = messageBody;
    }

    public String getCreateAt() {
        String date="";
        if (createAt!=null)
        try {

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date newDate = format.parse(createAt);
            if (!DateUtils.isToday(newDate.getTime())) {
                format = new SimpleDateFormat("MM/dd/yyyy");//, HH:mm a
                date = format.format(newDate);
            }
            else
            {
                date="Today";
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date;
    }

    public String getCreateAtTime() {
        String date="";
        if (createAt!=null)
            try {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date newDate = format.parse(createAt);
                if (!DateUtils.isToday(newDate.getTime())) {
                    format = new SimpleDateFormat("HH:mm a");
                    date = format.format(newDate);
                }
                else
                {
                    date = DateUtils.getRelativeTimeSpanString(newDate.getTime(), System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS).toString();
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }

        return date;
    }
    private long getDateInMillis_() {
        long date=0;
        if (createAt!=null)
            try {
                String temp= createAt.substring(0, 10);
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                Date newDate = format.parse(temp);
               date=newDate.getTime();

            } catch (ParseException e) {
                e.printStackTrace();
            }

        return date;
    }

    public void setCreateAt(String createAt) {
        this.createAt = createAt;
        dateInMillis=getDateInMillis_();
    }

    public long getDateInMillis() {
        return dateInMillis;
    }

    public Sender getSender() {
        return sender;
    }

    public void setSender(Sender sender) {
        this.sender = sender;
    }

    public List<MessageFile> getMessageFiles() {
        return messageFiles;
    }

    public void addMessageFiles(MessageFile messageFile) {
        this.messageFiles.add(messageFile);
    }


}
