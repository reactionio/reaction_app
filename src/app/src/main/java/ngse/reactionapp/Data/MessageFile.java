package ngse.reactionapp.Data;

/**
 * Created by Kulykov Anton on 06.11.15.
 */
public class MessageFile {
    private String name;
    private String url;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFileName() {
        String filenameArray[] = url.split("\\.");
        String extension = filenameArray[filenameArray.length-1];

        return name+"."+extension;
    }
}
