package ngse.reactionapp.Data;

/**
 * Created by Kulykov Anton on 08.10.15.
 */
public class Sender {
    private long id;
    private String senderClass;
    private String image;
    private String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSenderClass() {
        return senderClass;
    }

    public void setSenderClass(String senderClass) {
        this.senderClass = senderClass;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
