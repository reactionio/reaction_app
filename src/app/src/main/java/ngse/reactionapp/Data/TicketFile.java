package ngse.reactionapp.Data;

import java.io.File;

import ngse.reactionapp.Enums.FILE_TYPE;

/**
 * Created by Kulykov Anton on 29.10.15.
 */
public class TicketFile {
    private FILE_TYPE fileType;
    private File file;

    public TicketFile(FILE_TYPE fileType, File file) {
        this.fileType = fileType;
        this.file = file;
    }

    public FILE_TYPE getFileType() {
        return fileType;
    }

    public void setFileType(FILE_TYPE fileType) {
        this.fileType = fileType;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }
}
