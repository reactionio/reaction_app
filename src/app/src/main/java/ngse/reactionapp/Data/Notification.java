package ngse.reactionapp.Data;

/**
 * Created by Kulykov Anton on 07.10.15.
 */
public class Notification {
    private String notification;
    private String created;

    public String getNotification() {
        return notification;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }
}
