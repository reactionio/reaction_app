package ngse.reactionapp.Data;

import java.io.Serializable;

/**
 * Created by Kulykov on 22.09.2015.
 */
public class Profile implements Serializable {
    private String firstname = null;
    private String lastname = null;
    private String email = null;
    private String phone = null;
    private String address = null;
    private String imageUrl;
    private long id;


    public void update(Profile profile) {
        this.id = profile.getId();
        this.firstname=profile.getFirstname();
        this.lastname=profile.getLastname();
        this.email=profile.getEmail();
        this.phone=profile.getPhone();
        this.address=profile.getAddress();
        this.imageUrl=profile.getImageUrl();

    }
    public String getFullname() {
        return firstname+" "+lastname;
    }
    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
