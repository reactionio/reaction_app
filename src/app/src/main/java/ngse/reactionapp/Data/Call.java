package ngse.reactionapp.Data;

import android.util.Log;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import ngse.reactionapp.Adapters.SortedListAdapter;


public class Call implements SortedListAdapter.ViewModel, Serializable {

    private long id;
    //    private long userId;
//    private long clientId;
    private String statusText;
    private String createdAt;
    //private Manager manager;
    //после правок
    private String companyName;
    private String contactName;
    private String phone;
    private int fromType; // 1 or 2
    private String callDate;
    private int callType; //1-OUTGOING 2-INCOMING 3-MISSED
    private int duration;

    //boolean == true if is form standart call log
    private boolean isStandartCall;

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getContactName() {
        return contactName;
    }

    public void setCallType(int callType) {
        this.callType = callType;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setFromType(int fromType) {
        this.fromType = fromType;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getPhone() {
        return phone;
    }

    public int getFromType() {
        return fromType;
    }

    public int getCallType() {
        return callType;
    }

    public int getDuration() {
        return duration;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setCallDate(long callDate) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateString = formatter.format(new Date(callDate));
        Log.d("Logger", contactName + ", date: " + dateString);
        this.createdAt = dateString;
    }

    public boolean isStandartCall() {
        return isStandartCall;
    }

    public void setStandartCall(boolean standartCall) {
        isStandartCall = standartCall;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCDate() {
        return callDate;
    }

    public long getCallDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(createdAt);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return convertedDate.getTime();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Call call = (Call) o;

        if (id != call.id) return false;
        if (fromType != call.fromType) return false;
        if (callType != call.callType) return false;
        if (duration != call.duration) return false;
        if (isStandartCall != call.isStandartCall) return false;
        if (statusText != null ? !statusText.equals(call.statusText) : call.statusText != null)
            return false;
        if (createdAt != null ? !createdAt.equals(call.createdAt) : call.createdAt != null)
            return false;
        if (companyName != null ? !companyName.equals(call.companyName) : call.companyName != null)
            return false;
        if (contactName != null ? !contactName.equals(call.contactName) : call.contactName != null)
            return false;
        if (phone != null ? !phone.equals(call.phone) : call.phone != null) return false;
        return callDate != null ? callDate.equals(call.callDate) : call.callDate == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (statusText != null ? statusText.hashCode() : 0);
        result = 31 * result + (createdAt != null ? createdAt.hashCode() : 0);
        result = 31 * result + (companyName != null ? companyName.hashCode() : 0);
        result = 31 * result + (contactName != null ? contactName.hashCode() : 0);
        result = 31 * result + (phone != null ? phone.hashCode() : 0);
        result = 31 * result + fromType;
        result = 31 * result + (callDate != null ? callDate.hashCode() : 0);
        result = 31 * result + callType;
        result = 31 * result + duration;
        result = 31 * result + (isStandartCall ? 1 : 0);
        return result;
    }
}
