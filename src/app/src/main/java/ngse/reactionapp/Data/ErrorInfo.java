package ngse.reactionapp.Data;

public class ErrorInfo {

    private boolean result; // false - error
    private int status;
    private String text;

    public ErrorInfo(boolean result, int status,String text) {
        this.result = result;
        this.status = status;
        this.text = text;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
